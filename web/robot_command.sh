#Ex.
#--loglevel TRACE --suitestatlevel 3
#-v HUB:http://127.0.0.1:4444/wd/hub

robot -v ENV:SIT -i IncludeRobot  --suite api  /Users/aphirak/ant-robot

robot -v ENV:SIT -i IncludeRobot  --suite web  /Users/aphirak/ant-robot

robot -v ENV:SIT -v HUB:http://127.0.0.1:4444/wd/hub  -i IncludeRobot  --suite web  /Users/aphirak/ant-robot

robot -v ENV:SIT -i IncludeRobot  /Users/aphirak/ant-robot/api/testcases/foreign_holiday

#Run "foreign_holiday" All Folder
robot -v ENV:SIT -i IncludeRobot  --suite foreign_holiday /Users/aphirak/ant-robot/api/testcases
robot -v ENV:SIT -i IncludeRobot   /Users/aphirak/ant-robot/api/testcases/foreign_holiday
robot -v ENV:SIT -i IncludeRobot  --suite mm_conf_eq_exception_controller  --suite mm_rerun_eq_controller  /Users/aphirak/ant-robot/api/testcases/market_maker

#Run "foreign_holiday" by testcase 
robot -v ENV:SIT -i IncludeRobot  -t 'Get foreign holiday'  /Users/aphirak/ant-robot/api/testcases/foreign_holiday/foreign_holiday.robot
robot -v ENV:SIT -i IncludeRobot  -t 'Get foreign holiday' -t 'Get holiday status' /Users/aphirak/ant-robot/api/testcases/foreign_holiday/foreign_holiday.robot

