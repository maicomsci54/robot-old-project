*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/index_profile/new_customize_index_locators.robot
Resource     ../common/web_common.robot
Resource     new_customize_index_page_keywords.robot

*** Keywords *** 

Input Index Short Name Value
    [Arguments]  ${value}
    Input Text  ${tbx_index_short_name}   ${value}

Input First Cut-Off Date Value
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${tbx_first_cut_off_date}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Input First Capped Date Value
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${tbx_first_capped_date}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Input First Effective Date Value
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${tbx_first_effective_date}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Input Base Index Value
    [Arguments]  ${value}
    Input Text  ${tbx_first_base_index_value}   ${value}

Input Base TRI Index Value
    [Arguments]  ${value}
    Input Text  ${tbx_base_tri_index_value}   ${value}

Select Status  
    [Arguments]   ${status_value}
    Click Element   ${ddl_status}
    ${txt_status_locator}=   Generate Element From Dynamic Locator   ${lbl_doup_down_value}   ${status_value} 
    Click Element   ${txt_status_locator}

Verify First Capped Date Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled   ${tbx_first_capped_date}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_first_capped_date}

Verify First Effective Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled  ${tbx_first_effective_date}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_first_effective_date}

Verify First Cut Off Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled  ${tbx_first_cut_off_date}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_first_cut_off_date}

Input Values On New Customize Index
    [Arguments]  &{field_name_and_values}
    :FOR  ${field_name}    IN   @{field_name_and_values.keys()}
    \     ${value} =    Set Variable     &{field_name_and_values}[${field_name}]
    \     Run Keyword If   '${field_name}' == 'Index_Short_Name'    Input Index Short Name Value  ${value}
    ...   ELSE IF  '${field_name}' == 'Index_Full_Name'    Input Index Full Name Value   ${value}
    ...   ELSE IF  '${field_name}' == 'Index_Calculate_Method'   Select Index Calculate Method   ${value}
    ...   ELSE IF  '${field_name}' == 'Formula'  Select Formula    ${value}
    ...   ELSE IF  '${field_name}' == 'First_Capped_Date'     Input First Capped Date Value   ${value}
    ...   ELSE IF  '${field_name}' == 'First_Cut-Off_Date'    Run keywords  Input First Cut-Off Date Value    ${value}    AND   Sleep    1s
    ...   ELSE IF  '${field_name}' == 'First_Effective_Date'  Run keywords  Input First Effective Date Value  ${value}    AND   Sleep    1s
    ...   ELSE IF  '${field_name}' == 'Base_Index_Value'  Input Base Index Value  ${value}
    ...   ELSE IF  '${field_name}' == 'Base_TRI_Index_Value'  Input Base TRI Index Value  ${value}
    ...   ELSE IF  '${field_name}' == 'Status'  Select Status  ${value}
  
#Rebalance Profile

Input Start Month & Year Calculate Value
    [Arguments]  ${year}  ${month}
    Click Element  ${tbx_start_month_year_calculate}
    Select From List By Value  ${ddl_select_years}   ${year} 
    ${lbl_monuth_locator}=   Generate Element From Dynamic Locator  ${lbl_monuth}   ${month}
    Click Element  ${lbl_monuth_locator}

Select Rebalance Frequency
    [Arguments]   ${rebalance_frequency_value} 
    Click Element   ${ddl_rebalance_frequency}
    ${lbl_rebalance_frequency}=   Generate Element From Dynamic Locator  ${lbl_doup_down_value}    ${rebalance_frequency_value}  
    Click Element   ${lbl_rebalance_frequency}

Input Effective Day  #(Business Day of Month)
    [Arguments]  ${value}
    Input Text  ${tbx_effective_day}   ${value}
    Press Keys  ${tbx_effective_day}  \ue004

Input Capped Day  #(No. of days before Effective Day)
    [Arguments]  ${value}
    Input Text  ${tbx_capped_day}   ${value}
    Press Keys  ${tbx_effective_day}  \ue004

Input Cut-Off Day  #(No. of days before Effective Day)
    [Arguments]  ${value}
    Input Text  ${tbx_cut_off_day}  ${value}
    Press Keys  ${tbx_cut_off_day}  \ue004

Verify Effective Day Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled  ${tbx_effective_day}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_effective_day}

Verify Capped Day Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled  ${tbx_capped_day}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_capped_day}

Verify Cut-Off Day Filed
   [Arguments]   ${status}
   Run Keyword If   '${status}' == 'Disable'  Element Should Be Disabled  ${tbx_cut_off_day}
   ...  ELSE IF   '${status}' == 'Enabled'   Element Should Be Enabled   ${tbx_cut_off_day}

Input Values On Rebalance Profile
    [Arguments]  &{field_name_and_values}
    :FOR  ${field_name}    IN   @{field_name_and_values.keys()}
    \     ${value} =    Set Variable     &{field_name_and_values}[${field_name}]
    \     Run Keyword If  '${field_name}' == 'Rebalance_Frequency'  Select Rebalance Frequency   ${value}
    ...   ELSE IF  '${field_name}' == 'Effective_Day'  Input Effective Day     ${value}
    ...   ELSE IF  '${field_name}' == 'Capped_Day'   Input Capped Day  ${value}
    ...   ELSE IF  '${field_name}' == 'Cut_Off_Day'  Input Cut-Off Day  ${value}
    Capture Page Screenshot

Input Cut-Off Day By Random Number In Range Until Got Success Message
    [Arguments]  ${warning_message}
    :FOR  ${index}  IN RANGE   2  16
    \   Set Test Variable   ${index}
    \   Input Cut-Off Day   ${index}
    \   Click Save Button 
    \   ${status}	${value} =   Run Keyword And Ignore Error   Verify Alert Message On Popup   ${warning_message}
    \   Click No Cofirm Button 
    \   Run Keyword If  '${status}' == 'PASS'	
        ...    Exit For Loop
    Click Save Button 
    Verify Alert Message On Popup   ${warning_message}