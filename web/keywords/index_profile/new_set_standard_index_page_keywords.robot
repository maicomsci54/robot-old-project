*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/index_profile/new_set_standard_index_locators.robot
Resource     ../common/web_common.robot

*** Keywords *** 

Input Auto-Complete Index Short Name Value
    [Arguments]  ${value}
    Input Text  ${tbx_auto_index_short_name}   ${value}
    ${lbl_auto_complete_locator}=  Generate Element From Dynamic Locator  ${lbl_auto_complete}  ${value}
    Click Element  ${lbl_auto_complete_locator}
    Check Loading Bar Is Not Visible 

Input Index Full Name Value
    [Arguments]  ${value}
    Input Text  ${tbx_index_full_name}   ${value}

Select Index Calculate Method 
    [Arguments]   ${index_calculate_method_value}
    Click Element   ${ddl_index_calculate_method}
    ${ddl_index_calculate_method_locator}=   Generate Element From Dynamic Locator   ${lbl_doup_down_value}   ${index_calculate_method_value} 
    Click Element   ${ddl_index_calculate_method_locator}

Select Related Data 
    [Arguments]   ${related_data_value}
    Click Element   ${ddl_related_data} 
    ${txt_related_data_locator}=   Generate Element From Dynamic Locator   ${lbl_doup_down_value}   ${related_data_value} 
    Click Element   ${txt_related_data_locator}
    
Select Formula  
    [Arguments]   ${formula_value}
    Click Element   ${ddl_formula}
    ${txt_formula_locator}=   Generate Element From Dynamic Locator   ${lbl_doup_down_value}   ${formula_value} 
    Click Element   ${txt_formula_locator}

Verify Default Index Type
    [Arguments]   ${index_type}
    ${index_type_value}=   Get Value    ${tbx_index_type}
    Should Be Equal   ${index_type_value}   ${index_type}

Verify Error Message Under Text Field
    [Arguments]   &{field_name_and_msg_error}
    :FOR  ${field_name}    IN   @{field_name_and_msg_error.keys()}
    \     ${value} =    Set Variable     &{field_name_and_msg_error}[${field_name}]
    \     ${field_name}=	Replace String	${field_name}	_	${SPACE}
    \     ${lbl_error_under_field_locator}=   Generate Element From Dynamic Locator   ${lbl_error_under_field}    ${field_name}    ${value}
    \     Element Text Should Be   ${lbl_error_under_field_locator}    ${value}
    Capture Page Screenshot 

Verify Error Message is Disappear
    [Arguments]   &{field_name_and_msg_error}
    :FOR  ${field_name}    IN   @{field_name_and_msg_error.keys()}
    \     ${value} =    Set Variable     &{field_name_and_msg_error}[${field_name}]
    \     ${field_name}=	Replace String	${field_name}	_	${SPACE}
    \     ${lbl_error_under_field_locator}=   Generate Element From Dynamic Locator   ${lbl_error_under_field}    ${field_name}    ${value}
    \     Wait Until Element Is Not Visible   ${lbl_error_under_field_locator}
    Capture Page Screenshot 

Verify Text Field Should Be Disabled
    [Arguments]   ${field_name}
    ${ddl_text_field_disable_locator}=   Generate Element From Dynamic Locator   ${ddl_text_field_disable}   ${field_name}
    Wait Until Element Is Visible   ${ddl_text_field_disable_locator}   10s

Verify Text Field Should Be Enabled
    [Arguments]    ${field_name}
    ${ddl_text_field_enabled_locator}=   Generate Element From Dynamic Locator   ${ddl_text_field_enabled}     ${field_name}
    Wait Until Element Is Visible   ${ddl_text_field_enabled_locator}  10s

Verify Value Is Exist In Text field
    [Arguments]   ${field_name}   ${value}
    ${ddl_text_field_locator}=    Generate Element From Dynamic Locator  ${ddl_text_fields}   ${field_name}
    ${field_name_value}=   Get Text    ${ddl_text_field_locator}      
    Should Be Equal   ${field_name_value}    ${value}

Input Values On New SET Standard Index
    [Arguments]  &{field_name_and_values}
    :FOR  ${field_name}    IN   @{field_name_and_values.keys()}
    \     ${value} =    Set Variable     &{field_name_and_values}[${field_name}]
    \     Run Keyword If   '${field_name}' == 'Index_Short_Name'    Input Auto-Complete Index Short Name Value   ${value}
    ...   ELSE IF  '${field_name}' == 'Index_Calculate_Method'   Select Index Calculate Method   ${value}
    ...   ELSE IF  '${field_name}' == 'Relate_Data'   Select Related Data    ${value}
    ...   ELSE IF  '${field_name}' == 'Formula'  Select Formula    ${value}