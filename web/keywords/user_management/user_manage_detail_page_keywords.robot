*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/user_management/user_manage_detail_page_locators.robot
*** Keywords ***

Input Username
    [Arguments]  ${username_field}  
    Check Loading Bar Is Not Visible
    Input Text    ${txt_username_field}    ${username_field}
    ${search_username_locator}=    Generate Element From Dynamic Locator   ${lbl_search_username}     ${username_field}
    Wait Until Element Is Visible    ${search_username_locator}   3s
    Capture Page Screenshot
    Click Element    ${search_username_locator}

Select Type
    [Arguments]     ${type_name}
    Click Element   ${ddl_type}
    ${type_value_locator}=    Generate Element From Dynamic Locator   ${ddl_type_value}     ${type_name}
    Click Element   ${type_value_locator}

Click Status
    [Arguments]  ${status_name}
    ${status_value_locator}=    Generate Element From Dynamic Locator   ${rdo_status_value}     ${status_name}
    Click Element  ${status_value_locator}

Verify Message On Confirmation Popup
    [Arguments]  ${text_name}
    ${msg_popup_locator}=    Generate Element From Dynamic Locator   ${lbl_msg_popup}     ${text_name} 
    Wait Until Page Contains Element   ${msg_popup_locator}

Close Popup QR Code
    Wait Until Element Is Visible     ${pup_qr_code}    3s
    Click Element    ${pup_qr_code} 

Get Id From URL
    Page Should Contain     User Management Detail
    ${user_id_from_url}=    Get Value From URL By Regexp    [0-9]{3}
    Set Test Variable    ${USER_ID_FROM_URL}    ${user_id_from_url}[0]

Verify Column Username
    [Arguments]  ${username}
    ${lbl_column_user_locator}=    Generate Element From Dynamic Locator   ${lbl_column_username}     ${username} 
    Wait Until Page Contains Element   ${lbl_column_user_locator}

Delete User
   [Arguments]   ${username}   
   ${status}=  Run Keyword And Return Status   Input Username   ${username}
   Run Keyword If  '${status}' == 'False'  Press Keys   ${txt_username_field}   RETURN
   ${status}=    Run Keyword And Return Status   Verify Column Username   ${username}
   Run Keyword If   '${status}' == 'True'   Run keywords   Press Column On Table   username=${username}
   ...   AND  Sleep  5s
   ...   AND  Click Delete Button
   ...   AND  Verify Message On Confirmation Popup    Do you want to delete this user ( ${username} ) ?
   ...   AND  Click Yes Cofirm Button
   ...   AND  Verify Alert Message  Delete Successfully