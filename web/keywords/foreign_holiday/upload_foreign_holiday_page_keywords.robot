*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/foreign_holiday/upload_foreign_holiday_locators.robot
Resource     ../common/web_common.robot
Resource     ../../../api/keywords/common/list_common.robot

*** Keywords *** 

Select Year
    [Arguments]   ${year_value}
    Click Element   ${ddl_year}
    ${txt_year_locator}=   Generate Element From Dynamic Locator   ${lbl_year_value}   ${year_value} 
    Click Element   ${txt_year_locator}

Upload Foreign Holiday File
    [Arguments]    ${file_path}
    Wait Until Element Is Enabled   ${btn_browse_foreign}
    Choose File    ${btn_browse_foreign}    ${file_path}

Compare Pre-Verify Should Be Equal All List
    [Arguments]  ${pre_verify_status_value}
    @{pre_verify_status_list}=   Append Column Values In List  ${col_pre_verify_status}  
    Remove Values From List   ${pre_verify_status_list}  Pre-Verify Status
    All Items In List Match   ${pre_verify_status_list}   ${pre_verify_status_value}

Compare Pre-Verify Should Be Equal By Index
    [Arguments]  ${pre_verify_status_value}   ${index}
    @{pre_verify_status_list}=   Append Column Values In List  ${col_pre_verify_status}  
    Remove Values From List   ${pre_verify_status_list}  Pre-Verify Status
    Should Contain   ${pre_verify_status_list}[${index}]  ${pre_verify_status_value}

Click Upload All Button
    Click Element   ${btn_upload_all} 

Compare Upload Status Should Be Equal By Index
    [Arguments]  ${upload_status_value}   ${index}
    @{upload_status_list}=   Append Column Values In List  ${col_upload_status}  
    Remove Values From List   ${upload_status_list}  Upload Status
    Should Contain   ${upload_status_list}[${index}]  ${upload_status_value}