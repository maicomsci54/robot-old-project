*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/foreign_holiday/new_foreign_holiday_locators.robot
Resource     ../common/web_common.robot

*** Keywords *** 

Select Country
    [Arguments]   ${country_value}
    Click Element  ${ddl_country} 
    Input Text  ${txb_search_country}   ${country_value}
    ${txt_country_locator}=   Generate Element From Dynamic Locator   ${lbl_country_value}   ${country_value} 
    Click Element   ${txt_country_locator}
    
Select Holiday Date
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${txb_holiday}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Input Description
    [Arguments]  ${description_value}
    Input Text   ${txb_description}   ${description_value}

Check If Exist Found Holiday And Remove
   [Arguments]  ${holiday_date}  ${country}
   Select Holiday Date From  ${holiday_date}
   Select Holiday Date To    ${holiday_date}
   ${status}	${value} =	Run Keyword And Ignore Error	Click Checkbox To Cancel   ${country}
   Run Keyword IF  '${status}' == 'PASS'   Run Keywords   Click Delete Button   
   ...  AND   Click Yes Cofirm Button  
   ...  AND   Verify Alert Message  Delete Successfully