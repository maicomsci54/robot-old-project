*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/foreign_holiday/foreign_holiday_locators.robot
Resource     ../common/web_common.robot

*** Keywords *** 

Select Holiday Date From
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${txb_holiday_from}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Select Holiday Date To
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${txb_holiday_to}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Click Checkbox To Cancel
    [Arguments]  ${country}
    ${chb_cancel_locator}=   Generate Element From Dynamic Locator    ${chb_cancel}    ${country}
    Wait Until Keyword Succeeds  30s  0.2s  Click Element  ${chb_cancel_locator}
    Click Header On Table