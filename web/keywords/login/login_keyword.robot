*** Settings ***
Resource    ../../../web/resources/locators/login/login_page_locators.robot

*** Keywords ***
Login To ANT Website
    [Arguments]    ${ant_username}    ${ant_password}
    Input Text    ${txt_username}    ${ant_username}
    Input Text    ${txt_password}    ${antpassword}
    Wait Until Keyword Succeeds  10min  2s    Run keywords  Click Element   ${btn_signin}  AND  Page Should Contain   Welcome to ANT
  