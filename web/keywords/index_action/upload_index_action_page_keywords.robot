*** Settings ***
Resource     ../../resources/init.robot
Resource     ../common/web_common.robot
Resource     ../../../web/resources/locators/index_action/upload_index_action_locators.robot

*** Keywords *** 
Select Index Type   
    [Arguments]   ${index_type_value} 
    Click Element  ${ddl_index_type}  
    ${lbl_index_type_value_locator}=   Generate Element From Dynamic Locator   ${lbl_index_type_value}   ${index_type_value} 
    Click Element   ${lbl_index_type_value_locator}

Select Effective Date
    [Arguments]  ${yyyy_mm_dd}
    Input Text   ${tbx_effective_date}   ${yyyy_mm_dd}
    Generate Locator Click Day On Date Picker   ${yyyy_mm_dd}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}

Upload Index Actions File
    [Arguments]    ${file_path}
    Wait Until Element Is Enabled   ${btn_browse}
    Choose File    ${btn_browse}    ${file_path}

Verify Pre-Verify Status 
    [Arguments]  ${column_msg}
    Sleep  1.5s
    Capture Page Screenshot
    ${lbl_result}=   Get Text  ${lbl_pre_verify_status}
    Should Be Equal  ${column_msg}   ${lbl_result}

Verify Upload Status 
    [Arguments]  ${column_msg}
    Sleep  1.5s
    Capture Page Screenshot
    ${lbl_result}=   Get Text  ${lbl_upload_status}
    Should Be Equal  ${column_msg}   ${lbl_result}

Copy File Name 
   [Arguments]  ${old_file_name}  ${new_file_name}
   Copy File	${old_file_name}  ${new_file_name}
   [Return]   ${new_file_name}

Set New Date In File Name 
    [Arguments]   ${date}
    ${new_date}=   Replace String   ${dummy_date}   date    ${date}
    Set Test Variable  ${NEW_DATE}

Click Upload All Button
    Click Element  ${btn_upload_all}