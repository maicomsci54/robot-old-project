*** Settings ***
Resource     ../../resources/init.robot
Resource     ../common/web_common.robot
Resource     ../../../api/keywords/common/list_common.robot
Resource     ../../../web/resources/locators/index_action/Index_action_detail_locators.robot

*** Keywords *** 

Compare Stock Name In Table With CSV File
    [Arguments]   ${csv_path}
    @{col_stock_name_list}=   Append Column Values In List  ${col_stock_name}    
    Remove Values From List  ${col_stock_name_list}  TI
    ${col_stock_name_list}=  Sort List With Options  ${col_stock_name_list}   ASC 
    ${csv_data}=    Read CSV File     ${csv_path}
    @{csv_stock_name_list}=   Create List
    :FOR  ${stock_name}   IN   @{csv_data}
    \    Append To List   ${csv_stock_name_list}   @{stock_name}[0]
    ${csv_stock_name_list}=  Sort List With Options   ${csv_stock_name_list}    ASC
    Remove Values From List  ${csv_stock_name_list}  Ti
    Lists Should Be Equal  ${col_stock_name_list}  ${csv_stock_name_list}

Compare Factor Score In Table With CSV File
    [Arguments]   ${csv_path}  ${calculate_method}=${non_capped}  ${rerate_data}=${empty}
    @{col_factor_score_list}=   Append Column Values In List  ${col_factor_score} 
    Remove Values From List  ${col_factor_score_list}  Factor Score
    ${col_factor_score_list}=  Sort List With Options  ${col_factor_score_list}   ASC 
    log  ${col_factor_score_list} 
    ${csv_data}=    Read CSV File     ${csv_path}
    @{csv_factor_score_list}=   Create List
    :FOR  ${factor_score}   IN   @{csv_data}
    \    Append To List   ${csv_factor_score_list}   @{factor_score}[1]
    Remove Values From List  ${csv_factor_score_list}  Factor Score
    ${csv_factor_score_list}=   Sort List With Options   ${csv_factor_score_list}    ASC
    Run Keyword If  ('${calculate_method}' == 'capped') or ('${calculate_method}' == 'non_capped' and '${rerate_data}' != '${empty}')  
    ...  Lists Should Be Equal  ${col_factor_score_list}  ${csv_factor_score_list}
    ...  ELSE IF   '${calculate_method}' == 'non_capped' and '${rerate_data}' == '${empty}'  All Items In List Empty   ${col_factor_score_list}

Compare Factor Score Should Be Equal To Weigth Column
    @{col_factor_score_list}=   Append Column Values In List  ${col_factor_score} 
    Remove Values From List  ${col_factor_score_list}  Factor Score
    ${col_factor_score_list}=  Sort List With Options  ${col_factor_score_list}   ASC
    ${col_factor_score_list}=   Round Decimal In List   ${col_factor_score_list}  4
    @{col_weight_list}=   Append Column Values In List  ${col_weight}
    Remove Values From List  ${col_weight_list}  Weight
    ${col_weight_list}=   Sort List With Options  ${col_weight_list}   ASC
    ${col_weight_list}=   Convert String To Float  ${col_weight_list}
    Lists Should Be Equal   ${col_factor_score_list}  ${col_weight_list}