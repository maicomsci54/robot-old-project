*** Settings ***
Resource     ../../resources/init.robot
Resource     ../common/web_common.robot
Resource     ../../../web/resources/locators/index_action/index_action_locators.robot

*** Variable ***
${index}   1

*** Keywords *** 

Verify Value On Table By Column
    [Arguments]  &{column_name_and_expect_value}
    Sleep  3s
    :FOR  ${column_name}    IN   @{column_name_and_expect_value.keys()}
    \     ${expect_value} =    Set Variable     &{column_name_and_expect_value}[${column_name}]
    \     Run Keyword If   '${column_name}' == 'Index_Name'  Compere Values On Column By Index   indexShortName  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Capped_Date'  Compere Values On Column By Index   cappedDate  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Effective_Date'  Compere Values On Column By Index   effectiveDate  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Type'  Compere Values On Column By Index   type  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Status'  Compere Values On Column By Index   caStatus  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Entry_Date'  Compere Values On Column By Index   entryDate  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'source'  Compere Values On Column By Index   Source  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Last_Updated'  Compere Values On Column By Index   updatedDate  ${expect_value}   ${index}
    ...   ELSE IF    '${column_name}' == 'Last_Update_User'  Compere Values On Column By Index   updatedBy  ${expect_value}   ${index}

Select All Checkbox
    Wait Until Keyword Succeeds  30s  0.2s  Click Element    ${cbx_all}
    Click Header On Table
    
Click Index Name On Table Row 
    [Arguments]  ${index_name}
    ${index_short_name_locator}=   Generate Element From Dynamic Locator   ${lbl_index_short_name}   ${index_name}
    Sleep  1s
    Click Element  ${index_short_name_locator}
    Reload page
    Page Should Contain  Index Action Detail

Select/Unselect Status
    [Arguments]  ${status_name}
    ${status_name_locator}=   Generate Element From Dynamic Locator   ${cbx_status}  ${status_name}
    Click Element  ${status_name_locator}

Click Approve Button
    Click Element   ${btn_approve}

Click Capped Button
    Click Element   ${btn_capped}