*** Settings ***
Resource     ../../resources/init.robot
Resource    ../../../web/resources/locators/common/common_locators.robot
Resource    locator_common.robot

*** Variables ***
@{chrome_arguments}    --disable-infobars    --disable-gpu     --no-sandbox     --ignore-certificate-errors    --headless
${download_directory}    ${CURDIR}/download
${timeout}    30s
${hub_url}  ${HUB}

*** Keywords ***
Open Browser With Option
    [Arguments]    ${url}    ${browser}=Chrome    ${headless_mode}=${True}
    Run Keyword If    ${headless_mode} == ${True} and '${browser}' == 'Chrome'
    ...    Run Keywords
    ...    Open Browser With Chrome Headless Mode    ${url}
    ...    AND   Set Browser Implicit Wait    3s
    ...    ELSE IF    ${headless_mode} == ${False}
    ...    Run keywords   Open Browser    ${url}     ${browser}   remote_url=${hub_url}  options=add_argument("--ignore-certificate-errors")   
    ...    AND   Maximize Browser Window

Open Browser With Chrome Headless Mode
    [Arguments]      ${url}
    ${chrome_options}=    Set Chrome Options
    Open Browser    ${url}     ${browser}    remote_url=${hub_url}   options=${chrome_options}  
    Set Window Size    1920    1080

Set Chrome Options
    [Documentation]    Set Chrome options for headless mode
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver   
    ${prefs}    Create Dictionary   download.default_directory=${download_directory} 
    Directory Should Exist   ${download_directory} 
    : FOR    ${option}    IN    @{chrome_arguments}
    \    Call Method    ${options}    add_argument    ${option}
    \    Call Method    ${options}    add_experimental_option   prefs   ${prefs}
    [Return]    ${options} 

Clean Environment
    Close All Browsers
    
Navigate To Main Menu On Dashboard
    [Arguments]    ${main_menu_name}
    ${main_menu_locator}=    Generate Element From Dynamic Locator   ${lnk_main_menu}     ${main_menu_name}
    Click Element    ${main_menu_locator}

Navigate To Left Main Menu
    [Arguments]    ${main_left_menu_name}   ${sub_left_menu_name}=${empty}
    ${main_left_menu_locator}=    Generate Element From Dynamic Locator   ${lnk_main_left_menu}    ${main_left_menu_name}
    Wait Until Page Contains Element     ${main_left_menu_locator}   3s
    ${sub_left_menu_locator}=    Generate Element From Dynamic Locator   ${lnk_sub_left_menu}    ${sub_left_menu_name}
    Wait Until Page Contains Element     ${sub_left_menu_locator}   3s
    Run Keyword If   '${sub_left_menu_name}' == '${empty}'  Run Keywords   Click Element    ${main_left_menu_locator}  
    ...  AND    Click Element    ${main_left_menu_locator}
    ...  ELSE   Run Keywords   Sleep   5s  AND  Click Element    ${main_left_menu_locator}  
    ...  AND   Click Element    ${sub_left_menu_locator}

Check Loading Bar Is Not Visible 
   Wait Until Element Is Not Visible    ${ele_loading_bar}   ${timeout}

Verify Alert Message
   [Arguments]    ${warning_message}
    Wait Until Element Is Visible   ${lbl_msg_box}   10s
   ${msg_detail}=   Get Text   ${lbl_msg_box} 
   Should Be Equal  ${msg_detail}   ${warning_message}
   Capture Page Screenshot 
   ${status}	${value} =	Run Keyword And Ignore Error	Should Contain  ${warning_message}   Successfully
   Run Keyword IF  '${status}' == 'PASS'   Wait Until Element Is Not Visible   ${lbl_msg_box}   10s

Verify Alert Message On Popup
   [Arguments]    ${msg_name}
   ${msg_popup_locator}=    Generate Element From Dynamic Locator   ${lbl_msg_popup}    ${msg_name}
   Wait Until Element Is Visible   ${msg_popup_locator}
   Capture Page Screenshot 

Get Value From URL By Regexp
    [Arguments]    ${regular_expression}
    ${url} =   Get Location
    ${value} =    Get Regexp Matches	${url}    ${regular_expression}
    [Return]    ${value}

List Should Contain Property With Value 
    [Arguments]   ${property_list}   ${expect_value}
    :FOR  ${property}    IN   @{property_list}
    \  Should Contain   ${property}   ${expect_value}

Navigate To Main And Sub Menu Bar
    [Arguments]    ${main_menu_bar_name}   ${sub_main_menu_bar_name}=${empty}   ${sub_menu_bar_name}=${empty}
    Check Loading Bar Is Not Visible
    ${main_menu_bar_locator}=   Generate Element From Dynamic Locator   ${lnk_main_menu_bar}    ${main_menu_bar_name}
    ${sub_main_menu_bar_locator}=    Generate Element From Dynamic Locator   ${lnk_sub_main_menu_bar}      ${sub_main_menu_bar_name}
    ${sub_menu_bar_locator}=    Generate Element From Dynamic Locator   ${lnk_sub_menu_bar}      ${sub_menu_bar_name}
    Run Keyword If  '${sub_main_menu_bar_name}' == '${empty}'    Click Element  ${main_menu_bar_locator}
    ...  ELSE IF   '${sub_menu_bar_name}' == '${empty}'  Run Keywords  Click Element  ${main_menu_bar_locator}  
    ...  AND  Click Element  ${sub_main_menu_bar_locator}
    ...  ELSE IF  '${sub_main_menu_bar_name}' != '${empty}' and '${sub_menu_bar_name}' != '${empty}' 
    ...  Run Keywords   Click Element  ${main_menu_bar_locator}  
    ...  AND  Mouse Over  ${sub_main_menu_bar_locator}  
    ...  AND  Click Element  ${sub_menu_bar_locator} 
    
Click Label Search Auto Complete
    [Arguments]    ${label_name}
    ${lbl_text_box_locator}=   Generate Element From Dynamic Locator   ${lbl_text_box}    ${label_name}
    Wait Until Keyword Succeeds  10s  3s  Click Element   ${lbl_text_box_locator}
    
Generate Locator Click Day On Date Picker
    [Arguments]  ${yyyy_mm_dd}
    ${result_date}=   Remove String Using Regexp    ${yyyy_mm_dd}    [0-9]{4}-[0-9]{2}-
    @{characters} =	Split String To Characters	${result_date}
    Run Keyword If   '${characters}[0]' == '0'    Remove 0 In Day   ${result_date}  ${characters}[1]
    ${lbl_date_value_locator} =    Generate Element From Dynamic Locator    ${lbl_date_value}   ${result_date}
    Set Test Variable   ${lbl_date_value_locator}

Remove 0 In Day
    [Arguments]   ${day}  ${number}
    ${result_date}=   Replace String    ${day}    ${day}    ${number}
    Set Test Variable   ${result_date}

Click Save Button
    Wait Until Page Contains Element  ${btn_save}     5s
    Wait Until Keyword Succeeds  1min  3s  Element Should Be Enabled   ${btn_save} 
    Click Element    ${btn_save} 

Click Edit Button
    Wait Until Page Contains Element  ${btn_edit}      5s
    Wait Until Keyword Succeeds  1min  3s  Element Should Be Enabled   ${btn_edit} 
    Click Element   ${btn_edit} 

Click Yes Cofirm Button
    Wait Until Page Contains Element  ${btn_confirm_yes}
    Click Element    ${btn_confirm_yes}

Click No Cofirm Button
    Wait Until Page Contains Element  ${btn_confirm_no}
    Click Element    ${btn_confirm_no}

Click Delete Button
    Check Loading Bar Is Not Visible
    Mouse Over  ${btn_delete}
    Click Element    ${btn_delete}

Click Cancel Button
    Check Loading Bar Is Not Visible
    Mouse Over  ${btn_cancel}
    Click Element    ${btn_cancel}

Press Column On Table
    [Arguments]  &{column_and_value}
    Check Loading Bar Is Not Visible
    Capture Page Screenshot
    :FOR  ${column_field}    IN   @{column_and_value.keys()}
    \     ${value} =    Set Variable    &{column_and_value}[${column_field}]
    ${col_tabel_locator}=    Generate Element From Dynamic Locator   ${col_tabel_fileds}    ${column_field}   ${value}
    Wait Until Page Contains Element   ${col_tabel_locator}   5s
    Click Element   ${col_tabel_locator}
    Capture Page Screenshot
    ${location}=   Get Location
    Sleep  1s
    Reload Page
    Capture Page Screenshot

Verify Textbox Fileds Should Be Disabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${txb_fileds_locator}=    Generate Element From Dynamic Locator   ${txb_fileds}   ${form_control_name} 
    \      Element Should Be Disabled  ${txb_fileds_locator}

Verify Textbox Fileds Should Be Enabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${txb_fileds_locator}=    Generate Element From Dynamic Locator   ${txb_fileds}   ${form_control_name} 
    \      Element Should Be Enabled  ${txb_fileds_locator}

Verify Datepicker Fileds Should Be Disabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${dpk_fileds_locator}=    Generate Element From Dynamic Locator   ${dpk_fileds}   ${form_control_name} 
    \      Element Should Be Disabled  ${dpk_fileds_locator}

Verify Datepicker Fileds Should Be Enabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${dpk_fileds_locator}=    Generate Element From Dynamic Locator   ${dpk_fileds}   ${form_control_name} 
    \      Element Should Be Enabled  ${dpk_fileds_locator}

Verify Dropdown List Should Be Disabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${ddl_fileds_locator}=    Generate Element From Dynamic Locator   ${ddl_fileds_disabled}    ${form_control_name} 
    \      Wait Until Element Is Visible   ${ddl_fileds_locator}

Verify Dropdown List Should Be Enabled
    [Arguments]  @{form_control_name_list} 
    : FOR  ${form_control_name}  IN   @{form_control_name_list} 
    \      ${ddl_fileds_locator}=    Generate Element From Dynamic Locator   ${ddl_fileds_disabled}    ${form_control_name} 
    \      Wait Until Element Is Not Visible   ${ddl_fileds_locator}

Verify Button Should Be Enabled
    [Arguments]  @{button_name_list} 
    : FOR  ${button_name}  IN   @{button_name_list} 
    \      ${button_name_locator}=    Generate Element From Dynamic Locator   ${btn_name}    ${button_name} 
    \      Element Should Be Enabled   ${button_name_locator}

Verify Button Should Be Disabled
    [Arguments]  @{button_name_list} 
    : FOR  ${button_name}  IN   @{button_name_list} 
    \      ${button_name_locator}=    Generate Element From Dynamic Locator   ${btn_name}    ${button_name} 
    \      Element Should Be Disabled   ${button_name_locator}

Click Header On Table
    Click Element    ${div_header}