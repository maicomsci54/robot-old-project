*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***
Generate Element From Dynamic Locator
    [Arguments]    ${dynamic_locator}    @{list_value}
    ${index}=    Set Variable    ${0}
    :FOR    ${value}    IN    @{list_value}
    \    ${dynamic_locator}=    Replace String    ${dynamic_locator}    _DYNAMIC_${index}    ${value}
    \    ${index}=    Set Variable    ${index+1}
    [Return]    ${dynamic_locator}

Get Text From Elements Append To List
   [Arguments]  ${element_locator}
   Sleep  3s
   @{actual_list}=   Create List
   ${elements}=    Get WebElements   ${element_locator}
   :FOR  ${element}  IN   @{elements}
   \   Wait Until Keyword Succeeds  30s  0.2s    Append To List   ${actual_list}  ${element.text}
   [Return]  @{actual_list}

Append Column Values In List
    [Arguments]  ${column_element}
    @{list_values}=   Create List
    ${elements}=    Get WebElements    ${column_element}
    :FOR  ${element}    IN    @{elements}
    \     ${value}=  Get Text    ${element}
    \     Append To List   ${list_values}   ${value}
    [Return]   ${list_values}

Compere Values On Column By Index
    [Arguments]  ${column_name}   ${expect_value}   ${index}
    ${col_name_locator}=   Generate Element From Dynamic Locator   ${col_name_id}   ${column_name}
    ${list_values}=  Append Column Values In List  ${col_name_locator}
    Should Be Equal  ${list_values}[${index}]   ${expect_value}

