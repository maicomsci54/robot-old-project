*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/instrument/Instrument_h_sp_detail_page_locators.robot
Resource     ../common/web_common.robot

*** Keywords *** 

Input Instrument Short Name On Textbox
    [Arguments]  ${instrument_short_name}
    Check Loading Bar Is Not Visible 
    Wait Until Element Is Visible     ${txt_instrument_short_name_filed}
    Input Text     ${txt_instrument_short_name_filed}    ${instrument_short_name}
    Click Label Search Auto Complete   ${instrument_short_name}

Input Effective Date On Textbox
    [Arguments]  ${effective_date}
    Wait Until Element Is Visible   ${txt_effective_date_filed}
    Input Text  ${txt_effective_date_filed}  ${effective_date}
    Generate Locator Click Day On Date Picker  ${effective_date}
    Wait Until Keyword Succeeds  10s  0.2s   Click Element   ${lbl_date_value_locator}

Verify Not Select Effective Date
    [Arguments]  ${effective_date}
    ${status}   ${value}=   Run Keyword And Ignore Error  Input Effective Date On Textbox  ${effective_date}
    Should Be True   '${status}' == 'FAIL'

Clear Default Effective Date
    Check Loading Bar Is Not Visible 
    Wait Until Element Is Visible    ${btn_effective_date_filed}
    Click Element   ${btn_effective_date_filed}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${btn_clear_effective_date}

Verify Instrument Short Name Field Should Be Disabel
    Wait Until Element Is Visible  ${txt_instrument_short_name_disabel}
    Element Should Be Disabled    ${txt_instrument_short_name_disabel}

Check And Remove Instrument Name
    [Documentation]   Can use this keyword to check first before run test make sure testdate
    [Arguments]  ${instrument_name}   ${user}   ${date_from}   ${date_to}
    Input Instrument Search Values   Instrument_Short_Name=${instrument_name}   User=${user}    effectiveDateFrom=${date_from}   effectiveDateTo=${date_to}
    ${status}   ${value}=  Run Keyword And Ignore Error    Page Should Contain   No Rows To Show
    Run Keyword If  '${status}' == 'FAIL'  Run Keywords  Press Column On Table   instrumentShortName=${instrument_name} 
    ...  AND   Click Delete Button
    ...  AND   Click Yes Cofirm Button
    ...  AND   Verify Alert Message  Delete Successfully