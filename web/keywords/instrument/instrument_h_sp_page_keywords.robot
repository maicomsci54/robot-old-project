*** Settings ***
Resource     ../../resources/init.robot
Resource     ../../../web/resources/locators/instrument/instrument_h_sp_page_locators.robot
Resource     ../common/web_common.robot

*** Variable ***
${file_name_pattern}  instrument_halt_suspend_date_.csv
${date_regexp}   ([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))
${download_path}  ${CURDIR}/../common/download

*** Keywords *** 
Input Instrument Search Values
    [Arguments]   &{txt_search_date_field_and_values_for_search}
    Clear Default Upload Date
    :FOR  ${search_field}    IN   @{txt_search_date_field_and_values_for_search.keys()}
    \     ${value} =    Set Variable     &{txt_search_date_field_and_values_for_search}[${search_field}]
    \     ${search_field}=   Replace String		  ${search_field}    _   ${SPACE}
    \     ${txt_search_text_field_locator} =    Generate Element From Dynamic Locator    ${txt_search_text_field}    ${search_field}
    \     ${txt_search_date_field_locator} =    Generate Element From Dynamic Locator    ${txt_search_date_field}    ${search_field}
    \     Run Keyword If   '${search_field}' == 'Instrument Short Name' or '${search_field}' == 'User'
    ...   Run Keywords   Wait Until Element Is Visible    ${txt_search_text_field_locator} 
    ...   AND   Input Text   ${txt_search_text_field_locator}   ${value}
    ...   AND   Click Label Search Auto Complete   ${value}
    ...   ELSE  Run Keywords   Wait Until Element Is Visible    ${txt_search_date_field_locator} 
    ...   AND   Input Text   ${txt_search_date_field_locator}   ${value}
    ...   AND   Generate Locator Click Day On Date Picker     ${value}
    ...   AND   Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${lbl_date_value_locator}  

Compare Column List With Result Values
    [Arguments]   &{column_value_and_search_result}
    :FOR  ${column_value}    IN   @{column_value_and_search_result.keys()}
    \     ${value}=    Set Variable     &{column_value_and_search_result}[${column_value}]
    \     ${clm_result_text_locator} =    Generate Element From Dynamic Locator    ${clm_result_text}   ${column_value}
    \     ${column_list}=  Get Text From Elements Append To List  ${clm_result_text_locator}
    \     Remove From List   ${column_list}   0
    \     List Should Contain Property With Value   ${column_list}  ${value}

Remove Number In Day
    [Arguments]   ${day}  ${number}
    ${result_date}=   Replace String    ${day}    ${day}    ${number}
    Set Test Variable   ${result_date}

Verify Search Result
    [Arguments]  ${instrument_short_name}=${expect_value}   ${effective_date}=${expect_value}   ${upload_date}=${expect_value}   ${last_update_by}=${expect_value}   
    ${tb_result_search_locator}=   Generate Element From Dynamic Locator   ${tb_result_search}     ${instrument_short_name}   ${effective_date}   ${upload_date}   ${last_update_by}
    Wait Until Element Is Visible   ${tb_result_search_locator}  3s
    
Click Export File
    Wait Until Element Is Visible  ${btn_export_excel}  
    Capture Page Screenshot
    Click Element    ${btn_export_excel}
    Capture Page Screenshot

Clear Default Upload Date
    Click Element   ${btn_upload_date_from}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${btn_clear_date}
    Click Element   ${btn_upload_date_to}
    Wait Until Keyword Succeeds  30s  0.2s   Click Element   ${btn_clear_date}

Verify File Download Should Exist
    [Arguments]  ${file_name}
    Directory Should Exist   ${download_path}
    Wait Until Created     ${download_path}/${file_name}
    File Should Exist	   ${download_path}/${file_name}
    
Delete Download File
    Remove Files   ${download_path}/${file_name}

Generate File Name By Current Date
    ${current_date}=  Get Date With Format And Increment   %Y%m%d 
    ${file_name}=    Replace String  ${file_name_pattern}   _date_   ${current_date}
    Set Test Variable  ${FILE_NAME}   ${file_name}

Load Values From Instrument CSV File In List
    ${all_instrument_list}=    Read CSV File    ${download_path}/${file_name}
    @{instrument_name_list}=   Create List
    @{effective_date_list}=   Create List
    @{upload_date_list}=   Create List
    @{last_update_by_list}=   Create List
    @{last_update_time_list}=   Create List
    :FOR  ${instrument}   IN   @{all_instrument_list}
    \    Append To List   ${instrument_name_list}   @{instrument}[0]
    \    Append To List   ${effective_date_list}   @{instrument}[1]
    \    Append To List   ${upload_date_list}   @{instrument}[2]
    \    Append To List   ${last_update_by_list}   @{instrument}[3]
    \    Append To List   ${last_update_time_list}   @{instrument}[4]
    Run Keywords  Sort List   ${instrument_name_list}  AND  Set Test Variable   ${instrument_name_list}
    Run Keywords  Sort List   ${effective_date_list}  AND  Set Test Variable   ${effective_date_list}
    Run Keywords  Sort List   ${upload_date_list}  AND  Set Test Variable   ${upload_date_list}
    Run Keywords  Sort List   ${last_update_by_list}  AND  Set Test Variable   ${last_update_by_list}
    Run Keywords  Sort List   ${last_update_time_list}  AND  Set Test Variable   ${last_update_time_list}

Get Value List On Colum
   [Arguments]  ${column_value}
    ${clm_result_text_locator} =    Generate Element From Dynamic Locator    ${clm_result_text}   ${column_value}
    ${column_list}=  Get Text From Elements Append To List  ${clm_result_text_locator}
    Sort List   ${column_list}
    Set Test Variable    ${column_list}

Compare Instrument CSV File With Result On Web
    Load Values From Instrument CSV File In List
    ${column_list}    Create List    instrumentShortName    effectiveDate   uploadDate   lastUpdateBy    lastUpdateTime
    :FOR  ${column}    IN    @{column_list}
    \    Get Value List On Colum  ${column}
    \    Run Keyword If  '${column}' == 'instrumentShortName'   Lists Should Be Equal   ${instrument_name_list}   ${column_list}
    ...  ELSE IF  '${column}' == 'effectiveDate'    Lists Should Be Equal   ${effective_date_list}   ${column_list}
    ...  ELSE IF  '${column}' == 'uploadDate'    Lists Should Be Equal   ${upload_date_list}   ${column_list}
    ...  ELSE IF  '${column}' == 'lastUpdateBy'    Lists Should Be Equal   ${last_update_by_list}   ${column_list}
    ...  ELSE IF  '${column}' == 'lastUpdateTime'  Lists Should Be Equal   ${last_update_time_list}  ${column_list}
    