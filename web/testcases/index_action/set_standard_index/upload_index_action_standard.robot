*** Settings ***
Documentation   Verify that can create index profiles and can't create if have any error 
Resource    ../../../../api/keywords/common/database_common.robot
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_action/upload_index_action_page_keywords.robot
Resource    ../../../keywords/index_action/index_action_page_keywords.robot
Resource    ../../../keywords/index_action/index_action_detail_page_keywords.robot
Resource    ../../../../api/keywords/common/date_time_common.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...  AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...  AND    Navigate To Main Menu On Dashboard    Index
Test Teardown   Clean Environment   

*** Variables ***
${file_duplicate}   ${CURDIR}/file_data/FIN_20210520.csv
${file_invaild_ti}   ${CURDIR}/file_data/FIN_20210519.csv
${file_invaild_af}   ${CURDIR}/file_data/FIN_20210518.csv
${dummy_date}   ${CURDIR}/file_data/TECH-ms_date.csv
${file_valid_capped}  ${CURDIR}/file_data/TOURISM_20210518.csv
${file_valid_non_capped}  ${CURDIR}/file_data/FINCIAL-m.csv
${file_valid_non_capped_rerate}  ${CURDIR}/file_data/PROF.csv
${effective_date}   2021-05-21
${index_action_name_capped}  TOURISM
${index_action_name_non_capped}  FINCIAL-m
${index_action_name_non_rerate}   PROF

*** Test Cases ***

Upload file name invalid format.
   [Documentation]  [UI] To Verify that the user can't upload file name invalid format.
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Set New Date In File Name   2020-03-20
   ${file_name}=  Copy File Name   ${dummy_date}  ${NEW_DATE}
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File  ${file_name}
   Verify Pre-Verify Status   Failure [1]: Invalid capped date.
   Remove File   ${file_name}

Upload a file at cappped date less than today
   [Documentation]  [UI] To Verify that the user can't upload a file at cappped date less than today
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Set New Date In File Name   20200121
   ${file_name}=  Copy File Name   ${dummy_date}  ${NEW_DATE}
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File  ${file_name}
   Verify Pre-Verify Status   Failure [1] :"Capped date less than today"
   Remove File   ${file_name}

Upload a file at cappped date equal today
   [Documentation]  [UI] To Verify that the user can upload a file at cappped date equal today
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   ${today_date}=  Get Current Date   result_format=%Y%m%d
   Set New Date In File Name   ${today_date}
   ${file_name}=  Copy File Name   ${dummy_date}  ${NEW_DATE}
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_name}
   Verify Pre-Verify Status  Successfully
   Remove File   ${file_name}

Upload a file at cappped date greater than effective date
   [Documentation]  [UI] To Verify that the user can't upload a file at cappped date 
   ...  greater than effective date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Set New Date In File Name   20210524
   ${file_name}=  Copy File Name   ${dummy_date}  ${NEW_DATE}
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date  ${effective_date}
   Upload Index Actions File   ${file_name}
   Verify Pre-Verify Status  Failure [1] :"Capped date gether than effective date"
   Remove File   ${file_name}

Upload a file at cappped date is holiday less than effective date
   [Documentation]  [UI] To Verify that the user can't upload a file 
   ...  at cappped date is holiday less than effective date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Set New Date In File Name   20201212
   ${file_name}=  Copy File Name   ${dummy_date}  ${NEW_DATE}
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_name}
   Verify Pre-Verify Status  Failure [1] :"Capped date is holiday"
   Remove File   ${file_name}

Upload a file at duplicate short name in the system.
   [Documentation]  [UI] To Verify that the user can't Upload a file at duplicate short name in the system.
   [Tags]    Regression    Smoke    Sanity   IncludeRobot  tesr
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_duplicate}
   Verify Pre-Verify Status  Successfully
   Click Upload All Button
   Click Yes Cofirm Button
   Verify Alert Message   Upload Completed
   Verify Upload Status  Failure : File name 'FIN' is duplicated, please re-check Index Action.
   Upload Index Actions File   ${file_duplicate}

Upload a file invalid data in file at TI doesn’t exist in system
   [Documentation]  [UI] To Verify that the user can't upload a file invalid data in file at TI doesn’t exist in system
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_invaild_ti}
   Verify Pre-Verify Status  Failure [1]: The security "wqqww" couldn't be found in trading system or contain space.
 
Upload a file at the AF less than 0 cases "index is Capped"
   [Documentation]  [UI] To Verify that the user can't upload a file invalid data in file at TI doesn’t exist in system
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_invaild_af}
   Verify Pre-Verify Status  Failure [1]: The factor score of "AMATA","BCP" must be greater than zero.

Upload index action set standard index is capped and verify status
   [Documentation]  [UI] To Verify that the user can upload index action set standard index  is capped
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File   ${file_valid_capped}
   Verify Pre-Verify Status  Successfully
   Click Upload All Button
   Click Yes Cofirm Button
   Verify Alert Message   Upload Completed
   Verify Upload Status   Successfully
   # Back to index action page to verify after upload
   Navigate To Left Main Menu  Index Action 
   Input Auto-Complete Index Short Name Value   ${index_action_name_capped}
   Verify Value On Table By Column   Index_Name=${index_action_name_capped}  Capped_Date=2021-05-18  Effective_Date=${effective_date}  Type=SET STANDARD   Status=PENDING
   # Verify index action detail with status is "PENDING"
   Click Index Name On Table Row  ${index_action_name_capped}
   Verify Textbox Fileds Should Be Disabled    indexShortName
   Verify Dropdown List Should Be Disabled     indexType   calculateMethod  formula
   Verify Datepicker Fileds Should Be Enabled  cappedDate  effectiveDate
   Verify Button Should Be Enabled   Add   Approve
   Verify Button Should Be Disabled  Delete   Capped
   Compare Stock Name In Table With CSV File    ${file_valid_non_capped}
   Compare Factor Score In Table With CSV File  ${file_valid_non_capped}   capped
   #Verify index action detail with status is "APPROVED"
   Click Approve Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully
   Input Auto-Complete Index Short Name Value   ${index_action_name_capped}
   Select/Unselect Status  PENDING
   Select/Unselect Status  APPROVED
   Verify Value On Table By Column   Status=APPROVED
   Click Index Name On Table Row  ${index_action_name_capped}
   Verify Textbox Fileds Should Be Disabled    indexShortName
   Verify Dropdown List Should Be Disabled     indexType   calculateMethod  formula
   Verify Datepicker Fileds Should Be Enabled  cappedDate  effectiveDate
   Verify Button Should Be Enabled   Add   Capped  Save
   Verify Button Should Be Disabled  Delete 
   # Verify index action detail with status is "CAPPED"
   Click Capped Button
   Click Yes Cofirm Button
   Verify Alert Message   Manual Capped Successfully
   Verify Textbox Fileds Should Be Disabled    indexShortName
   Verify Dropdown List Should Be Disabled     indexType   calculateMethod  formula 
   Verify Datepicker Fileds Should Be Disabled  cappedDate
   Verify Datepicker Fileds Should Be Enabled   effectiveDate
   Verify Button Should Be Enabled   Add  Save
   Verify Button Should Be Disabled  Capped  Delete 
   Navigate To Left Main Menu  Index Action
   Input Auto-Complete Index Short Name Value   ${index_action_name_capped}
   Select/Unselect Status  PENDING
   Select/Unselect Status  CAPPED
   Verify Value On Table By Column   Status=CAPPED
   Select All Checkbox
   Click Delete Button   
   Click Yes Cofirm Button
   Verify Alert Message   Delete Successfully

Upload index action set standard index is non-capped
   [Documentation]  [UI] To Verify that the user can upload index action set standard index is non-capped
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File  ${file_valid_non_capped}
   Verify Pre-Verify Status  Successfully
   Click Upload All Button
   Click Yes Cofirm Button
   Verify Alert Message   Upload Completed
   Verify Upload Status   Successfully
   Navigate To Left Main Menu  Index Action 
   Input Auto-Complete Index Short Name Value   ${index_action_name_non_capped}
   Verify Value On Table By Column   Index_Name=${index_action_name_non_capped}  Capped_Date=${empty}  Effective_Date=${effective_date}  Type=SET STANDARD   Status=PENDING
   #Verify index action detail with status is "PENDING"
   Click Index Name On Table Row  ${index_action_name_non_capped}
   Verify Textbox Fileds Should Be Disabled    indexShortName
   Verify Dropdown List Should Be Disabled     indexType   calculateMethod  formula
   Verify Datepicker Fileds Should Be Disabled  cappedDate
   Verify Datepicker Fileds Should Be Enabled   effectiveDate
   Verify Button Should Be Enabled   Add   Approve
   Verify Button Should Be Disabled  Delete
   Compare Stock Name In Table With CSV File    ${file_valid_non_capped}
   Compare Factor Score In Table With CSV File  ${file_valid_non_capped}   non_capped
   #Verify index action detail with status is "APPROVED"
   Click Approve Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully
   Input Auto-Complete Index Short Name Value   ${index_action_name_non_capped}
   Select/Unselect Status  PENDING
   Select/Unselect Status  APPROVED
   Verify Value On Table By Column   Status=APPROVED
   Click Index Name On Table Row  ${index_action_name_non_capped}
   Verify Textbox Fileds Should Be Disabled    indexShortName
   Verify Dropdown List Should Be Disabled     indexType   calculateMethod  formula
   Verify Datepicker Fileds Should Be Disabled  cappedDate
   Verify Datepicker Fileds Should Be Enabled   effectiveDate
   Verify Button Should Be Enabled   Add   Save
   Verify Button Should Be Disabled  Delete 
   Navigate To Left Main Menu  Index Action 
   Input Auto-Complete Index Short Name Value   ${index_action_name_non_capped}
   Select/Unselect Status  PENDING
   Select/Unselect Status  APPROVED
   Select All Checkbox
   Click Delete Button
   Click Yes Cofirm Button
   Verify Alert Message   Delete Successfully

Upload index action set standard index is non-capped and has related data
   [Documentation]  [UI] To Verify that the user can upload index action set standard 
   ...  index is non-capped and has related data
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu  Index Action 
   Navigate To Main And Sub Menu Bar   Index Action    Upload File
   Select Index Type   SET Standard Index
   Select Effective Date   ${effective_date}
   Upload Index Actions File  ${file_valid_non_capped_rerate} 
   Verify Pre-Verify Status  Successfully
   Click Upload All Button
   Click Yes Cofirm Button
   Verify Alert Message   Upload Completed
   Verify Upload Status   Successfully
   Navigate To Left Main Menu  Index Action 
   Input Auto-Complete Index Short Name Value   ${index_action_name_non_rerate} 
   Click Index Name On Table Row  ${index_action_name_non_rerate} 
   Compare Stock Name In Table With CSV File    ${file_valid_non_capped}
   Compare Factor Score In Table With CSV File  ${file_valid_non_capped}   non_capped   rerate_data
   Compare Factor Score Should Be Equal To Weigth Column
   Navigate To Left Main Menu  Index Action 
   Select All Checkbox
   Click Delete Button
   Click Yes Cofirm Button
   Verify Alert Message   Delete Successfully