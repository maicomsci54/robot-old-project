*** Settings ***
Documentation   Verify that mandatory/optional and other condition fields of "SET Standard Index" type
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_customize_index_page_keywords.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Index
Test Teardown   Clean Environment   

*** Variable ***
${msg_error_index_short_name}   The value must not equal Null or Blank or Thai language or Special Character except '#', '_', '@', '-'.
${msg_error_index_full_name}    The value must not equal Null or Blank or Thai language or Special Character except '#', '_', '@', '-' or Space.
${msg_error_calculate_method}   Select a index calculated method.
${msg_error_formula}    Select a formula.
${msg_error_related_data}  Select a related Data.
${msg_error_first_cut_off_date}    Select a first cut-off date.
${msg_error_base_index_value}    The value must be greater than zero.
${msg_error_base_tri_index_value}  The value must be greater than zero.
${msg_error_status}  Select a status.
${msg_error_start_month_year_calculate}   Select a Start Month & Year Calculate.
${msg_error_effective_day}  Effective day must less than or equal 15.
${msg_error_rebalance_frequency}    Select a Rebalance Frequency.
${msg_error_capped_day}  Capped Day must less than or equal 15.
${msg_error_cut_off_day}  Cut-off day must greater than Capped day and less than or equal 15.

*** Test Cases ***

Verify condition and mandatory of each field
   [Documentation]   [UI] Verify condition and mandatory of each fields is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Click Save Button
   Verify Default Index Type   Customize Index
   # Customize Index Panel
   Verify Error Message Under Text Field   Index_Short_Name=${msg_error_index_short_name}
   Verify Error Message Under Text Field   Index_Full_Name=${msg_error_index_full_name} 
   Verify Error Message Under Text Field   Index_Calculate_Method=${msg_error_calculate_method}
   Verify Error Message Under Text Field   First_Cut-Off_Date=${msg_error_first_cut_off_date}
   Verify Error Message Under Text Field   Base_Index_Value=${msg_error_base_index_value}
   Verify Error Message Under Text Field   Base_TRI_Index_Value=${msg_error_base_tri_index_value}
   Verify Error Message Under Text Field   Status=${msg_error_status}
   # Rebalance Profile Panel
   Verify Error Message Under Text Field   Start_Month_&_Year_Calculate=${msg_error_start_month_year_calculate} 
   Verify Error Message Under Text Field   Rebalance_Frequency=${msg_error_rebalance_frequency}
   Verify Error Message Under Text Field   Effective_Day=${msg_error_effective_day}
   Input Index Full Name Value     TEST_automate_01
   Verify Error Message is Disappear    Index_Full_Name=${msg_error_index_full_name} 
   Input Index Full Name Value     ทดสอบระบบ_๑๑๑
   Verify Error Message Under Text Field   Index_Full_Name=${msg_error_index_full_name} 

Verify the relation formula field with index calculate method field
   [Documentation]   [UI] Verify the relation formula field with index calculate method field is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot  
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   # If still not select index calculate method will disable formula field
   Verify Text Field Should Be Disabled    Formula
   Select Index Calculate Method   Equal Weighted
   Sleep   0.5s 
   Verify Text Field Should Be Disabled     Formula
   Select Index Calculate Method   Market Cap Weighted
   Click Save Button  
   Verify Error Message Under Text Field  Formula=${msg_error_formula}
   Verify Text Field Should Be Enabled    Formula
   Select Formula  A
   Select Index Calculate Method   Large-Cap Multi Factor Index
   Verify Text Field Should Be Disabled    Formula
   Sleep   0.5s 
   Verify Value Is Exist In Text field  formula    Select Formula

Verify the relation of datepick
   [Documentation]   [UI] Verify the relation first cut-off date field with first
   ...  capped date and first effective date method field is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   # If not still select
   Verify First Capped Date Filed  Disable
   Input First Cut-Off Date Value  2022-10-04
   Verify First Capped Date Filed  Enabled
   Verify First Effective Filed    Disable
   Input First Capped Date Value   2022-10-05
   Verify First Effective Filed    Enabled
   Input First Effective Date Value  2022-10-06

Verify Verify the relation effective Day field with first Capped day and first cut-off day field
   [Documentation]   [UI] Verify the relation effective Day field with first
   ...  Capped day and first cut-off day field is work correctly
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Verify Effective Day Filed  Enabled
   Verify Capped Day Filed   Disable
   Verify Cut-Off Day Filed  Disable
   Input Effective Day  1
   Verify Capped Day Filed   Enabled
   Verify Cut-Off Day Filed  Disable
   Input Capped Day  2
   Verify Cut-Off Day Filed   Enabled
   Input Cut-Off Day  3
   #Effective Day
   Input Effective Day  0
   Verify Error Message Under Text Field   Effective_Day=${msg_error_effective_day}
   Input Effective Day  16
   Verify Error Message Under Text Field   Effective_Day=${msg_error_effective_day}
   Input Effective Day  15
   Verify Error Message is Disappear    Effective_Day=${msg_error_effective_day}
   #Capped Day
   Input Capped Day  16
   Verify Error Message Under Text Field  Capped_Day=${msg_error_capped_day}
   Input Capped Day  15
   Verify Error Message is Disappear  Capped_Day=${msg_error_capped_day}
   Input Capped Day  0
   Verify Error Message Under Text Field  Capped_Day=${msg_error_capped_day}
   #Cut-Off Day
   Input Cut-Off Day   0
   Verify Error Message Under Text Field   Cut-Off_Day=${msg_error_cut_off_day} 
   Input Cut-Off Day   16
   Verify Error Message Under Text Field   Cut-Off_Day=${msg_error_cut_off_day} 
   Input Cut-Off Day   15
   Verify Error Message is Disappear  Cut-Off_Day=${msg_error_cut_off_day} 
   # Cut-Off Day less than Capped Day
   Input Capped Day   10
   Input Cut-Off Day  9
   Verify Error Message Under Text Field   Cut-Off_Day=${msg_error_cut_off_day} 
   Input Capped Day   8
   Verify Error Message is Disappear  Cut-Off_Day=${msg_error_cut_off_day} 