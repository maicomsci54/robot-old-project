*** Settings ***
Documentation   Verify that can create index profiles and can't create if have any error with "customize_index"
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../api/keywords/index_profile_controller/index_profiles_keywords.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_customize_index_page_keywords.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Index
#Test Teardown   Clean Environment   

*** Variable ***
#customize_index
${exsit_index_short_name}   index_robot_test_dup
${index_full_name}  TEST_ERROR
${formula}  A
${index_calculate_method}   Market Cap Weighted
${first_cut-off_date}   2022-02-02
${first_capped_date}    2022-02-03
${first_effective_date}  2022-02-04
${base_index_value}  1
${base_tri_index_value}  2
${status}  Calculate and Broadcast
#Rebalance Profile
${rebalance_frequency}  every 2 months
${effective_day}  11
${capped_day}   12
${cut_off_day}  13

*** Test Cases ***

Create New index profile wiht customize_index
   [Documentation]  [UI] To verify the user can create New "customize_index"
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Generate Index Short Name
   Input Values On New Customize Index   Index_Short_Name=${RANDOM_INDEX_SHORT_NAME}
   ...  Index_Full_Name=${RANDOM_INDEX_SHORT_NAME}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=${first_cut-off_date}  
   ...  First_Capped_Date=${first_capped_date}  
   ...  First_Effective_Date=${first_effective_date}
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  May
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=${effective_day}  
   ...  Capped_Day=${capped_day} 
   ...  Cut_Off_Day=${cut_off_day}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully

Create duplicate New index profile wiht customize index
   [Documentation]  [UI] To verify the user can't create New "customize_index" 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Input Values On New Customize Index   Index_Short_Name=${exsit_index_short_name}
   ...  Index_Full_Name=${index_full_name}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=${first_cut-off_date}  
   ...  First_Capped_Date=${first_capped_date}  
   ...  First_Effective_Date=${first_effective_date}
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  May
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=${effective_day}  
   ...  Capped_Day=${capped_day} 
   ...  Cut_Off_Day=${cut_off_day}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Already this index short name in system

Create index profile at First Cut-Off Date is a holiday.
   [Documentation]  [UI] To verify the user can't create index profile 
   ...  at First Cut-Off Date is a holiday.
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Generate Index Short Name
   Input Values On New Customize Index   Index_Short_Name=${RANDOM_INDEX_SHORT_NAME}
   ...  Index_Full_Name=${index_full_name}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=2022-05-07 
   ...  First_Capped_Date=2022-05-10
   ...  First_Effective_Date=2022-05-11
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  May
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=${effective_day}  
   ...  Capped_Day=${capped_day} 
   ...  Cut_Off_Day=${cut_off_day}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   The first cut-off date should not be holiday

Create index profile at First Capped Date is a holiday.
   [Documentation]  [UI] To verify the user can't create index profile
   ...  at First Capped Date is a holiday.
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Generate Index Short Name
   Input Values On New Customize Index   Index_Short_Name=${RANDOM_INDEX_SHORT_NAME}
   ...  Index_Full_Name=${index_full_name}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=2022-05-06 
   ...  First_Capped_Date=2022-05-07
   ...  First_Effective_Date=2022-05-11
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  May
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=${effective_day}  
   ...  Capped_Day=${capped_day} 
   ...  Cut_Off_Day=${cut_off_day}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   The first capped date should not be holiday

Create index profile at first effective date is a holiday
   [Documentation]  [UI] To verify the user can't create index profile 
   ...  at first effective date is a holiday.
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Generate Index Short Name
   Input Values On New Customize Index   Index_Short_Name=${RANDOM_INDEX_SHORT_NAME}
   ...  Index_Full_Name=${index_full_name}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=2022-05-06  
   ...  First_Capped_Date=2022-05-11 
   ...  First_Effective_Date=2022-05-14
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  May
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=${effective_day}  
   ...  Capped_Day=${capped_day} 
   ...  Cut_Off_Day=${cut_off_day}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   The first effective date should not be holiday
   
Create index profile at the rebalance effective date less that the first effective date
   [Documentation]  [UI] To verify the user can't create index profile 
   ...  the rebalance effective date less that the first effective date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   Customize Index
   Generate Index Short Name
   Input Values On New Customize Index   Index_Short_Name=${RANDOM_INDEX_SHORT_NAME}
   ...  Index_Full_Name=${index_full_name}    
   ...  Index_Calculate_Method=${index_calculate_method}   
   ...  Formula=${formula} 
   ...  First_Cut-Off_Date=2024-03-18
   ...  First_Capped_Date=2024-03-18
   ...  First_Effective_Date=2024-03-20
   ...  Base_Index_Value=${base_index_value}  
   ...  Base_TRI_Index_Value=${base_tri_index_value} 
   ...  Status=${status}
   Input Start Month & Year Calculate Value   2024  Mar
   Input Values On Rebalance Profile  Rebalance_Frequency=${rebalance_frequency}  
   ...  Effective_Day=13
   ...  Capped_Day=14
   ...  Cut_Off_Day=15
   Click Save Button
   Verify Alert Message   the rebalance effective date must after the first effective date


