*** Settings ***
Documentation   Verify that can create index profiles and can't create if have any error with "customize_index"
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../api/keywords/index_profile_controller/index_profiles_keywords.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_customize_index_page_keywords.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Index
Test Teardown   Clean Environment   

*** Test Cases ***

Update new index profile wiht customize_index
   [Documentation]  [UI] To verify the user can update "customize_index"
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Generate Index Short Name
   Navigate To Left Main Menu   Index Profile
   Input Auto-Complete Index Short Name Value   index_test_robot_update
   Click Edit Button
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully