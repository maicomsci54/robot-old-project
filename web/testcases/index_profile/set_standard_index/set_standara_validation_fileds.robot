*** Settings ***
Documentation   Verify that mandatory/optional and other condition fields of "SET Standard Index" type
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true} 
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Index
Test Teardown   Clean Environment

*** Variable ***
${msg_error_index_short_name}   The value must not be Blank.
${msg_error_calculate_method}   Select a index calculated method.
${msg_error_formula}    Select a formula.
${msg_error_related_data}  Select a related Data.

*** Test Cases ***

Verify condition and mandatory of each field
   [Documentation]   [UI] Verify condition and mandatory of each fields is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   SET Standard Index
   Click Save Button
   Verify Default Index Type   SET Standard Index
   Verify Error Message Under Text Field   Index_Calculate_Method=${msg_error_calculate_method}
   Input Auto-Complete Index Short Name Value    TECH
   Verify Error Message is Disappear   Index_Short_Name=${msg_error_index_short_name}
   
Verify the relation formula field with index calculate method field
   [Documentation]   [UI] Verify the relation formula field with index calculate method field is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   SET Standard Index
   # If still not select index calculate method will disable formula field
   Verify Text Field Should Be Disabled    Formula
   # If index calculate method is capped will enable formula field and can select a value
   Select Index Calculate Method   Capped
   Verify Text Field Should Be Enabled     Formula
   Select Formula    S05_Percent
   # Changed back to test clear value
   Select Index Calculate Method   Non-Capped
   Verify Text Field Should Be Disabled    Formula

Verify the relation related data field with index calculate method field
   [Documentation]   [UI] Verify the relation related data field with index calculate method field is work correctly
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   SET Standard Index
   # If related data will required field when index calculate method is capped 
   Select Index Calculate Method   Capped
   Click Save Button
   Verify Error Message Under Text Field  Related_Data=${msg_error_related_data}
   # If related data will not required field when index calculate method is non-capped 
   Select Index Calculate Method   Non-Capped
   Verify Error Message is Disappear    Related_Data=${msg_error_related_data}
   # after has changed index calculate method if have value in related data don't clear value
   Select Related Data   adjustmentFactor1
   Sleep  0.5s
   Select Index Calculate Method   Capped
   Verify Value Is Exist In Text field  relatedData    adjustmentFactor1
   