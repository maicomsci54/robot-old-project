*** Settings ***
Documentation   Verify that can create index profiles and can't create if have any error 
Resource    ../../../../api/keywords/common/database_common.robot
Resource    ../../../../api/keywords/index_profile_controller/index_profiles_keywords.robot
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...  AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...  AND    Navigate To Main Menu On Dashboard    Index
...  AND    Connect Database   INDX
...  AND    Check Index ID If Exists Delete Index Profile  ${index_short_name}
Test Teardown   Run keywords   Clean Environment   
...  AND   Check Index ID If Exists Delete Index Profile  ${index_short_name}
...  AND   Disconnect from Database	

*** Variable ***
${index_short_name}   HELTH
${index_full_name}   TEST_ROBOT_INDEX_PROFILE
${formula}  S05_Percent
${relate_data}   sustainabilityScore
${index_calculate_method}   Capped
${dup_index_short_name}   SET50

*** Test Cases ***

Create New index profile wiht SET Standard Index type 
   [Documentation]  [UI] To verify the user create New index profile wiht SET Standard Index type 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   SET Standard Index
   Input Values On New SET Standard Index  Index_Short_Name=${index_short_name}
   ...  Index_Calculate_Method=${index_calculate_method}   Formula=${formula}  Relate_Data=${relate_data}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully

Create duplicate New "SET Standard Index" with index short name
   [Documentation]  [UI] To verify the user can't create duplicate New "SET Standard Index" with index short name
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Index Profile
   Navigate To Main And Sub Menu Bar   Index Profile    New   SET Standard Index
   Input Values On New SET Standard Index  Index_Short_Name=${dup_index_short_name}
   ...  Index_Calculate_Method=${index_calculate_method}   Formula=${formula}  Relate_Data=${relate_data}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Already this index short name in system