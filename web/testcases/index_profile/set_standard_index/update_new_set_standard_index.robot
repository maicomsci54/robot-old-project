*** Settings ***
Documentation   Verify that can update index profiles
Resource    ../../../../api/keywords/common/database_common.robot
Resource    ../../../../web/keywords/common/web_common.robot
Resource    ../../../../web/keywords/login/login_keyword.robot
Resource    ../../../keywords/index_profile/new_set_standard_index_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...  AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...  AND    Navigate To Main Menu On Dashboard    Index
Test Teardown   Clean Environment   

*** Test Cases ***

Update index profile wiht SET Standard Index type 
   [Documentation]  [UI] To verify the user update index profile wiht SET Standard Index type 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Index Profile
   Input Auto-Complete Index Short Name Value   HOME
   Click Edit Button
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message   Save Successfully