*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/instrument/instrument_h_sp_page_keywords.robot
Resource    ../../keywords/instrument/Instrument_h_sp_detail_page_keywords.robot
Resource    ../../../api/keywords/common/date_time_common.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Market Operation
Test Teardown   Clean Environment

*** Variable ***
${less_effective_date}   2020-03-02
${user}   kittiwut

*** Test Cases ***
Create New instrument Halt/SP
   [Documentation]   [UI] Verify that the can create New instrument Halt/SP
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Check And Remove Instrument Name   CGD   ${user}    2023-04-18  2023-04-18
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  CGD
   Input Effective Date On Textbox    2023-04-18
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Verify Search Result   instrument_short_name=CGD   effective_date=2023-04-18  
   ...   upload_date=${current_date}   last_update_by=${user}

Create duplicate instrument name in the same effective date
   [Documentation]   [UI] Verify that the can't create New instrument Halt/SP in the same effective date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Check And Remove Instrument Name   GCAP  ${user}   2023-04-19   2023-04-19
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  GCAP
   Input Effective Date On Textbox    2023-04-19
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Verify Search Result   instrument_short_name=GCAP   effective_date=2023-04-19
   ...   upload_date=${current_date}   last_update_by=${user}
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  GCAP
   Input Effective Date On Textbox    2023-04-19
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Duplicated instrument and Effective date[GCAP1,2023-04-19]

Create instrument Halt/SP with effective date less than current date
   [Documentation]   [UI] Verify that the can't create instrument effective with
   ...  date less than current date
   [Tags]    Regression    Smoke    Sanity   ExcludeRobot 
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  GCAP
   Verify Not Select Effective Date  ${less_effective_date}

Create instrument Halt/SP with effective date is current date
   [Documentation]   [UI] Verify that the can't create instrument with effective date is current date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  GCAP
   Verify Not Select Effective Date  ${current_date}

Create instrument Halt/SP with effective date is holiday
   [Documentation]   [UI] Verify that the can't create instrument with effective date is holiday
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   ${holiday}=  Get Holiday
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  GCAP
   Verify Not Select Effective Date  ${holiday}
   