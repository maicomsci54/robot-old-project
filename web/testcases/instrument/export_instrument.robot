*** Settings ***
Documentation     [UI] Verify that the can delete instrument Halt/SP
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/instrument/instrument_h_sp_page_keywords.robot
Resource    ../../keywords/instrument/Instrument_h_sp_detail_page_keywords.robot
Resource    ../../../api/keywords/common/date_time_common.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...    AND  Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD} 
...    AND  Navigate To Main Menu On Dashboard    Market Operation
...    AND  Navigate To Left Main Menu   Equity    Instrument H/SP
Test Teardown   Clean Environment

*** Test Case ***

Export all instrument Halt/SP
    [Documentation]   [UI] Verify that the user can click ecxport CSV file and verify result correctly
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot  Export
    Clear Default Upload Date
    Click Export File
    Generate File Name By Current Date
    Verify File Download Should Exist  ${FILE_NAME}
    Compare Instrument CSV File With Result On Web  
    Delete Download File