*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/instrument/instrument_h_sp_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  AND  Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}
Test Teardown   Clean Environment

*** Test Cases ***
Search instrument with all criteria
   [Documentation]   [UI] Verify that the search instrument with all criteria
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Input Instrument Search Values    Instrument_Short_Name=CGD     User=kittiwut   uploadDateFrom=2020-05-13  uploadDateTo=2020-05-13
   ...   effectiveDateFrom=2023-04-18   effectiveDateTo=2023-04-18
   Compare Column List With Result Values   instrumentShortName=CGD    uploadDate=2020-05-13  lastUpdateBy=kittiwut   effectiveDate=2023-04-18

Search instrument Halt/SP with instrument short name
   [Documentation]   [UI] Verify that the search instrument Halt/SP with instrument short name
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Input Instrument Search Values  Instrument_Short_Name=CGD
   Compare Column List With Result Values   instrumentShortName=CGD

Search instrument Halt/SP with user
   [Documentation]   [UI] Verify that the search instrument Halt/SP with user
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Input Instrument Search Values   User=kittiwut
   Compare Column List With Result Values  lastUpdateBy=kittiwut
  
Search instrument Halt/SP with uploadDate
   [Documentation]   [UI] Verify that the search instrument Halt/SP with uploadDate
   [Tags]    Regression    Smoke    Sanity   ExcludeRobot
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Input Instrument Search Values  uploadDateFrom=2020-03-18  uploadDateTo=2020-03-18
   Compare Column List With Result Values   uploadDate=2020-03-18

Search instrument Halt/SP with effectiveDate
   [Documentation]   [UI] Verify that the search instrument Halt/SP with effectiveDate
   [Tags]    Regression    Smoke    Sanity   ExcludeRobot
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   Input Instrument Search Values  effectiveDateFrom=2020-03-23   effectiveDateTo=2020-03-23
   Compare Column List With Result Values   effectiveDate=2020-03-23