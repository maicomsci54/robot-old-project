*** Settings ***
Documentation     [UI] Verify that the can delete instrument Halt/SP
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/instrument/instrument_h_sp_page_keywords.robot
Resource    ../../keywords/instrument/Instrument_h_sp_detail_page_keywords.robot
Resource    ../../../api/keywords/common/date_time_common.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...    AND  Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD} 
...    AND  Navigate To Main Menu On Dashboard    Market Operation
...    AND  Navigate To Left Main Menu   Equity    Instrument H/SP
...    AND  Check And Remove Instrument Name   ${instrument_name}   ${user}   ${effective_date}  ${effective_date}
Test Teardown   Clean Environment

*** Variable ***
${instrument_name}   AAV
${effective_date}    2023-04-17
${user}  kittiwut

*** Test Cases ***
Delete instrument Halt/SP
   [Documentation]   [UI] Verify that the can delete instrument Halt/SP
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Navigate To Main And Sub Menu Bar     Instrument H/SP   New
   Input Instrument Short Name On Textbox  ${instrument_name} 
   Input Effective Date On Textbox    2023-04-17
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   Verify Search Result   instrument_short_name=${instrument_name}    effective_date=${effective_date}
   ...   upload_date=${current_date}   last_update_by=${user}
   Press Column On Table   instrumentShortName=${instrument_name} 
   Click Delete Button
   Click Yes Cofirm Button
   Verify Alert Message  Delete Successfully