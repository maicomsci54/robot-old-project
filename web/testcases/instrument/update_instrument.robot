*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/instrument/instrument_h_sp_page_keywords.robot
Resource    ../../keywords/instrument/Instrument_h_sp_detail_page_keywords.robot
Resource    ../../../api/keywords/common/date_time_common.robot
Resource    ../../../api/keywords/common/api_common.robot
Resource    ../../../api/keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  AND  Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
Test Teardown   Clean Environment

*** Test Cases ***
Update instrument Halt/SP
   [Documentation]   [UI] Verify that the can update instrument Halt/SP
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Clear Instrument Halt/SP In List   kittiwut   2023-04-20   2023-04-21
   Clear Instrument Halt/SP In List   kittiwut   2023-04-20   2023-04-21
   Post Create Instrument Halt/SP   { "instrumentId": "TACC_SYMB", "internalId": "TACC", "effectiveDate": "2023-04-20" } 
   Response Correct Code   ${SUCCESS_CODE}
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Verify Search Result   instrument_short_name=TACC_SYMB   effective_date=2023-04-20
   ...   upload_date=${current_date}   last_update_by=kittiwut
   Press Column On Table   instrumentShortName=TACC_SYMB
   Verify Instrument Short Name Field Should Be Disabel
   Clear Default Effective Date
   Input Effective Date On Textbox    2023-04-21
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Update Successfully
   Verify Search Result   instrument_short_name=TACC_SYMB   effective_date=2023-04-21   
   ...   upload_date=${current_date}   last_update_by=kittiwut
   Clear Instrument Halt/SP In List   kittiwut   2023-04-20   2023-04-21

Update instrument Halt/SP with same effective date
   [Documentation]   [UI] Verify that the can update instrument Halt/SP with same effective date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Clear Instrument Halt/SP In List   kittiwut   2023-04-24   2023-04-24
   Post Create Instrument Halt/SP   { "instrumentId": "CBG_SYMB", "internalId": "CBG", "effectiveDate": "2023-04-24" }   
   Response Correct Code   ${SUCCESS_CODE}
   Navigate To Main Menu On Dashboard    Market Operation
   Navigate To Left Main Menu   Equity    Instrument H/SP
   ${current_date}=  Get Date With Format And Increment   %Y-%m-%d 
   Verify Search Result   instrument_short_name=CBG_SYMB   effective_date=2023-04-24
   ...   upload_date=${current_date}   last_update_by=kittiwut
   Press Column On Table   instrumentShortName=CBG_SYMB
   Verify Instrument Short Name Field Should Be Disabel
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  The new effective date same as old effective date !!!
   Clear Instrument Halt/SP In List   kittiwut   2023-04-24   2023-04-24