*** Settings ***
Documentation     Verify Can Create User 
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../../web/keywords/user_management/user_manage_page_keywords.robot
Resource    ../../../web/keywords/user_management/user_manage_detail_page_keywords.robot
Test Setup  Run keywords  Open Browser With Option    ${ANT_URL}    headless_mode=${true}
...   AND   Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}
...   AND   Navigate To Main Menu On Dashboard    User Management
...   AND   Navigate To Left Main Menu   User Management
Test Teardown   Clean Environment

*** Variables ***
${existing_user}     dolruethai

*** Test Cases ***
Create new user management
   [Documentation]   [UI] Verify can management create user 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Delete User   chanapai
   Navigate To Main And Sub Menu Bar   User Management   New
   Input Username     chanapai
   Select Type   User
   Click Status  ACTIVE
   Click Save Button
   Verify Message On Confirmation Popup    Do you want to save this user ( chanapai ) ?
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   Close Popup QR Code

Update user management
   [Documentation]   [UI] Verify can management update user 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Input Username     ${existing_user}
   Press Column On Table   username=${existing_user} 
   Click Save Button
   Verify Message On Confirmation Popup    Do you want to save this user ( ${existing_user} ) ?
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   
Delete user management
   [Documentation]   [UI] Verify can management delete user
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Delete User   phannee
   Navigate To Main And Sub Menu Bar   User Management   New
   Input Username     phannee
   Select Type   User
   Click Status  ACTIVE
   Click Save Button
   Verify Message On Confirmation Popup    Do you want to save this user ( phannee ) ?
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully
   Close Popup QR Code
   Input Username     phannee
   Press Column On Table   username=phannee
   Click Delete Button
   Verify Message On Confirmation Popup    Do you want to delete this user ( phannee ) ?
   Click Yes Cofirm Button
   Verify Alert Message  Delete Successfully

Create user has already exists
   [Documentation]    Create User Has Already Exists
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Navigate To Main And Sub Menu Bar   User Management   New
   Input Username    ${existing_user}
   Select Type   User
   Click Status  ACTIVE
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  This username already exited.