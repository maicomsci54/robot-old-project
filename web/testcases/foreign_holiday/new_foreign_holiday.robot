*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/foreign_holiday/new_foreign_holiday_page_keywords.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Market Operation
...   AND    Navigate To Left Main Menu   Equity    Foreign Holiday
...   AND    Check If Exist Found Holiday And Remove    ${holiday_date}    ${country}
Test Teardown   Clean Environment

*** Variable ***
${holiday_date}   2021-12-07
${country}   VIETNAM
${holiday_date_dup}   2021-12-08

*** Test Case ***

Verify that the user can add foreign holiday 
   [Documentation]  [UI] To Verify that the user can add foreign holiday 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Main And Sub Menu Bar     Foreign Holiday   New
   Select Country  ${country}
   Select Holiday Date  ${holiday_date}
   Input Description    TEST_ROBOT
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully 

Verify that the user can't add foreign holiday duplicate with country and holiday date
   [Documentation]  [UI] To Verify that the user can't add foreign holiday duplicate 
   ...  with country and holiday date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Main And Sub Menu Bar     Foreign Holiday   New
   Select Country  ${country}
   Select Holiday Date  ${holiday_date_dup} 
   Input Description    data_test_dup_update_robot
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Duplicated Foreign Holiday and Country[${holiday_date_dup},${country}]