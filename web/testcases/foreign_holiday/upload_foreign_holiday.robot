*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/foreign_holiday/new_foreign_holiday_page_keywords.robot
Resource    ../../keywords/foreign_holiday/upload_foreign_holiday_page_keywords.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Market Operation
Test Teardown   Clean Environment

*** Variable ***
${holiday_date}   2021-12-07
${country}   LAO
${holiday_date_dup}   2021-12-08

${invalid_date_path}   ${CURDIR}/file_data/invalid_date.csv
${not_macth_path}     ${CURDIR}/file_data/holiday_not_macth.csv
${dup_holiday_path}     ${CURDIR}/file_data/dup_holiday.csv
${holiday_path}     ${CURDIR}/file_data/holiday.csv
${emtyp_date_path}    ${CURDIR}/file_data/emtyp_date.csv
${sunday_saturday_path}    ${CURDIR}/file_data/sunday_saturday.csv
${valid_date_path}  ${CURDIR}/file_data/valid_date.csv

*** Test Case ***

Verify upload file invalid date format
   [Documentation]  [UI] To Verify upload file invalid date format
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2020
   Select Country  ${country}
   Upload Foreign holiday File  ${invalid_date_path}
   Compare Pre-Verify Should Be Equal All List  Holiday date invalid format, format should be "yyyy-mm-dd".

Verify upload file year and date don't macth in file
   [Documentation]  [UI] To Verify upload file invalid date format
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2021
   Select Country  ${country}
   Upload Foreign holiday File  ${not_macth_path}
   Compare Pre-Verify Should Be Equal All List  Year and Holiday don’t match.

Verify upload file duplicate date same file 
   [Documentation]  [UI] To Verify upload file duplicate date same file 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2021
   Select Country  ${country}
   Upload Foreign holiday File  ${dup_holiday_path}
   Compare Pre-Verify Should Be Equal By Index     Holiday date is duplicated    1

Verify upload file duplicate file same year in system
   [Documentation]  [UI] To Verify upload file duplicate date same file 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2021
   Select Country  VIETNAM
   Upload Foreign holiday File  ${holiday_path}
   Compare Pre-Verify Should Be Equal By Index     Successfully    0
   Click Upload All Button
   Verify Alert Message On Popup  Duplicated : Year 2021, VIETNAM. Do you want to replace the existing calendar?

Verify upload file invalid emtyp date and description
   [Documentation]  [UI] To Verify upload file invalid emtyp date and description
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2020
   Select Country  ${country} 
   Upload Foreign holiday File  ${emtyp_date_path}
   Compare Pre-Verify Should Be Equal By Index     Holiday date invalid format, format should be "yyyy-mm-dd".    0
   Compare Pre-Verify Should Be Equal By Index     Successfully    1

Verify upload file on Sunday or Saturday
   [Documentation]  [UI] To Verify upload file on Sunday or Saturday
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File
   Select Year  2020
   Select Country  ${country} 
   Upload Foreign holiday File  ${sunday_saturday_path}
   Compare Pre-Verify Should Be Equal By Index   Holiday date is saturday.    0
   Compare Pre-Verify Should Be Equal By Index   Holiday date is sunday.  1

Verify can upload file valid 
   [Documentation]  [UI] To Verify can upload file valid 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Navigate To Main And Sub Menu Bar     Foreign Holiday   Upload File 
   Select Year  2021
   Select Country  LAO
   Upload Foreign holiday File  ${valid_date_path}
   Compare Pre-Verify Should Be Equal By Index     Successfully    0
   Compare Upload Status Should Be Equal By Index   ${empty}    0
   Click Upload All Button  
   Click Yes Cofirm Button 
   Verify Alert Message  Upload Successfully 
   Compare Upload Status Should Be Equal By Index  Successfully    0