*** Settings ***
Documentation     Verify that the search result correctly
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/login/login_keyword.robot
Resource    ../../keywords/foreign_holiday/new_foreign_holiday_page_keywords.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_page_keywords.robot
Test Setup    Run keywords   Open Browser With Option    ${ANT_URL}    headless_mode=${true}  
...   AND    Login To ANT Website    ${ANT_USERNAME}    ${ANT_PASSWORD}  
...   AND    Navigate To Main Menu On Dashboard    Market Operation
Test Teardown   Clean Environment

*** Variable ***
${country}   VIETNAM
${holiday_date}   2021-12-09
${holiday_date_dup}   2021-12-20
*** Test Case ***

Verify that the user can edit foreign holiday 
   [Documentation]  [UI] To Verify that the user can edit foreign holiday 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Select Holiday Date From  ${holiday_date}
   Select Holiday Date To    ${holiday_date}
   Select Country  ${country}
   Press Column On Table   country=${country}
   Input Description    Automate
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Save Successfully 

Verify that the user can't edit foreign holiday duplicate with country and holiday date
   [Documentation]  [UI] To Verify that the user can't edit foreign holiday 
   ...  duplicate with country and holiday date
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Navigate To Left Main Menu   Equity    Foreign Holiday
   Select Holiday Date From  ${holiday_date_dup}
   Select Holiday Date To    ${holiday_date_dup}
   Select Country  ${country}
   Press Column On Table   country=${country}
   Select Holiday Date  ${holiday_date}
   Click Save Button
   Click Yes Cofirm Button
   Verify Alert Message  Duplicated Foreign Holiday and Country[${holiday_date},${country}]