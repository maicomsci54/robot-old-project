*** Variables ***
${txt_username_field}    //div[text()="Username : "]/..//input[@placeholder="Please fill username"]
${lbl_search_username}    //span[text()="_DYNAMIC_0"]  
${ddl_type}    //p-dropdown[@id="type"]
${ddl_type_value}    //span[text()="_DYNAMIC_0"]
${rdo_status_value}   //input[@id="_DYNAMIC_0"]/../../..//div[@class="ui-radiobutton ui-widget"]
${lbl_msg_popup}  //span[text()="_DYNAMIC_0"]
${pup_qr_code}       //span[@class="pi pi-times"]
${lbl_column_username}   //div[@col-id="username" and text()='_DYNAMIC_0']