*** Variables ***
${txt_search_date_field}    //p-calendar[@formcontrolname="_DYNAMIC_0"]//input
${txt_search_text_field}    //input[@placeholder="_DYNAMIC_0"]
${btn_export_excel}   //button[@class="btn-excel"]
${tb_result_search}     //div[text()="_DYNAMIC_0"]/..//div[text()="_DYNAMIC_1"]/..//div[contains(text(),'_DYNAMIC_2')]/..//div[text()="_DYNAMIC_3"]
${btn_clear_date}     //span[text()="Clear"]
${clm_result_text}    //div[@col-id="_DYNAMIC_0"]
${btn_upload_date_from}    //p-calendar[@formcontrolname="uploadDateFrom"]//button
${btn_upload_date_to}   //p-calendar[@formcontrolname="uploadDateTo"]//button
