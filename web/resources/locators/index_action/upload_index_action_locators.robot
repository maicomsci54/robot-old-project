*** Variable ***
${ddl_index_type}  //p-dropdown[@id="indexType"]
${lbl_index_type_value}  //span[text()="_DYNAMIC_0"]
${tbx_effective_date}   //input[@placeholder="Effective Date"]
${btn_browse}   //input[@id="browseIndex"]
${lbl_pre_verify_status}  //div[@class="ag-center-cols-clipper"]//div[4]
${lbl_upload_status}  //div[@class="ag-center-cols-clipper"]//div[5]
${btn_upload_all}  //button[@id="buttonSave"]