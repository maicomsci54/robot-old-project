*** Variable ***
${cbx_all}   (//span[@class="ag-icon ag-icon-checkbox-unchecked"])[last()]
${lbl_index_short_name}  //div[@col-id="indexShortName" and text()="_DYNAMIC_0"]
${cbx_status}   //label[text()="_DYNAMIC_0"]/../..//div[contains(@class, 'ui-chkbox-box')]
${btn_approve}  //button[contains(text(),'Approve')]
${btn_capped}   //button[contains(text(),'Capped')]