*** Variable ***
# Customize Index
${tbx_index_short_name}   //input[@id="indexShortName"ช
${tbx_first_cut_off_date}   //p-calendar[@id="firstCutOffDate"]/span/input
${tbx_first_capped_date}  //p-calendar[@id="firstCappedDate"]/span/input
${tbx_first_effective_date}    //p-calendar[@id="firstEffectiveDate"]/span/input
${tbx_first_base_index_value}   //input[@id="baseIndexValue"]
${tbx_base_tri_index_value}   //input[@id="baseTRIIndexValue"]
${ddl_status}   //p-dropdown[@id="indexStatusId"]

#Rebalance Profile
${tbx_effective_day}   //input[@id="effectiveDay"]
${tbx_capped_day}     //input[@id="cappedDay"]
${tbx_cut_off_day}   //input[@id="cutOffDay"]
${ddl_rebalance_frequency}    //p-dropdown[@id="rebalanceFrequency"]
${tbx_start_month_year_calculate}  //p-calendar[@id="startMonthYearCalculate"]
${ddl_select_years}   //*[@id="startMonthYearCalculate"]/span/div/div[1]/div/div/select
${lbl_monuth}   //a[contains(text(),'_DYNAMIC_0')]