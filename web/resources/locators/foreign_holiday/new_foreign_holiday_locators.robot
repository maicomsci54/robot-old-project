*** Variable ***
${ddl_country}   //p-dropdown[@id="country"]
${txb_search_country}  //p-dropdown[@id="country"]//input[contains(@class, 'ui-dropdown-filter')]
${lbl_country_value}   //li[@aria-label="_DYNAMIC_0"]
${txb_description}   //input[@id="description"]
${txb_holiday}  //p-calendar[@id="holidayDate"]//input
${col_pre_verify_status}    //div[@col-id="preVerifyStatus"]
${btn_upload_all}   //button[@id="buttonSave"]
${col_upload_status}    //div[@col-id="uploadStatus"]
