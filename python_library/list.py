#Function sort list with ascending sort and descending sort
def Sort_List_With_Options (item_list,option):
    if   option == 'ASC':
        item_list.sort()
    elif option == 'DESC':
        item_list.sort(reverse = True) 
    return  (item_list)

def Round_Decimal_In_List (item_list,places):
    places = int(places)
    convert_float = [float(i) for i in item_list]
    round_decimal = [round(num, places) for num in convert_float]
    return  (round_decimal)

def Convert_String_To_Float (item_list):
    convert_float = [float(i) for i in item_list]
    return  (convert_float)

def Count_String (string):
    count = string.count(string)
    return  (count)