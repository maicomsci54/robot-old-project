*** Settings ***
Documentation    Verify that can create foreign holiday
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_controller.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***

*** Test Case ***

Get foreign holiday    
    [Documentation]  [API] Verify that get foreign holiday  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Foreign Holiday
    Response Correct Code   ${SUCCESS_CODE}

Get countries
    [Documentation]  [API] Verify that get countries
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Countries
    Response Correct Code   ${SUCCESS_CODE}

Get holiday status
    [Documentation]  [API] Verify that get holiday status
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Holiday Status
    Response Correct Code   ${SUCCESS_CODE}