*** Settings ***
Documentation    Verify that update foreign holiday
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_controller.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${holiday_date}   2021-06-10
${holiday_date_dup}   2021-12-09 
${country_id}    4

*** Test Case ***

Pre verify upload foreign holiday duplicate
    [Documentation]     [API] Pre verify upload foreign holiday duplicate
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Pre Verify Upload Foreign Holiday   [{"countryId":4,"holidayDate":"${holiday_date_dup}","description":" Test_Holiday"}]
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value  errorMessage   Duplicated : Year 2021, VIETNAM. Do you want to replace the existing calendar?