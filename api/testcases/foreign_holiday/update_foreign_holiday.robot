*** Settings ***
Documentation    Verify that update foreign holiday
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_controller.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${holiday_date}   2021-06-14
${holiday_date_create}   2021-06-16
${holiday_date_dup}    2021-12-09
${country_id}    4

*** Test Case ***

Update foreign holiday
    [Documentation]     [API] Verify that update foreign holiday
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Foreign Holiday    {"countryId":${country_id},"holidayDate":"${holiday_date}","description":"api_test"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Foreign Holiday ID  
    Get Foreign Holiday By ID  ${FOREIGN_HOLIDAY_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   countryId     ${${country_id}}
    Response Should Contain Property With Value   holidayDate   ${holiday_date} 
    Response Should Contain Property With Value   description   api_test
    Put Update Foreign Holiday  ${FOREIGN_HOLIDAY_ID}   {"countryId":${country_id},"holidayDate":"${holiday_date}","description":"api_test_update"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Foreign Holiday By ID  ${FOREIGN_HOLIDAY_ID}
    Response Should Contain Property With Value   countryId     ${${country_id}}
    Response Should Contain Property With Value   holidayDate   ${holiday_date} 
    Response Should Contain Property With Value   description   api_test_update
    Patch Delete Foreign Holiday  {"ids":[${FOREIGN_HOLIDAY_ID}]}
    Response Correct Code   ${SUCCESS_CODE}

Update foreign holiday duplicate
    [Documentation]     [API] Verify that update foreign holiday duplicate
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Foreign Holiday    {"countryId":${country_id},"holidayDate":"${holiday_date_create}","description":"api_test"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Foreign Holiday ID  
    Get Foreign Holiday By ID  ${FOREIGN_HOLIDAY_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   countryId     ${${country_id}}
    Response Should Contain Property With Value   holidayDate   ${holiday_date_create} 
    Response Should Contain Property With Value   description   api_test
    Put Update Foreign Holiday  ${FOREIGN_HOLIDAY_ID}   {"countryId":${country_id},"holidayDate":"${holiday_date_dup}","description":"api_test_update"}
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value  errorMessage   Duplicated Foreign Holiday and Country[${holiday_date_dup},VIETNAM]
    Get Foreign Holiday By ID  ${FOREIGN_HOLIDAY_ID}
    Response Should Contain Property With Value   countryId     ${${country_id}}
    Response Should Contain Property With Value   holidayDate   ${holiday_date_create} 
    Response Should Contain Property With Value   description   api_test
    Patch Delete Foreign Holiday  {"ids":[${FOREIGN_HOLIDAY_ID}]}
    Response Correct Code   ${SUCCESS_CODE}