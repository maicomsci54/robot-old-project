*** Settings ***
Documentation    Verify that create foreign holiday
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/foreign_holiday/foreign_holiday_controller.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${holiday_date}   2021-06-15
${country_id}    4

*** Test Case ***

Create foreign holiday
    [Documentation]     [API] Verify that create foreign holiday
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Foreign Holiday    {"countryId":${country_id},"holidayDate":"${holiday_date}","description":"api_test"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Foreign Holiday ID  
    Get Foreign Holiday By ID  ${FOREIGN_HOLIDAY_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   countryId     ${${country_id}}
    Response Should Contain Property With Value   holidayDate   ${holiday_date} 
    Response Should Contain Property With Value   description   api_test
    Patch Delete Foreign Holiday  {"ids":[${FOREIGN_HOLIDAY_ID}]}
    Response Correct Code   ${SUCCESS_CODE}

Create foreign holiday duplicate
    [Documentation]     [API] Verify that create foreign holiday duplicate
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Foreign Holiday    {"countryId":${country_id},"holidayDate":"${holiday_date}","description":"api_test"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Foreign Holiday ID  
    Post Create Foreign Holiday    {"countryId":${country_id},"holidayDate":"${holiday_date}","description":"api_test"}
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value  errorMessage   Duplicated Foreign Holiday and Country[${holiday_date},VIETNAM]
    Patch Delete Foreign Holiday  {"ids":[${FOREIGN_HOLIDAY_ID}]}
    Response Correct Code   ${SUCCESS_CODE}