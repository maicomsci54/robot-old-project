*** Settings ***
Documentation   Verify that GetMktActions work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${USERNAME_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Get MKT actions  
    [Documentation]     [API] GetMktActions  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MKT actions
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   [0].id     ${1}
    Response Should Contain Property With Value   [0].name   ENABLE
    Response Should Contain Property With Value   [1].id     ${2}
    Response Should Contain Property With Value   [1].name   DISABLE