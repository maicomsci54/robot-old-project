*** Settings ***
Documentation   Verify that GetUserActions work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${USERNAME_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Get user actions
    [Documentation]     [API] GetUserActions
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get User Actions
    Response Correct Code   ${SUCCESS_CODE}