*** Settings ***
Documentation   To make sure that user action API is still working with othor actions
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${USERNAME_MARKET}  AND   Get Min Effective Date
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
#data for create
${username}   D0001_CU1
${effective_time}  03:00
${action_id}  1
${previous_date}  2020-02-18
#data for update
${username_update}   D0001_CU1
${action_id_update}  2

*** Test Cases ***
Create user actions
    [Documentation]     [API] Verify that can create on user actions  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create User Action   { "effectiveDate": "${EFFECTIVE_DATE}", "effectiveTime": "${effective_time}", "mktActionId": 1,"remark": "Test automate", "username": "${username}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Job Status  1
    Get Last User ID  .id
    Get User Action By ID   ${LAST_USER_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   .username       ${username} 
    Response Should Contain Property With Value   .mktActionId    ${1}
    Response Should Contain Property With Value   .effectiveDate  ${EFFECTIVE_DATE}
    Response Should Contain Property With Value   .effectiveTime  ${effective_time}
    Response Should Contain Property With Value   .remark    Test automate
    Response Should Contain Property With Value   .jobStatusName  PENDING
    Response Should Contain Property With Value   .jobStatusId    ${1}
    Response Should Contain Property With Value   .responseBody   ${null}
    Patch Cancel User Action   ${LAST_USER_ID}  
    Response Correct Code    ${SUCCESS_CODE}

Update user actions
    [Documentation]     [API] Verify that can update on user actions  
    Post Create User Action   { "effectiveDate": "${EFFECTIVE_DATE}", "effectiveTime": "${effective_time}", "mktActionId": 1,"remark": "Test automate", "username": "${username}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Job Status  1
    Get Last User ID  .id
    Put Update User Action  ${LAST_USER_ID}   { "effectiveDate": "${EFFECTIVE_DATE}", "effectiveTime": "${effective_time}", "mktActionId": ${action_id_update},"remark": "Test Automate Update", "username": "${username_update}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get User Action By ID   ${LAST_USER_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   .username       ${username_update}
    Response Should Contain Property With Value   .mktActionId    ${2}
    Response Should Contain Property With Value   .effectiveDate  ${EFFECTIVE_DATE}
    Response Should Contain Property With Value   .effectiveTime  ${effective_time}
    Response Should Contain Property With Value   .remark    Test Automate Update
    Response Should Contain Property With Value   .jobStatusName  PENDING
    Response Should Contain Property With Value   .jobStatusId    ${1}
    Response Should Contain Property With Value   .responseBody   ${null}
    Patch Cancel User Action   ${LAST_USER_ID}  
    Response Correct Code    ${SUCCESS_CODE}

Create user actions at previous effective date
    [Documentation]     [API] Verify that can create on user actions  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create User Action   { "effectiveDate": "${previous_date}", "effectiveTime": "${effective_time}", "mktActionId": 1,"remark": "Test automate", "username": "${username}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   .status        ${400}
    Response Should Contain Property With Value   .errorMessage  The effective date and time was passed. Please select effective date to next business date.
    
Delete user actions
    [Documentation]     [API] Verify that can delete on user actions  
    Post Create User Action   { "effectiveDate": "${EFFECTIVE_DATE}", "effectiveTime": "${effective_time}", "mktActionId": 1,"remark": "Test automate", "username": "${username}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Job Status  1
    Get Last User ID  .id
    Patch Cancel User Action   ${LAST_USER_ID}
    Response Correct Code      ${SUCCESS_CODE}
    Get User Action By ID      ${LAST_USER_ID}
    Response Correct Code      ${SUCCESS_CODE}
    Response Should Contain Property With Value   .jobStatusName  CANCELED