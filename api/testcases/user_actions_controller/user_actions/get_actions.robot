*** Settings ***
Documentation    Verify that GetActions work correctly 
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${USERNAME_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Get actions  
    [Documentation]   [API] GetActions  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get actions
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   [0].id     ${1}
    Response Should Contain Property With Value   [0].name   ADDED
    Response Should Contain Property With Value   [1].id     ${2}
    Response Should Contain Property With Value   [1].name   MODIFIED
    Response Should Contain Property With Value   [2].id     ${3}
    Response Should Contain Property With Value   [2].name   CANCELED
    Response Should Contain Property With Value   [3].id     ${4}
    Response Should Contain Property With Value   [3].name   DELETED