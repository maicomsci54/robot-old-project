*** Settings ***
Documentation     Verify that getMinEffectiveDate work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${USERNAME_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${effective_date_regex}   (?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:
...  (?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))
${effective_time_regex}   ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$

*** Test Cases ***
Get min effective date
    [Documentation]     [API] Verify that getMinEffectiveDate work correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Response Correct Code   ${SUCCESS_CODE}
    ${effective_date}=  Get Future Date   1 Day   %Y-%m-%d
    Response Should Contain Property With Value     .effectiveDate   ${effective_date}