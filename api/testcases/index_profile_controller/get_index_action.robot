*** Settings ***
Documentation   Verify Get action API can get and retrun correct code.
Resource    ../../resources/init.robot
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/index_profile_controller/index_profiles_keywords.robot
Test Setup   Run Keywords   Create Index Action Gateway Session  AND  Generate ANT Gateway Header   ${USERNAME_MARKET}    ${PASSWORD_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get index rebalance frequency
    [Documentation]     [API]  Verify Get Index Rebalance Frequency 
    [Tags]    Regression    Smoke    Sanity
    Get Index Rebalance Frequency
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With All Values  .name
    ...  every 1 month
    ...  every 2 months
    ...  every 3 months
    ...  every 4 months
    ...  every 6 months
    ...  every 12 months

Get index types
    [Documentation]     [API]  Verify Get index types 
    [Tags]    Regression    Smoke    Sanity
    Get Index Types
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With All Values  .name
    ...  Customize Index  
    ...  SET Standard Index

Get index profile names
    [Documentation]     [API]  Verify Get index profile name
    [Tags]    Regression    Smoke    Sanity
    Get Index Profile Names
    Response Correct Code    ${SUCCESS_CODE}

Get index profile QS names
    [Documentation]     [API]  Verify Get  index profile QS names
    [Tags]    Regression    Smoke    Sanity
    Get Index Profile QS Names
    Response Correct Code    ${SUCCESS_CODE}

    
