*** Settings ***
Documentation   To make sure that index profile with standard index  API is still working with othor actions
Resource    ../../../resources/init.robot
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/index_profile_controller/index_profiles_keywords.robot
Test Setup   Run Keywords   Create Index Action Gateway Session  AND  Generate ANT Gateway Header   ${USERNAME_MARKET}    ${PASSWORD_MARKET}
...  AND  Connect Database   INDX
...  AND  Check Index ID If Exists Delete Index Profile  ${index_short_name}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions  
...  AND  Check Index ID If Exists Delete Index Profile  ${index_short_name}
...  AND  Disconnect from Database	

*** Variable ***
${index_type}   2
${index_short_name}  FIN
${index_composition_id}  1043
${index_full_name}  Test_Create_API
${exsit_index_short_name}   HOME
${index_profile_id}  82

*** Test Case *** 
Create new index profile of standard index
    [Documentation]  [API]  Verify that user can be create new index profile 
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Post Create Index Profile Standard Index  {"indexTypeId":${index_type},"indexShortName":"${index_short_name}","indexCompositionId":${index_composition_id},"indexFullName":"${index_full_name}","indexCalculateMethodId":4,"formulaId":1,"relatedDataId":1}
    Get Index Profile ID   
    Response Correct Code     ${SUCCESS_CODE}
    Get Index Profile Standard Index   ${INDEX_PROFILE_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value    indexShortName    ${index_short_name}
    Response Should Contain Property With Value    indexFullName     ${index_full_name}
    Response Should Contain Property With Value    calculateMethod.name  Capped

Create duplicate new index profile of standard index
    [Documentation]     [API]  Verify that user can't create new index profile if index short name is exist
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Post Create Index Profile Standard Index  {"indexTypeId":${index_type},"indexShortName":"${exsit_index_short_name}","indexCompositionId":1047,"indexFullName":"RTEE","indexCalculateMethodId":5,"formulaId":null,"relatedDataId":null}
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    errorMessage    Already this index short name in system

Create index profile doesn’t exist in system 
    [Documentation]     [API]  Verify that user can't create new index profile if index short name is exist
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Post Create Index Profile Standard Index  {"indexTypeId":${index_type},"indexShortName":"XXXX","indexCompositionId":1047,"indexFullName":"RTEE","indexCalculateMethodId":5,"formulaId":null,"relatedDataId":null}
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    errorMessage   This index short name doesn’t exist in system.

Update index profile of standard index
    [Documentation]     [API]  Verify that user can be update new index profile 
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Generate Index Short Name
    Put Update Index Profile Standard Index  ${index_profile_id}  {"id":${index_profile_id} ,"indexTypeId":2,"indexShortName":"${exsit_index_short_name}","indexCompositionId":1047,"indexFullName":"${RANDOM_INDEX_SHORT_NAME}","indexCalculateMethodId":4,"formulaId":1,"relatedDataId":4}  
    Response Correct Code     ${SUCCESS_CODE}
    Get Index Profile Standard Index   ${index_profile_id}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value    indexShortName    ${exsit_index_short_name}
    Response Should Contain Property With Value    indexFullName     ${RANDOM_INDEX_SHORT_NAME}
    Response Should Contain Property With Value    calculateMethod.name  Capped