*** Settings ***
Documentation   To make sure that index profile with customize index API is still working with othor actions
Resource    ../../../resources/init.robot
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/index_profile_controller/index_profiles_keywords.robot
Test Setup   Run Keywords   Create Index Action Gateway Session  AND  Generate ANT Gateway Header   ${USERNAME_MARKET}    ${PASSWORD_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${create_index_profile_path}   ../../resources/testdata/index_profile/create_index_profile.json

*** Test Case ***
Create new index action of customize index
    [Documentation]   [API]  Verify that user can be create new index profile 
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Read Dummy Json From File  ${create_index_profile_path}
    Generate Index Short Name
    Update Dummy Json Value Data  $.indexShortName   ${RANDOM_INDEX_SHORT_NAME}
    Post Create Index Profile Customize Index  ${JSON_DUMMY_DATA}
    Response Correct Code     ${SUCCESS_CODE}
    Get Index Profile ID  
    Get Index Profile Customize Index   ${INDEX_PROFILE_ID}
    Response Correct Code     ${SUCCESS_CODE}
    Response Should Contain Property With Value    indexShortName    ${RANDOM_INDEX_SHORT_NAME}
    Response Should Contain Property With Value    indexFullName     Automate_Cus_Test
    Response Should Contain Property With Value    calculateMethod.name  Market Cap Weighted
    Response Should Contain Property With Value    firstCutOffDate    2022-02-02
    Response Should Contain Property With Value    firstCappedDate    2022-02-03
    Response Should Contain Property With Value    firstEffectiveDate  2022-02-04
    Response Should Contain Property With Value    startMonthYearCalculate   2022-02
    Response Should Contain Property With Value    effectiveDay    ${11}
    Response Should Contain Property With Value    cutOffDay    ${14}
    Response Should Contain Property With Value    cappedDay    ${11}
    Response Should Contain Property With Value    caStatus  PENDING

Create duplicate new index action 
    [Documentation]     [API]  Verify that user can't create new index action if index short name is exist
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Read Dummy Json From File  ${create_index_profile_path}
    Generate Index Short Name
    Update Dummy Json Value Data  $.indexShortName   index_Robot_Y8tzw
    Post Create Index Profile Customize Index  ${JSON_DUMMY_DATA}
    Response Correct Code     ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    status    ${400}
    Response Should Contain Property With Value    errorMessage   Already this index short name in system

Create index profile at firstCutOffDatef Date is a holiday.
    [Documentation]     [API]  Verify that user can't create index profile at First Cut-Off Date is a holiday.
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Read Dummy Json From File  ${create_index_profile_path}
    Generate Index Short Name
    Update Dummy Json Value Data  $.indexShortName   ${RANDOM_INDEX_SHORT_NAME}
    Update Dummy Json Value Data  $.firstCutOffDate   2022-02-05
    Update Dummy Json Value Data  $.firstCappedDate   2022-02-08
    Update Dummy Json Value Data  $.firstEffectiveDate  2022-02-09
    Post Create Index Profile Customize Index  ${JSON_DUMMY_DATA}
    Response Correct Code     ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    status    ${400}
    Response Should Contain Property With Value    errorMessage  The first cut-off date should not be holiday

Create index profile at firstCappedDate is a holiday.
    [Documentation]     [API]  Verify that user can't create index profile at firstCappedDate is a holiday.
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Read Dummy Json From File  ${create_index_profile_path}
    Generate Index Short Name
    Update Dummy Json Value Data  $.indexShortName   ${RANDOM_INDEX_SHORT_NAME}
    Update Dummy Json Value Data  $.firstCutOffDate   2022-02-04
    Update Dummy Json Value Data  $.firstCappedDate   2022-02-06
    Update Dummy Json Value Data  $.firstEffectiveDate  2022-02-09
    Post Create Index Profile Customize Index  ${JSON_DUMMY_DATA}
    Response Correct Code     ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    status    ${400}
    Response Should Contain Property With Value    errorMessage  The first capped date should not be holiday

Create index profile at firstEffectiveDate is a holiday.
    [Documentation]     [API]  Verify that user can't create index profile at firstEffectiveDate is a holiday.
    [Tags]    Regression    Smoke    Sanity   ExcludeRobot
    Read Dummy Json From File  ${create_index_profile_path}
    Generate Index Short Name
    Update Dummy Json Value Data  $.indexShortName   ${RANDOM_INDEX_SHORT_NAME}
    Update Dummy Json Value Data  $.firstCutOffDate   2022-02-04
    Update Dummy Json Value Data  $.firstCappedDate   2022-02-09
    Update Dummy Json Value Data  $.firstEffectiveDate  2022-02-12
    Post Create Index Profile Customize Index  ${JSON_DUMMY_DATA}
    Response Correct Code     ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    status    ${400}
    Response Should Contain Property With Value    errorMessage  The first effective date should not be holiday

