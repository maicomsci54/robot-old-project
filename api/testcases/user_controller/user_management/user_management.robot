*** Settings ***
Documentation     Verify search users management detail and verify fromat  POST, GET, Patch, DELETE
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_management/user_management_keywords.robot
Test Setup    Run Keywords   Generate ANT Gateway Header   ${USERNAME_ADMIN}   ${PASSWORD_ADMIN}  AND  Clear User Data  ${new_user}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
#data for create
${id}  3
${name}  indexcus_indexprofile_read
${status}  ACTIVE
${type}    false
${new_user}  chinthad
${existing_user}  jirakitr
#data for update
${update_status}  INACTIVE
${update_type}    true

*** Test Cases ***
Create new user management
   [Documentation]   [API] Verify can management create user
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Post Create Users  { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${status}", "type": ${type}, "username": "${new_user}", "isBypass": true }
   Response Correct Code   ${CREATED_CODE}
   Get User ID
   Get Users Management   username=${new_user}
   Response Correct Code   ${SUCCESS_CODE}
   Response Should Contain Property With Integer Value  .id
   Response Should Contain Property With Value    .username    ${new_user}
   Response Should Contain Property With Value    .status      ${status}
   Delete Users By ID   ${TEST_VARIABLE_USER_ID}
   Response Correct Code   ${SUCCESS_CODE}

Update user management
   [Documentation]  [API] Verify can management update user
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Post Create Users  { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${status}", "type": ${type}, "username": "${new_user}", "isBypass": true }
   Response Correct Code   ${CREATED_CODE}
   Get User ID
   Get Users Management   username=${new_user}
   Response Correct Code   ${SUCCESS_CODE}
   Patch Users By ID    ${TEST_VARIABLE_USER_ID}    { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${update_status}", "type": ${update_type}, "username": "${new_user}", "isBypass": true } 
   Response Correct Code   ${SUCCESS_CODE}
   Response Should Be Empty Body
   Get Users Management   username=${new_user}
   Response Should Contain Property With Integer Value  .id
   Response Should Contain Property With Value    .username    ${new_user} 
   Response Should Contain Property With Value    .status      ${update_status}
   Response Correct Code   ${SUCCESS_CODE}
   Delete Users By ID   ${TEST_VARIABLE_USER_ID}
   Response Correct Code   ${SUCCESS_CODE}
   
Delete user management
   [Documentation]  [API] Verify can management delete user
   [Tags]    Regression    Smoke    Sanity   IncludeRobot 
   Post Create Users  { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${status}", "type": ${type}, "username": "${new_user}", "isBypass": true }
   Response Correct Code   ${CREATED_CODE}
   Get User ID
   Get Users Management  username=${new_user}
   Response Correct Code   ${SUCCESS_CODE}
   Delete Users By ID      ${TEST_VARIABLE_USER_ID}
   Response Correct Code   ${SUCCESS_CODE}
   Response Should Be Empty Body
   Get Users Management   username=${new_user}
   Response Should Be Empty

Create user has user already exists
   [Documentation]    [API] Verify that can't create a user has already exists
   [Tags]    Regression    Smoke    Sanity   IncludeRobot  
   Post Create Users  { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${status}", "type": ${type}, "username": "${existing_user}", "isBypass": true }
   Response Correct Code   ${BAD_REQUEST_CODE} 
   Response Should Contain Property With Value    status      ${400}
   Response Should Contain Property With Value    message     This username already exited.

Create user without permissions
   [Documentation]   [API] Verify that can't create user at there are no permissions
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Generate ANT Gateway Header Without Permissions
   Post Create Users  { "permissionsSelected": [ { "id": ${id}, "name": "${name}" } ], "status": "${status}", "type": ${type}, "username": "${new_user}" }
   Response Correct Code   ${UNAUTHORIZED}
   Response Should Contain Property With Value    error      Unauthorized
