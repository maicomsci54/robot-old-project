*** Settings ***
Documentation   Verify that API search work correctly 
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_management/user_management_keywords.robot
Test Setup       Generate ANT Gateway Header   ${USERNAME_ADMIN}   ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${number_regex}    ^\\d+$
${string_regex}    [a-z]|[A-Z]
${status_regex}    ^(ACTIVE|INACTIVE)$

*** Test Cases ***
Search all users without params 
    [Documentation]   [API] Verify API can be search all users and get all users and got response correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Users Management
    Response Should Contain Property Matches Regex All list  .id         ${number_regex}
    #Response Should Contain Property Matches Regex All list  .username   ${string_regex} 
    Response Should Contain Property Matches Regex All list  .status     ${status_regex} 
    Response Correct Code   ${SUCCESS_CODE}

Search users by status
    [Documentation]    [API] Verify API can be search by status and got response correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Users Management  status=ACTIVE
    Response Should Contain Property Matches Regex All list  .id         ${number_regex}
    #Response Should Contain Property Matches Regex All list  .username   ${string_regex} 
    Response Should Contain Property Matches Regex All list  .status     ^(ACTIVE)$
    Response Correct Code   ${SUCCESS_CODE}

Search users by username
    [Documentation]    [API] Verify API can be search by username and got response correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Users Management  username=kanokwanph
    Response Should Contain Property Matches Regex All list  .id         ${number_regex}
    Response Should Contain Property Matches Regex All list  .username   ^(kanokwanph)$
    Response Should Contain Property Matches Regex All list  .status     ${status_regex} 
    Response Correct Code   ${SUCCESS_CODE}

Search users by username without system
    [Documentation]    [API] Verify API can be search by username and got response correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Users Management  username=incorrectdata
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Be Empty

Search users without permissions
    [Documentation]    [API] Verify API Unauthorized 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Generate ANT Gateway Header Without Permissions
    Get Users Management
    Response Correct Code   ${UNAUTHORIZED}
    Response Should Contain Property With Value    error      Unauthorized
    Response Should Contain Property With Value    message    Unauthorized
    Response Should Contain Property With Value    path    /api/user-management