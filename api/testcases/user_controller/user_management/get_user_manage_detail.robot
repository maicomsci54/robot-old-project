*** Settings ***
Documentation     Verify search users management detail and verify fromat 
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_management/user_management_keywords.robot
Test Setup       Generate ANT Gateway Header   ${USERNAME_ADMIN}   ${PASSWORD_ADMIN} 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${user_id}    29
${number_regex}    ^\\d+$
${string_regex}    [a-z]|[A-Z]
${status_regex}    ^(ACTIVE|INACTIVE)$
${email_regex}    ^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$
${bool_regex}    ^(?i)(true|false)$

*** Test Cases ***
Search users management detail
    [Documentation]    [API] Verify search users management detail and verify fromat 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Users Management Detail    ${user_id}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property Matches Regex  .username   ${string_regex}
    Response Should Contain Property Matches Regex  .fullName   ${string_regex}
    Response Should Contain Property Matches Regex  .status     ${status_regex}
    Response Should Contain Property Matches Regex  .email      ${email_regex}
    Response Should Contain All Property Values Are Integer    .telephoneNo
    Response Should Contain Property Matches Regex  .type     ${bool_regex}
    Response Should Contain All Property Values Are Integer   .permissions..id
    Response Should Contain Property Matches Regex    .permissions..name  ${string_regex}
    Response Should Contain All Property Values Are Integer    .permissionsSelected..id
    Response Should Contain Property Matches Regex    .permissionsSelected..name  ${string_regex}