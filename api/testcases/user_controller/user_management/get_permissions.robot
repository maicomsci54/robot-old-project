*** Settings ***
Documentation     Verification list of permissions false and true
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_management/get_permissions_keywords.robot
Test Setup       Generate ANT Gateway Header   ${USERNAME_ADMIN}   ${PASSWORD_ADMIN}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Verify List Of Permissions At isAdmin=false
   [Documentation]    [API] Verify list of permissions isAdmin=false
   [Tags]    Regression    Smoke    Sanity   ExcludeRobot
   Get Permissions List   isAdmin=false
   Response Equal With Count Property Values   .name   44
   Response Should Contain Property With All Values  .name    
   ...  customizeindex_formula_read
   ...  customizeindex_formula_write
   ...  customizeindex_indexaction_read
   ...  customizeindex_indexaction_write
   ...  customizeindex_indexhistory_read
   ...  customizeindex_indexhistory_write
   ...  customizeindex_indexprofile_read 
   ...  customizeindex_indexprofile_write 
   ...  externaldata_exchangeratebot_read 
   ...  externaldata_exchangeratebot_write 
   ...  externaldata_indexreuters_read 
   ...  externaldata_indexreuters_write 
   ...  marketoperation_adjustthrottle_read 
   ...  marketoperation_adjustthrottle_write 
   ...  marketoperation_adminmessagetemplate_read 
   ...  marketoperation_adminmessagetemplate_write 
   ...  marketoperation_historicallogs_read 
   ...  marketoperation_historicallogs_write 
   ...  marketoperation_userenabledisable_read 
   ...  marketoperation_userenabledisable_write 
   ...  marketoperation_warningpriceconfig_read 
   ...  marketoperation_warningpriceconfig_write 
   ...  marketoperationmobile_historicalmessage_read 
   ...  marketoperationmobile_historicalmessage_write 
   ...  marketoperationmobile_sendmessage_read 
   ...  marketoperationmobile_sendmessage_write 
   ...  monitortrading_access_read
   ...  monitortrading_access_write 
   ...  originalant_access_read 
   ...  originalant_access_write
   ...  marketoperation_haltsuspend_write
   ...  marketoperation_haltsuspend_read
   ...  marketoperation_haltsuspend_write
   ...  marketoperation_haltsuspend_read
   ...  marketoperation_downloadindexlog_read
   ...  marketoperation_downloadindexlog_write
   
Verify List Of Permissions At isAdmin=true
   [Documentation]    Verify list of permissions isAdmin=true
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Get Permissions List   isAdmin=true
   Response Equal With Count Property Values   .name   2
   Response Should Contain Property With All Values  .name    
   ...   user_management_read
   ...   user_management_write