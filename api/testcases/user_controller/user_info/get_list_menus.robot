*** Settings ***
Documentation   Verify that getMenus work correctly 
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/user_info/user_info_keywords.robot
Test Setup       Generate ANT Gateway Header   ${USERNAME_ADMIN}   ${PASSWORD_ADMIN}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Verify item list of menus
   [Documentation]    [API] Verify that getMenus work correctly 
   [Tags]    Regression    Smoke    Sanity   IncludeRobot
   Get Menus List
   Response Correct Code   ${SUCCESS_CODE}
   Response Should Contain Property With Value   .id      ${1}
   Response Should Contain Property With Value   .name    User Management
   Response Should Contain Property With Value   .prefix  usermanagement
   Response Should Contain Property With Value   .path    /user-management
   Response Should Contain Property With Value   .sequence  ${7}
   Response Should Contain Property With Value   .description   Application for User management.
   Response Should Contain Property With Value   .description   Application for User management.
   Response Should Contain Property With Value   .image    gear-icon.png