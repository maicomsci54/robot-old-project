*** Settings ***
Documentation   To make sure that adjust throttles API is still working with othor actions
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/user_actions_controller/user_actions/user_actions_keywords.robot
Resource    ../../keywords/adjust_throttle_controller /adjust_thro_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_MARKET}    ${PASSWORD_MARKET}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${username}    D0001_CU1 
${amount_throttle_limit}    200  

*** Test Cases ***
Create new adjust throttles
    [Documentation]   [API] Verify that can new create adjust throttles
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Post Create Adjust Throttles    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate","throttleLimit":${amount_throttle_limit},"username":"${username}"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Adjust Throttles    username=${username}&jobStatus=1
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value   .username    ${username}
    Response Should Contain Property With Value   .throttleLimit   ${200} 
    Response Should Contain Property With Value   .remark     Test_Automate
    Response Should Contain Property With Value   .jobStatusName   PENDING
    Response Should Contain Property With Value   .userActionName  ADDED
    Get User ID
    Patch Cancel Adjust Throttles    ${USER_ID}
    Response Correct Code   ${SUCCESS_CODE}
    

Create new adjust throttles at username is empty
    [Documentation]   [API] Verify that create new adjust throttles : username is empty
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Post Create Adjust Throttles    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate","throttleLimit":${amount_throttle_limit},"username":""}
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   .status    ${400}
    Response Should Contain Property With Value   .errorMessage    username: have error username is required

Create new adjust throttles at throttle limit is empty
    [Documentation]   [API] Verify that create new adjust throttles : throttle limit : empty
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Post Create Adjust Throttles    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate","throttleLimit":,"username":"${username}"}
    Response Correct Code   ${BAD_REQUEST_CODE}

Update Adjust Throttles
    [Documentation]   [API] Verify that can update adjust throttles
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Post Create Adjust Throttles    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate","throttleLimit":${amount_throttle_limit},"username":"${username}"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Adjust Throttles    username=${username}&jobStatus=1
    Response Correct Code   ${SUCCESS_CODE}
    Get User ID
    Put Update Adjust Throttles  ${USER_ID}    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate_update","throttleLimit":100,"username":"${username}"}
    Response Correct Code   ${SUCCESS_CODE}
    Patch Cancel Adjust Throttles    ${USER_ID}
    Response Correct Code   ${SUCCESS_CODE}

Delete Adjust Throttles
    [Documentation]   [API] Verify that can delete adjust throttles
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Min Effective Date
    Post Create Adjust Throttles    {"effectiveDate":"${EFFECTIVE_DATE}","effectiveTime":"03:00","remark":"Test_Automate","throttleLimit":${amount_throttle_limit},"username":"${username}"}
    Response Correct Code   ${SUCCESS_CODE}
    Get Adjust Throttles    username=${username}&jobStatus=1
    Response Correct Code   ${SUCCESS_CODE}
    Get User ID
    Patch Cancel Adjust Throttles    ${USER_ID}
    Response Correct Code   ${SUCCESS_CODE}