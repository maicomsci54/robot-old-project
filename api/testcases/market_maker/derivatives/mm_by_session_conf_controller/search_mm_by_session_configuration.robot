*** Settings ***
Documentation    Tests to verify that the search Session Configuration API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_by_session_conf_controller/mm_by_session_conf_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Search all Session Configuration
    [Documentation]  Verify that can search all Session Configuration sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM By Session Conf Summary
    Response Correct Code  ${SUCCESS_CODE}

Search Session Configuration with tradable instrument ID
    [Documentation]  Verify that can search by  tradable instrument ID sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM By Session Conf Summary     tiId=AAV
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..tiShortName  AAV

Search Session Configuration with session name
    [Documentation]  Verify that can search by  session name  sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM By Session Conf Summary     tiId=Morning
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..sessionName  Morning