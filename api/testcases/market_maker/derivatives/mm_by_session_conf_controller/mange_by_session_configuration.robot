*** Settings ***
Documentation    Tests to verify that the search Session Configuration API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_by_session_conf_controller/mm_by_session_conf_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${tradeable_instrument_id}  RFW
${line}  1
${total_time_in_market}   02:44:55
${user}  robot
${session_name}   test_robot

*** Test Case ***

Create mm configulation detail
    [Documentation]  Verify that can create mm configulation detail sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM By Session Conf  { "tiShortName": "${tradeable_instrument_id}", "line": "${line}", "totalTimeInMarket": "${total_time_in_market}", "totalTimeInMarketSecond": 9895, "sessionName": "${session_name}", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get Rec No
    Get MM By Session Conf Summary  ${REC_NO}
    Response Should Contain Property With Value  tiShortName  ${tradeable_instrument_id}
    Response Should Contain Property With Value  line  ${${line}} 
    Response Should Contain Property With Value  totalTimeInMarket  ${total_time_in_market}
    Response Should Contain Property With Value  sessionName  ${session_name} 
    Response Should Contain Property With Value  updUser  ${user}
    Post Delete MM By Session Conf  ${REC_NO}
    Response Correct Code  ${SUCCESS_CODE}    

Update mm configulation detail
    [Documentation]  Verify that can update mm configulation detail sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM By Session Conf  { "tiShortName": "${tradeable_instrument_id}", "line": "${line}", "totalTimeInMarket": "${total_time_in_market}", "totalTimeInMarketSecond": 9895, "sessionName": "${session_name}", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get Rec No
    Get MM By Session Conf Summary  ${REC_NO}
    Response Should Contain Property With Value  tiShortName  ${tradeable_instrument_id}
    Response Should Contain Property With Value  line  ${${line}} 
    Response Should Contain Property With Value  totalTimeInMarket  ${total_time_in_market}
    Response Should Contain Property With Value  sessionName  ${session_name} 
    Response Should Contain Property With Value  updUser  ${user}
    Put Update MM By Session Conf  { "recNo": ${REC_NO}, "tiShortName": "${tradeable_instrument_id}", "line": "${line}", "totalTimeInMarket": "${total_time_in_market}", "totalTimeInMarketSecond": 9895, "sessionName": "${session_name}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get MM By Session Conf Summary  ${REC_NO}
    Response Should Contain Property With Value  sessionName  ${session_name}_update
    Post Delete MM By Session Conf  ${REC_NO}
    Response Correct Code  ${SUCCESS_CODE}    
