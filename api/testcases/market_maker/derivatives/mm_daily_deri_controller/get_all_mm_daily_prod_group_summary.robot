*** Settings ***
Documentation    Tests to verify that getAllMMDailyProdGroupSummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_daily_deri_controller/mm_daily_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all mm daily prod group summary
    [Documentation]  Verify that getAllMMDailyProdGroupSummary  API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Daily Prod Group Summary   fromDay=20190701&toDay=20200421
    Response Correct Code  ${SUCCESS_CODE}