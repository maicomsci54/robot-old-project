*** Settings ***
Documentation    Tests to verify that getAllMMdailysummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_daily_deri_controller/mm_daily_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get mm email configuration for derivatives
    [Documentation]  Verify that getAllMMdailysummary  API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Daily Summary
    Response Correct Code  ${SUCCESS_CODE}