*** Settings ***
Documentation     Tests to verify that the user get on-top tier discount API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_conf_deri_on_top_controller/mm_conf_deri_on_top_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get on-top tier discount with on-top name
    [Documentation]  Verify that can search by on-top name
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Conf Deri On-Top Summary  onTopName=Equity Products
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..onTopName   Equity Products

Get prduct group options
    [Documentation]  Verify that can get prduct group options
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Prduct Group Options
    Response Correct Code  ${SUCCESS_CODE}
    