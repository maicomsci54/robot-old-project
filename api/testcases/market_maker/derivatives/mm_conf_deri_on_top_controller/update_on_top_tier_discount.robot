*** Settings ***
Documentation     Tests to verify that the user can create on-top tier discount API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_conf_deri_on_top_controller/mm_conf_deri_on_top_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${user}  robot
${on_top_name}     test_robot
${description}     test_robot

*** Test Case ***

Update on-top tier discount with product group	
    [Documentation]  Verify that user update create on-top tier discount 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Conf Deri On-Top   { "onTopName": "${on_top_name}", "description": "${description}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get On-Top ID
    Post Create MM Conf Deri On-Top ProdGroup  { "onTopID": ${ON_TOP_ID}, "prodGroupNo": "2", "condition": ">=", "tierDisc": "100", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get Rec No 
    Get MM Conf Deri OnTop   ${ON_TOP_ID}
    Response Should Contain Property With Value   onTopName    ${on_top_name}
    Response Should Contain Property With Value   description  ${description}
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..prodGroupNo  ${2}
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..prodGroupName  GOLD-D
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..condition   >=
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..tierDisc   100
    Response Should Contain Property Value Are Empty    mmConfDeriOnTopTierLevelsDTOS
    Put Update MM Conf Deri On-Top   { "onTopId": ${ON_TOP_ID}, "onTopName": "${on_top_name}_update", "description": "${description}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Put Update MM Conf Deri On-Top ProdGroup  { "recNo": ${REC_NO}, "onTopID": ${ON_TOP_ID}, "prodGroupNo": "8", "condition": ">=", "tierDisc": "200", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get MM Conf Deri OnTop   ${ON_TOP_ID}
    Response Should Contain Property With Value   onTopName    ${on_top_name}_update
    Response Should Contain Property With Value   description  ${description}_update
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..prodGroupNo  ${8}
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..prodGroupName  USD
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..condition   >=
    Response Should Contain Property With Value   mmConfDeriOnTopProdGroupDTOS..tierDisc   200
    Response Should Contain Property Value Are Empty    mmConfDeriOnTopTierLevelsDTOS
    Post Delete MM Conf Deri On-Top ProdGroup   { "recNo": ${REC_NO} }
    Response Correct Code  ${SUCCESS_CODE}
    Post Delete MM Conf Deri On-Top   { "onTopId": ${ON_TOP_ID} }
    Response Correct Code  ${SUCCESS_CODE}

Update on-top tier discount with tier level
    [Documentation]  Verify that user can update on-top tier discount 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Conf Deri On-Top   { "onTopName": "${on_top_name}", "description": "${description}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get On-Top ID
    Post Create MM Conf Deri On-Top Tier Level  { "onTopID": ${ON_TOP_ID}, "minProdGroup": "1", "tierDisc": "101", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get Rec No 
    Get MM Conf Deri OnTop   ${ON_TOP_ID}
    Response Should Contain Property With Value   onTopName    ${on_top_name}
    Response Should Contain Property With Value   description  ${description}
    Response Should Contain Property Value Are Empty    mmConfDeriOnTopProdGroupDTOS
    Response Should Contain Property With Value  mmConfDeriOnTopTierLevelsDTOS..minProdGroup   ${1}
    Response Should Contain Property With Value  mmConfDeriOnTopTierLevelsDTOS..tierDisc   101
    Put Update MM Conf Deri On-Top   { "onTopId": ${ON_TOP_ID}, "onTopName": "${on_top_name}_update", "description": "${description}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Put Update MM Conf Deri On-Top Tier Level  { "recNo": ${REC_NO}, "onTopID": ${ON_TOP_ID}, "minProdGroup": "2", "tierDisc": "3", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get MM Conf Deri OnTop   ${ON_TOP_ID}
    Response Should Contain Property With Value   onTopName    ${on_top_name}_update
    Response Should Contain Property With Value   description  ${description}_update
    Response Should Contain Property Value Are Empty    mmConfDeriOnTopProdGroupDTOS
    Response Should Contain Property With Value  mmConfDeriOnTopTierLevelsDTOS..minProdGroup   ${2}
    Response Should Contain Property With Value  mmConfDeriOnTopTierLevelsDTOS..tierDisc   3
    Post Delete MM Conf Deri On-Top Tier Level   { "recNo": ${REC_NO} }
    Response Correct Code  ${SUCCESS_CODE}
    Post Delete MM Conf Deri On-Top   { "onTopId": ${ON_TOP_ID} }
    Response Correct Code  ${SUCCESS_CODE}