*** Settings ***
Documentation    Tests to verify that the getAllMMConfigForDeriSummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_config_for_deri_controller/mm_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all MM config for deri summary with page and per page
    [Documentation]  Verify that getAllMMConfigForDeriSummary API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config For Deri Summary  page=1&per_page=5&getAll=false
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values   content..prodGroupName   5

Get all MM config for deri summary sort by prodGroupName
    [Documentation]  Verify that getAllMMConfigForDeriSummary API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config For Deri Summary  page=1&per_page=10&sort=ASC&order_by=prodGroupName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupName   ASC
    Get All MM Config For Deri Summary  page=1&per_page=10&sort=DESC&order_by=prodGroupName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupName   DESC