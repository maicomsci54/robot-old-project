*** Settings ***
Documentation     Tests to verify that the createMMConfigForDeri API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_config_for_deri_controller/mm_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
#MM Config For Deri
${prod_group_name}   Test_Robot
${user}   robot
${description}   automate_test
${email_condition}  P  
#MarketMaker
${clearing_member}  C003
${account_group_id}   14
${trading_member}  0002
${trading_member_unit}   0006_MU3
#Instrument Discount
${instrument_group_no}  14
${instrument_group_name}  AMATA Futures

*** Test Case ***

Create MM config for deri with relation type is none
    [Documentation]  Verify that user can createMMConfigForDeri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config For Deri  { "prodGroupName": "${prod_group_name}", "relationTypeID": "0", "instrumentGroupToDisplay": 0, "genMmPerformance": 0, "genMmTierDiscount": 0, "genMmInstrument": 0, "description": "${description}", "emailCondition": "${email_condition}", "alertDayChange": 1, "updUser": "${user}"}
    Response Correct Code  ${CREATED_CODE}
    Get Prod Group No  
    Post Create Obligation   { "prodGroupNo": ${PROD_GROUP_NO}, "instrumentGroupNo": "${instrument_group_no}", "block": null, "priority": "1", "obligation": "1", "tierDisc": "1", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create MarketMaker   { "prodGroupNo": ${PROD_GROUP_NO}, "clearingMember": "${clearing_member}", "accountGroupID": "${account_group_id}", "tradingMember": "${trading_member}", "tradingMemberUnit": "${trading_member_unit}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create Instrument Discount  { "prodGroupNo": ${PROD_GROUP_NO}, "instrumentGroupNo": "${instrument_group_no}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Config For Deri Summary  ${PROD_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    #Verify after create MM config for deri
    Response Should Contain Property With Value  prodGroupName   ${prod_group_name}
    Response Should Contain Property With Value  description   ${description}
    Response Should Contain Property With Value  emailCondition  ${email_condition}
    Response Should Contain Property With Value  updUser  ${user} 
    Response Should Contain Property With Value  mmRelationType.relationType  None
    #Verify after create Obligation
    Response Should Contain Property With Value  mmConfDeriObligation..instrumentGroupName   ${instrument_group_name}
    Response Should Contain Property With Value  mmConfDeriObligation..tierDisc   1
    Response Should Contain Property With Value  mmConfDeriObligation..obligation  ${1.0}
    #Verify after create MarketMakers
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMemberUnit  ${trading_member_unit}
    Response Should Contain Property With Value  mmConDeriMarketMakers..clearingMember  ${clearing_member}
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMember   ${trading_member}
    #Verify after create Instrument Discounts
    Response Should Contain Property With Value  mmConfDeriInstrumentGroupDiscounts..instrumentGroupName  ${instrument_group_name}
    Post Delete MM Config For Deri     { "prodGroupNo": ${PROD_GROUP_NO}, "detailNo": 1 }
    Response Correct Code  ${SUCCESS_CODE}

Create MM config for deri with relation type is priority-block
    [Documentation]  Verify that user can createMMConfigForDeri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config For Deri  { "prodGroupName": "${prod_group_name}", "relationTypeID": "3", "instrumentGroupToDisplay": 0, "genMmPerformance": 0, "genMmTierDiscount": 0, "genMmInstrument": 0, "description": "${description}", "emailCondition": "${email_condition}", "alertDayChange": 1, "updUser": "${user}"}
    Response Correct Code  ${CREATED_CODE}
    Get Prod Group No
    Post Create Priority Block Min Pass Tier  { "prodGroupNo": ${PROD_GROUP_NO}, "priority": "100", "block": "100", "minPassInstrumentGroup": "100", "tierDisc": "100", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create MarketMaker   { "prodGroupNo": ${PROD_GROUP_NO}, "clearingMember": "${clearing_member}", "accountGroupID": "${account_group_id}", "tradingMember": "${trading_member}", "tradingMemberUnit": "${trading_member_unit}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create Instrument Discount  { "prodGroupNo": ${PROD_GROUP_NO}, "instrumentGroupNo": "${instrument_group_no}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Config For Deri Summary  ${PROD_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    #Verify after create MM config for deri
    Response Should Contain Property With Value  prodGroupName   ${prod_group_name}
    Response Should Contain Property With Value  description   ${description}
    Response Should Contain Property With Value  emailCondition  ${email_condition}
    Response Should Contain Property With Value  updUser  ${user} 
    Response Should Contain Property With Value  mmRelationType.relationType  Priority - Block
    #Verify after create PriorityBlockMinPassTiers
    Response Should Contain Property With Value  mmConfDeriPriorityBlockMinPassTiers..priority   ${100}
    Response Should Contain Property With Value  mmConfDeriPriorityBlockMinPassTiers..block   100
    Response Should Contain Property With Value  mmConfDeriPriorityBlockMinPassTiers..minPassInstrumentGroup  ${100}
    Response Should Contain Property With Value  mmConfDeriPriorityBlockMinPassTiers..tierDisc   100
    #Verify after create MarketMakers
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMemberUnit  ${trading_member_unit}
    Response Should Contain Property With Value  mmConDeriMarketMakers..clearingMember  ${clearing_member}
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMember   ${trading_member}
    #Verify after create Instrument Discounts
    Response Should Contain Property With Value  mmConfDeriInstrumentGroupDiscounts..instrumentGroupName  ${instrument_group_name}
    Post Delete MM Config For Deri     { "prodGroupNo": ${PROD_GROUP_NO}, "detailNo": 1 }
    Response Correct Code  ${SUCCESS_CODE}

Create MM config for deri with relation type is X out of Y
    [Documentation]  Verify that user can createMMConfigForDeri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config For Deri  { "prodGroupName": "${prod_group_name}", "relationTypeID": "2", "instrumentGroupToDisplay": 0, "genMmPerformance": 0, "genMmTierDiscount": 0, "genMmInstrument": 0, "description": "${description}", "emailCondition": "${email_condition}", "alertDayChange": 1, "updUser": "${user}"}
    Response Correct Code  ${CREATED_CODE}
    Get Prod Group No
    Post Create Priority MinInt GroupTier  { "prodGroupNo": ${PROD_GROUP_NO}, "minPassInstrumentGroup": "100", "tierDisc": "100", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create MarketMaker   { "prodGroupNo": ${PROD_GROUP_NO}, "clearingMember": "${clearing_member}", "accountGroupID": "${account_group_id}", "tradingMember": "${trading_member}", "tradingMemberUnit": "${trading_member_unit}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create Instrument Discount  { "prodGroupNo": ${PROD_GROUP_NO}, "instrumentGroupNo": "${instrument_group_no}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Config For Deri Summary  ${PROD_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    #Verify after create MM config for deri
    Response Should Contain Property With Value  prodGroupName   ${prod_group_name}
    Response Should Contain Property With Value  description   ${description}
    Response Should Contain Property With Value  emailCondition  ${email_condition}
    Response Should Contain Property With Value  updUser  ${user} 
    Response Should Contain Property With Value  mmRelationType.relationType  X out of Y
    #Verify after create Priority MinInt GroupTier
    Response Should Contain Property With Value  mmConfDeriMinIntGroupTiers..minPassInstrumentGroup   ${100}
    Response Should Contain Property With Value  mmConfDeriMinIntGroupTiers..tierDisc   100
    #Verify after create MarketMakers
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMemberUnit  ${trading_member_unit}
    Response Should Contain Property With Value  mmConDeriMarketMakers..clearingMember  ${clearing_member}
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMember   ${trading_member}
    #Verify after create Instrument Discounts
    Response Should Contain Property With Value  mmConfDeriInstrumentGroupDiscounts..instrumentGroupName  ${instrument_group_name}
    Post Delete MM Config For Deri     { "prodGroupNo": ${PROD_GROUP_NO}, "detailNo": 1 }
    Response Correct Code  ${SUCCESS_CODE}

Create MM config for deri with relation type is X out of Y - block
    [Documentation]  Verify that user can createMMConfigForDeri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config For Deri  { "prodGroupName": "${prod_group_name}", "relationTypeID": "4", "instrumentGroupToDisplay": 0, "genMmPerformance": 0, "genMmTierDiscount": 0, "genMmInstrument": 0, "description": "${description}", "emailCondition": "${email_condition}", "alertDayChange": 1, "updUser": "${user}"}
    Response Correct Code  ${CREATED_CODE}
    Get Prod Group No
    Post Create Block Min PassInt  { "prodGroupNo": ${PROD_GROUP_NO}, "block": "robot", "minPassInstrumentGroup": "150", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create MarketMaker   { "prodGroupNo": ${PROD_GROUP_NO}, "clearingMember": "${clearing_member}", "accountGroupID": "${account_group_id}", "tradingMember": "${trading_member}", "tradingMemberUnit": "${trading_member_unit}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Post Create Instrument Discount  { "prodGroupNo": ${PROD_GROUP_NO}, "instrumentGroupNo": "${instrument_group_no}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Config For Deri Summary  ${PROD_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    #Verify after create MM config for deri
    Response Should Contain Property With Value  prodGroupName   ${prod_group_name}
    Response Should Contain Property With Value  description   ${description}
    Response Should Contain Property With Value  emailCondition  ${email_condition}
    Response Should Contain Property With Value  updUser  ${user} 
    Response Should Contain Property With Value  mmRelationType.relationType  X out of Y - Block
    #Verify after create Block Min PassInt
    Response Should Contain Property With Value  mmConfDeriBlockMinPassInts..block   robot
    Response Should Contain Property With Value  mmConfDeriBlockMinPassInts..minPassInstrumentGroup   150
    #Verify after create MarketMakers
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMemberUnit  ${trading_member_unit}
    Response Should Contain Property With Value  mmConDeriMarketMakers..clearingMember  ${clearing_member}
    Response Should Contain Property With Value  mmConDeriMarketMakers..tradingMember   ${trading_member}
    #Verify after create Instrument Discounts
    Response Should Contain Property With Value  mmConfDeriInstrumentGroupDiscounts..instrumentGroupName  ${instrument_group_name}
    Post Delete MM Config For Deri     { "prodGroupNo": ${PROD_GROUP_NO}, "detailNo": 1 }
    Response Correct Code  ${SUCCESS_CODE}
