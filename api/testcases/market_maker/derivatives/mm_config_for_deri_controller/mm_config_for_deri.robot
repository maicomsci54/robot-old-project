*** Settings ***
Documentation    Tests to verify that the other actions API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_config_for_deri_controller/mm_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all relation type option
    [Documentation]  Verify that getAllRelationTypOption API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Relation Type Option
    Response Correct Code  ${SUCCESS_CODE}

Get all trading member option
    [Documentation]  Verify that getAllTradingMemberOption API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Trading Member Option
    Response Correct Code  ${SUCCESS_CODE}

Get All Trading Member Unit Option
    [Documentation]  Verify that getAllTradingMemberUnitOption API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Trading Member Option
    Response Correct Code  ${SUCCESS_CODE}

Get all instrument
    [Documentation]  Verify that getAllInstrument API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Instrument
    Response Correct Code  ${SUCCESS_CODE}

Get all clearing member option
    [Documentation]  Verify that getAllClearingMemberOption API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Clearing Member Option
    Response Correct Code  ${SUCCESS_CODE}

