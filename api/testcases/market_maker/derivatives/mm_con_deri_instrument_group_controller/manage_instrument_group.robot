*** Settings ***
Documentation    Tests to verify that the can manage instrument group add instrument detail
...   API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_con_deri_instrument_group_controller/mm_con_deri_instrument_group_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header  AND  Remove Data before Execute  test
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${instrument_group_name}   Test_robot
${user}   robot
${description}   robot
#Instrument Group Detail
${instrument_id}   ADVANC_FUT
${type}   FUTURE
${contract_month}  12m
${scila_file_name}  Test_robot
${mm_session_file_name}  robot

*** Test Case ***

Create instrument group and add instrument detail
    [Documentation]  Verify that can create instrument group and add instrument detail sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Group   { "instrumentGroupName": "${instrument_group_name} ", "description": "${description}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get Instrument Group No
    #Add Instrument Group Detail
    Post Create Instrument Group Detail  { "instrumentGroupNo": ${INSTRUMENT_GROUP_NO}, "instrumentId": "${instrument_id}", "type": "${type}", "contractMonth": "${contract_month}", "scilaFileName": "${scila_file_name}", "mmSessionFileName": "${mm_session_file_name}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Con Deri Instrument Group  ${INSTRUMENT_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  instrumentGroupName  ${instrument_group_name}  
    Response Should Contain Property With Value  description  ${description}
    Response Should Contain Property With Value  updUser  ${user}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0].instrumentId   ${instrument_id} 
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..type   ${type}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..contractMonth   ${contract_month}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..scilaFileName   ${scila_file_name}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..mmSessionFileName  ${mm_session_file_name}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..updUser   ${user}
    Post Delete Instrument Group Detail  { "instrumentGroupNo": ${INSTRUMENT_GROUP_NO}, "instrumentGroupDetailNo": 1 }
    Response Correct Code  ${SUCCESS_CODE}
    Post Delete Instrument Group   ${INSTRUMENT_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}

Update instrument group and add instrument detail
    [Documentation]  Verify that can update instrument group and add instrument detail sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Group   { "instrumentGroupName": "${instrument_group_name}", "description": "${description}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get Instrument Group No
    #Add Instrument Group Detail
    Post Create Instrument Group Detail  { "instrumentGroupNo": ${INSTRUMENT_GROUP_NO}, "instrumentId": "${instrument_id}", "type": "${type}", "contractMonth": "${contract_month}", "scilaFileName": "${scila_file_name}", "mmSessionFileName": "${mm_session_file_name}", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get MM Con Deri Instrument Group  ${INSTRUMENT_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  instrumentGroupName  ${instrument_group_name}  
    Response Should Contain Property With Value  description  ${description}
    Response Should Contain Property With Value  updUser  ${user}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0].instrumentId   ${instrument_id} 
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..type   ${type}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..contractMonth   ${contract_month}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..scilaFileName   ${scila_file_name}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..mmSessionFileName  ${mm_session_file_name}
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..updUser   ${user}
    #Update Instrument Group and add Instrument Detail
    Put Update Instrument Group   { "instrumentGroupNo": ${INSTRUMENT_GROUP_NO}, "instrumentGroupName": "${instrument_group_name}_update", "description": "${description}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Put Update Instrument Group Detail  { "instrumentGroupNo": ${INSTRUMENT_GROUP_NO}, "instrumentGroupDetailNo": 1, "instrumentId": "${instrument_id}", "type": "${type}", "contractMonth": "${contract_month}", "scilaFileName": "${scila_file_name}_update", "mmSessionFileName": "${mm_session_file_name}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    #Verify After Update Instrument Group
    Get MM Con Deri Instrument Group  ${INSTRUMENT_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  instrumentGroupName  ${instrument_group_name}_update
    Response Should Contain Property With Value  description  ${description}_update
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..scilaFileName   ${scila_file_name}_update
    Response Should Contain Property With Value  mmConDeriInstrumentGroupDetails[0]..mmSessionFileName  ${mm_session_file_name}_update
    Post Delete Instrument Group   ${INSTRUMENT_GROUP_NO}
    Response Correct Code  ${SUCCESS_CODE}