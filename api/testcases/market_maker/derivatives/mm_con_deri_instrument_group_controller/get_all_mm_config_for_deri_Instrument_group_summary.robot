*** Settings ***
Documentation    Tests to verify that the getAllMMConfigForDeriInstrumentGroupSummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_con_deri_instrument_group_controller/mm_con_deri_instrument_group_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all MM config for deri instrument group summary with correct page and per page
    [Documentation]  Verify that getAllMMConfigForDeriInstrumentGroupSummary API can sucessfully 
    ...  call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config For Deri Instrument Group Summary  page=1&per_page=10&getAll=false
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values   content..instrumentGroupName   10

Get all MM config for deri instrument group summary sort by prodGroupName
    [Documentation]  Verify that getAllMMConfigForDeriInstrumentGroupSummary API can sucessfully 
    ...  sort by prodGroupName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config For Deri Instrument Group Summary  page=1&per_page=10&sort=ASC&order_by=instrumentGroupName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..instrumentGroupName   ASC
    Get All MM Config For Deri Instrument Group Summary  page=1&per_page=10&sort=DESC&order_by=instrumentGroupName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..instrumentGroupName   DESC

Get all MM config for deri instrument group summary with instrument ID
    [Documentation]  Verify that getAllMMConfigForDeriInstrumentGroupSummary API can sucessfully 
    ...  call with instrument ID
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config For Deri Instrument Group Summary  instrumentId=ADVANC_FUT
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..instrument.instrumentId    ADVANC_FUT