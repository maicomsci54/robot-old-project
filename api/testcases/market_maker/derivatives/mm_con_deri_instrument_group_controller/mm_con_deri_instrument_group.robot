*** Settings ***
Documentation   Tests to verify that the other actions API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_con_deri_instrument_group_controller/mm_con_deri_instrument_group_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all instrument option
    [Documentation]  Verify that getAllInstrumentOption API can sucessfully 
    ...  call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Instrument Option
    Response Correct Code  ${SUCCESS_CODE}

Get all instrument ID
    [Documentation]  Verify that getAllInstrumentId API can sucessfully 
    ...  call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Instrument ID
    Response Correct Code  ${SUCCESS_CODE}