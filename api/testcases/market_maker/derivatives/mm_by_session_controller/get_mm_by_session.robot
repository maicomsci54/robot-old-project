*** Settings ***
Documentation    Tests to verify that getAllMMBySessionSummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_by_session_controller/mm_by_session_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all mm by session summary
    [Documentation]  Verify that getAllMMBySessionSummary API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM By Session Summary
    Response Correct Code  ${SUCCESS_CODE}