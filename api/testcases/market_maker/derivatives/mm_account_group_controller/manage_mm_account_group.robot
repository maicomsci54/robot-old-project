*** Settings ***
Documentation    Tests to verify that can manage account group API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_account_group_controller/mm_account_group_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${account_group_name}  robot_test
${account}   test
${description}  automate_test
${user}  robot

*** Test Case ***

Add account group
    [Documentation]  Verify that can add account group sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Add Account Group  accountGroupName=${account_group_name}&account=${account}&description=${description}&user=${user}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain With Body   Add Success	
    Post Search Account Group  accountGroupName=${account_group_name}
    Response Correct Code  ${SUCCESS_CODE}
    Get Account Group ID
    Response Should Contain Property With Value  .AccountGroupName  ${account_group_name}
    Response Should Contain Property With Value  .Account  ${account}
    Response Should Contain Property With Value  .Description  ${description}
    Response Should Contain Property With Value  .UpdUser  ${user}
    Post Delete Account Group   accountGroupID=${ACCOUNT_GROUP_ID}&user${user}
    Response Correct Code  ${SUCCESS_CODE}

๊Update account group
    [Documentation]  Verify that can add account group sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Add Account Group  accountGroupName=${account_group_name}&account=${account}&description=${description}&user=${user}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain With Body   Add Success	
    Post Search Account Group  accountGroupName=${account_group_name}
    Response Correct Code  ${SUCCESS_CODE}
    Get Account Group ID
    Response Should Contain Property With Value  .AccountGroupName  ${account_group_name}
    Response Should Contain Property With Value  .Account  ${account}
    Response Should Contain Property With Value  .Description  ${description}
    Response Should Contain Property With Value  .UpdUser  ${user}
    Put Update Account Group   { "accountGroupID": ${ACCOUNT_GROUP_ID}, "accountGroupName": "${account_group_name}_update", "account": "${account}_update", "description": "${description}_update", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain With Body   Update Success	
    Post Search Account Group  accountGroupName=${account_group_name}
    Response Correct Code  ${SUCCESS_CODE}
    Get Account Group ID
    Response Should Contain Property With Value  .AccountGroupName  ${account_group_name}_update
    Response Should Contain Property With Value  .Account  ${account}_update
    Response Should Contain Property With Value  .Description  ${description}_update
    Response Should Contain Property With Value  .UpdUser  ${user}
    Post Delete Account Group   accountGroupID=${ACCOUNT_GROUP_ID}&user${user}
    Response Correct Code  ${SUCCESS_CODE}