*** Settings ***
Documentation    Tests to verify that the search account group API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_account_group_controller/mm_account_group_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Search all account group
    [Documentation]  Verify that can search all sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Search Account Group
    Response Correct Code  ${SUCCESS_CODE}

Search by account group
    [Documentation]  Verify that can search by account group sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Search Account Group   accountGroupName=USD_D0018_MU2
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  .AccountGroupName  USD_D0018_MU2

Search by account no
    [Documentation]  Verify that can search by account no sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Search Account Group   account=200008-0
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  .Account  200008-0
    