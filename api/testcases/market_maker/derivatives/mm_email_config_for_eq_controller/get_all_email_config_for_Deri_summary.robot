*** Settings ***
Documentation    Tests to verify that the other actions API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_email_config_for_deri_controller/mm_email_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get mm email configuration for derivatives with page and per page
    [Documentation]  Verify that getAllMMEmailConfigForDeriSummary
    ...   API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For Deri Summary   page=1&per_page=5
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values   content..emailNo   5

Get mm email configuration for derivatives with broker name
    [Documentation]  Verify that getAllMMEmailConfigForDeriSummary
    ...   API can sucessfully call with broker name
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For Deri Summary   brokerName=CAF
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..brokerName   CAF

Get mm email configuration for derivatives with tradingMember
    [Documentation]  Verify that getAllMMEmailConfigForDeriSummary
    ...   API can sucessfully call with tradingMember
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For Deri Summary   tradingMember=D0063_MU1&prod
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..tradingMember  D0063_MU1