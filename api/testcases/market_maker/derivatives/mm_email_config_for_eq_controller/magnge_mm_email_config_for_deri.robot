*** Settings ***
Documentation    Tests to verify that the magnge email config API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_email_config_for_deri_controller/mm_email_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${broker_name}   test-robot
${trading_member}  0004_MU2
${email_to}  robot@hotmail.com
${email_cc}  robot@hotmail.com
${email_bcc}  robot@hotmail.com
${mm_result}   1
${alert_day_change}  1
${description}  test
${user}  robot
${send_daily_email}  Daily

*** Test Case ***

Create mm email configulation for derivatives without product group
    [Documentation]  Verify that create mm email configulation for derivatives without product group
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Email Config For Deri   { "brokerName": "${broker_name}", "tradingMember": "${trading_member}", "emailTo": "${email_to}", "emailCC": "${email_cc}", "emailBCC": "${email_bcc}", "mmResult": ${mm_result}, "alertDayChange": ${alert_day_change}, "description": "${description}", "sendDailyEmail": "${send_daily_email}", "updUser": "${user}" }
    Response Correct Code   ${CREATED_CODE}
    Get Email No
    Response Should Contain Property With Value   brokerName   ${broker_name} 
    Response Should Contain Property With Value   tradingMember    ${trading_member}
    Response Should Contain Property With Value   emailTo   ${email_to}
    Response Should Contain Property With Value   emailCC   ${email_cc}
    Response Should Contain Property With Value   emailBCC  ${email_bcc} 
    Response Should Contain Property With Value   mmResult   ${${mm_result}}
    Response Should Contain Property With Value   alertDayChange  ${${alert_day_change}}
    Response Should Contain Property With Value   sendDailyEmail  ${send_daily_email}
    Response Should Contain Property With Value   updUser   ${user}
    Response Should Contain Property With Value   mmConfDeriEmailProdGroups   ${null}
    Post Delete MM Email Config For Deri   ${EMAIL_NO}
    Response Correct Code   ${SUCCESS_CODE}

Update mm email configulation for derivatives without product group
    [Documentation]  Verify that update mm email configulation for derivatives without product group
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Email Config For Deri   { "brokerName": "${broker_name}", "tradingMember": "${trading_member}", "emailTo": "${email_to}", "emailCC": "${email_cc}", "emailBCC": "${email_bcc}", "mmResult": ${mm_result}, "alertDayChange": ${alert_day_change}, "description": "${description}", "sendDailyEmail": "${send_daily_email}", "updUser": "${user}" }
    Response Correct Code   ${CREATED_CODE}
    Get Email No
    Response Should Contain Property With Value   brokerName   ${broker_name} 
    Put Update MM Email Config For Deri   { "emailNo": ${EMAIL_NO}, "brokerName": "${broker_name}_update", "tradingMember": "${trading_member}", "emailTo": "${email_to}", "emailCC": "${email_cc}", "emailBCC": "${email_bcc}", "mmResult": ${mm_result}, "alertDayChange": ${alert_day_change}, "description": "${description}", "sendDailyEmail": "${send_daily_email}", "updUser": "${user}" }
    Response Should Contain Property With Value   brokerName   ${broker_name}_update
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Email Config For Deri   ${EMAIL_NO}
    Response Correct Code   ${SUCCESS_CODE}

Create mm email configulation for derivatives with product Group
    [Documentation]  Verify that create mm email configulation for derivatives with product Group
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For Deri Summary   brokerName=${broker_name}
    Post Create MM Email Config For Deri   { "brokerName": "${broker_name}", "tradingMember": "${trading_member}", "emailTo": "${email_to}", "emailCC": "${email_cc}", "emailBCC": "${email_bcc}", "mmResult": ${mm_result}, "alertDayChange": ${alert_day_change}, "description": "${description}", "sendDailyEmail": "${send_daily_email}", "updUser": "${user}" }
    Response Correct Code   ${CREATED_CODE}
    Get Email No
    Post Create MM Email Config For Deri Prod Group  { "emailNo": ${EMAIL_NO}, "prodGroupNo": "2", "updUser": "${user}" }
    Response Correct Code   ${CREATED_CODE}
    Response Should Contain Property With Value  prodGroupNo   ${2}
    Response Should Contain Property With Value  updUser   ${user}
    Post Delete MM Email Config For Deri Prod Group   emailNo=${EMAIL_NO}&prodGroupNo=2
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Email Config For Deri   ${EMAIL_NO}
    Response Correct Code   ${SUCCESS_CODE}