*** Settings ***
Documentation    Tests to verify that the other actions API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_email_config_for_deri_controller/mm_email_config_for_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get product group options
    [Documentation]  Verify that can Get product group options API sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Product Group Options
    Response Correct Code  ${SUCCESS_CODE}

Get member unit options
    [Documentation]  Verify that can get member unit options API sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Member Unit Options
    Response Correct Code  ${SUCCESS_CODE}

