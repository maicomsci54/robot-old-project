*** Settings ***
Documentation    Tests to verify that user can getAllMMInstrumentSummary  work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_instrument_controller/mm_instrument_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all mm instrument summary
    [Documentation]  Verify that can getAllMMInstrumentSummary sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Instrument Summary
    Response Correct Code  ${SUCCESS_CODE}