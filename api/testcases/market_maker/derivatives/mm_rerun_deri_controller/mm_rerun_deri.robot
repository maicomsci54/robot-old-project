*** Settings ***
Documentation    Tests to verify that uthor action can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_rerun_deri_controller/mm_rerun_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get member options
    [Documentation]  Verify that get member options API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Member Options
    Response Correct Code   ${SUCCESS_CODE}

Get member unit options
    [Documentation]  Verify that get member unit options API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Member Unit Options
    Response Correct Code   ${SUCCESS_CODE}

Get product group options
    [Documentation]  Verify that get product group options API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Product Group Options
    Response Correct Code   ${SUCCESS_CODE}