*** Settings ***
Documentation    Tests to verify that user can mm rerun deri work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_rerun_deri_controller/mm_rerun_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${date}  20200422
${exe_type}  Rerun
${source_type}   MM Daily
${upd_user}  robor
${member}  0003
${member_unit}  D0006_MU1

*** Test Case ***

Rerun mm rerun deri
    [Documentation]  Verify that user can rerun deri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Rerun Deri   { "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "", "updUser": "${upd_user}" } 
    Response Correct Code   ${SUCCESS_CODE}
    Get No   
    Post Create Prod Group MM Rerun Deri   { "no": ${NO}, "prodGroupNo": "2", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Post Create Member MM Rerun Deri   { "no": ${NO}, "member": "${member}", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Post Create Member Unit MM Rerun Deri   { "no": ${NO}, "memberUnit": "${member_unit}", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  result  Pending
    Response Should Contain Property With Value  resultDesc  ${null}
    Put MM Rerun Deri   { "rerunNo": ${NO}, "executeDate": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "prodGroupList": [{"prodGroupNo": 5,"prodGroupName": "RSS3D I"}], "memberList": [ { "detailNo": 1,"member": "${member}"}],"memberUnitList": [{"detailNo": 1,"memberUnit": "${member_unit}"}]}
    Response Correct Code   ${SUCCESS_CODE}
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  result  Error
    Response Should Contain Property With Value  resultDesc  Something wrong, please contract IT team
    Post Delete MM Rerun Deri   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
