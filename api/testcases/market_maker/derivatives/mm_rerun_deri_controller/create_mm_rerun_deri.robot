*** Settings ***
Documentation    Tests to verify that user can create rerun deri work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_rerun_deri_controller/mm_rerun_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${date}  20200422
${exe_type}  Rerun
${source_type}   MM Daily
${upd_user}  robor
${member}  0003
${member_unit}  D0006_MU1

*** Test Case ***

Create mm rerun deri with add prod group
    [Documentation]  Verify that create mm rerun deri with prod group API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Rerun Deri   { "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "", "updUser": "${upd_user}" } 
    Response Correct Code   ${SUCCESS_CODE}
    Get No   
    Post Create Prod Group MM Rerun Deri   { "no": ${NO}, "prodGroupNo": "2", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  date  ${date}
    Response Should Contain Property With Value  exeType   ${exe_type}
    Response Should Contain Property With Value  sourceType   ${source_type}
    Response Should Contain Property With Value  updUser  ${upd_user}
    Response Should Contain Property With Value  prodGroupList..prodGroupNo  ${2}
    Post Delete Prod Group MM Rerun Deri    { "no": ${NO}, "detailNo": [1], "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Rerun Deri   ${NO}
    Response Correct Code   ${SUCCESS_CODE}

Create mm rerun deri with add member
    [Documentation]  Verify that create mm rerun deri with add member API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Rerun Deri   { "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "", "updUser": "${upd_user}" } 
    Response Correct Code   ${SUCCESS_CODE}
    Get No   
    Post Create Member MM Rerun Deri   { "no": ${NO}, "member": "${member}", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  date  ${date}
    Response Should Contain Property With Value  exeType   ${exe_type}
    Response Should Contain Property With Value  sourceType   ${source_type}
    Response Should Contain Property With Value  updUser  ${upd_user}
    Response Should Contain Property With Value  memberList..member  ${member}
    Post Delete Member MM Rerun Deri    { "no": ${NO}, "detailNo": [1], "updUser": "${upd_user}"}
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Rerun Deri   ${NO}
    Response Correct Code   ${SUCCESS_CODE}

Create mm rerun deri with add member unit
    [Documentation]  Verify that create mm rerun deri with add member unit API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Rerun Deri   { "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "", "updUser": "${upd_user}" } 
    Response Correct Code   ${SUCCESS_CODE}
    Get No   
    Post Create Member Unit MM Rerun Deri   { "no": ${NO}, "memberUnit": "${member_unit}", "updUser": "${upd_user}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  date  ${date}
    Response Should Contain Property With Value  exeType   ${exe_type}
    Response Should Contain Property With Value  sourceType   ${source_type}
    Response Should Contain Property With Value  updUser  ${upd_user}
    Response Should Contain Property With Value  memberUnitList..memberUnit  ${member_unit} 
    Post Delete Member Unit MM Rerun Deri    { "no": ${NO}, "detailNo": [1], "updUser": "${upd_user}"}
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Rerun Deri   ${NO}
    Response Correct Code   ${SUCCESS_CODE}

Update mm rerun deri
    [Documentation]  Verify that update mm rerun deri API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Rerun Deri   { "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "", "updUser": "${upd_user}" } 
    Response Correct Code   ${SUCCESS_CODE}
    Get No   
    Get MM Rerun Deri Summary   ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value  date  ${date}
    Response Should Contain Property With Value  exeType   ${exe_type}
    Response Should Contain Property With Value  sourceType   ${source_type}
    Response Should Contain Property With Value  updUser  ${upd_user}
    Put Update MM Rerun Deri   { "no": ${NO}, "date": "${date}", "exeType": "${exe_type}", "sourceType": "${source_type}", "reason": "test_robot", "updUser": "${upd_user}" } 
    Response Should Contain Property With Value  date  ${date}
    Response Should Contain Property With Value  exeType   ${exe_type}
    Response Should Contain Property With Value  sourceType   ${source_type}
    Response Should Contain Property With Value  updUser  ${upd_user}
    Response Should Contain Property With Value  reason   test_robot
    Response Correct Code   ${SUCCESS_CODE}
    Post Delete MM Rerun Deri   ${NO}
    Response Correct Code   ${SUCCESS_CODE}