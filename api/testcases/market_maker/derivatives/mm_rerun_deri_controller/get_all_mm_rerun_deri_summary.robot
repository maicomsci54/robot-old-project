*** Settings ***
Documentation    Tests to verify that can getAllMMRerunDeriSummary can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_rerun_deri_controller/mm_rerun_deri_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all mm rerun deri summary
    [Documentation]  Verify that can getAllMMRerunDeriSummary API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Rerun Deri Summary    
    Response Correct Code   ${SUCCESS_CODE}