*** Settings ***
Documentation    Tests to verify that user can download file report work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/file_controller/file_controller_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables***
${file_name}     MM_Daily_202004.csv

*** Test Case ***

Download file report
    [Documentation]  Verify that can download file report sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Download File Report    fileName=${file_name}
    Response Correct Code  ${SUCCESS_CODE}