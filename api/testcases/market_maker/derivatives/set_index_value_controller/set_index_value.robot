*** Settings ***
Documentation    Tests to verify that the set index value API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/set_index_value_controller/set_index_value_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header  
...   AND  Post Delete SET Index Value   { "idxName": "${idx_name}", "trdDate": "${trd_date}" }
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${trd_date}  2020-04-14
${idx_name}  SET50
${user}   robot
*** Test Case ***

Get index name options
    [Documentation]  Verify that can get index name options API sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Index Name Options
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   .idxName   ${idx_name}

Create adjust index
    [Documentation]  Verify that can create adjust index API sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create SET Index Value  { "trdDate": "${trd_date}", "industryNo": "11", "sectorNo": "11", "subSectorNo": "11", "idxName": "SET50", "indClose": "1000", "idxChg": "-22", "idxHigh": "1724", "idxLow": "123", "idxPrior": "222", "idxOpen": "33", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get All SET Index Value Summary    trdDate=${trd_date}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   content..setIndexValueIdentity.trdDate   ${trd_date}
    Response Should Contain Property With Value   content..setIndexValueIdentity.idxName   ${idx_name}
    Response Should Contain Property With Value   content..industryNo   11
    Response Should Contain Property With Value   content..sectorNo     11
    Response Should Contain Property With Value   content..subSectorNo  11
    Response Should Contain Property With Value   content..indClose     ${1000}
    Response Should Contain Property With Value   content..idxChg    ${-22}
    Response Should Contain Property With Value   content..idxHigh   ${1724}
    Response Should Contain Property With Value   content..idxLow    ${123}
    Response Should Contain Property With Value   content..idxPrior  ${222}
    Response Should Contain Property With Value   content..idxOpen   ${33}
    Response Should Contain Property With Value   content..updUser  ${user}
    Post Delete SET Index Value   { "idxName": "${idx_name}", "trdDate": "${trd_date}" }
    Response Correct Code  ${SUCCESS_CODE}

Create duplicate adjust index
    [Documentation]  Verify that can't create duplicate adjust index API
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create SET Index Value  { "trdDate": "${trd_date}", "industryNo": "11", "sectorNo": "11", "subSectorNo": "11", "idxName": "SET50", "indClose": "1000", "idxChg": "-22", "idxHigh": "1724", "idxLow": "123", "idxPrior": "222", "idxOpen": "33", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get All SET Index Value Summary    trdDate=${trd_date}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   content..setIndexValueIdentity.trdDate   ${trd_date}
    Response Should Contain Property With Value   content..setIndexValueIdentity.idxName   ${idx_name}
    Post Create SET Index Value  { "trdDate": "${trd_date}", "industryNo": "11", "sectorNo": "11", "subSectorNo": "11", "idxName": "SET50", "indClose": "1000", "idxChg": "-22", "idxHigh": "1724", "idxLow": "123", "idxPrior": "222", "idxOpen": "33", "updUser": "${user}" }
    Response Correct Code  ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   .statusCode    ${400}
    Response Should Contain Property With Value   .errorMessage    set index value existing
    Post Delete SET Index Value   { "idxName": "${idx_name}", "trdDate": "${trd_date}" }
    Response Correct Code  ${SUCCESS_CODE}

Update adjust index
    [Documentation]  Verify that can update adjust index API sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create SET Index Value  { "trdDate": "${trd_date}", "industryNo": "11", "sectorNo": "11", "subSectorNo": "11", "idxName": "SET50", "indClose": "1000", "idxChg": "-22", "idxHigh": "1724", "idxLow": "123", "idxPrior": "222", "idxOpen": "33", "updUser": "${user}" }
    Response Correct Code  ${CREATED_CODE}
    Get All SET Index Value Summary    trdDate=${trd_date}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   content..setIndexValueIdentity.trdDate   ${trd_date}
    Response Should Contain Property With Value   content..setIndexValueIdentity.idxName   ${idx_name}
    Response Should Contain Property With Value   content..industryNo   11
    Response Should Contain Property With Value   content..sectorNo     11
    Response Should Contain Property With Value   content..subSectorNo  11
    Response Should Contain Property With Value   content..indClose     ${1000}
    Response Should Contain Property With Value   content..idxChg    ${-22}
    Response Should Contain Property With Value   content..idxHigh   ${1724}
    Response Should Contain Property With Value   content..idxLow    ${123}
    Response Should Contain Property With Value   content..idxPrior  ${222}
    Response Should Contain Property With Value   content..idxOpen   ${33}
    Response Should Contain Property With Value   content..updUser  ${user}
    Put Update SET Index Value  { "trdDate": "${trd_date}", "idxName": "${idx_name}" , "indClose": "2000", "idxChg": "-221", "idxHigh": "17241", "idxLow": "1203", "idxPrior": "212", "idxOpen": "331", "updUser": "${user}" }
    Response Correct Code  ${SUCCESS_CODE}
    Get All SET Index Value Summary    trdDate=${trd_date}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   content..industryNo   11
    Response Should Contain Property With Value   content..sectorNo     11
    Response Should Contain Property With Value   content..subSectorNo  11
    Response Should Contain Property With Value   content..indClose     ${2000}
    Response Should Contain Property With Value   content..idxChg    ${-221}
    Response Should Contain Property With Value   content..idxHigh   ${17241}
    Response Should Contain Property With Value   content..idxLow    ${1203}
    Response Should Contain Property With Value   content..idxPrior  ${212}
    Response Should Contain Property With Value   content..idxOpen   ${331}
    Response Should Contain Property With Value   content..updUser  ${user}
    Post Delete SET Index Value   { "idxName": "${idx_name}", "trdDate": "${trd_date}" }
    Response Correct Code  ${SUCCESS_CODE}