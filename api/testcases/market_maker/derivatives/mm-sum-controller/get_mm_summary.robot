*** Settings ***
Documentation    Tests to verify that getMMSummary API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm-sum-controller/mm_sum_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get mm summary
    [Documentation]  Verify that getMMSummary  API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot 
    Get MM Summary
    Response Correct Code  ${SUCCESS_CODE}