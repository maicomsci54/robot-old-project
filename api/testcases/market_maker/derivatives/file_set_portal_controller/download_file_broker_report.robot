*** Settings ***
Documentation    Tests to verify that user can download file broker report work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/file_set_portal_controller/file_set_portal_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables***
${file_name}    MM_DV_20190218.ZIP

*** Test Case ***

Download file broker report
    [Documentation]  Verify that can download file broker report sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Download File Broker Report    fileName=${file_name}
    Response Correct Code  ${SUCCESS_CODE}