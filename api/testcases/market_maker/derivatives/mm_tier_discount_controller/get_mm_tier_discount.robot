*** Settings ***
Documentation    Tests to verify that user can getMMPerformance  work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_tier_discount_controller/mm_tier_discount_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get tier discount
    [Documentation]  Verify that can get tier discount sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Tier Discount Controller
    Response Correct Code  ${SUCCESS_CODE}