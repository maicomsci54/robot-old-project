*** Settings ***
Documentation    Tests to verify that user can getMMPerformance  work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_performance_controller/mm_performance_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get mm performance
    [Documentation]  Verify that can getMMPerformance sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Performance
    Response Correct Code  ${SUCCESS_CODE}