*** Settings ***
Documentation     Tests to verify that the user get and update email template API can work as expected.
Resource     ../../../../keywords/common/api_common.robot
Resource     ../../../../keywords/market_maker/derivatives/mm_conf_deri_general_controller/mm_conf_deri_general_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${email_template_path}   ../../resources/testdata/email_template/email_template.json

*** Test Case ***

Get email template
    [Documentation]  Verify that user can Get email template API
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Conf Deri General
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   mailFrom  DerivativesTradingOperationUnit@set.or.th
    Response Should Contain Property With Value   subject  Market Maker Performance Report (TFEX)
    Response Should Contain Property With Value   header  ${empty}
    Response Should Contain Property With Value   firstBodyMail   ${null}
    Response Should Contain Property With Value   secondBodyMail  ${null}
    Response Should Contain Property With Value   footer   \n\n\n\nDerivatives Trading Operation Unit\nDerivatives Trading Operation Department\nThe Stock Exchange of Thailand\nTel 0-2009-9334-5\nEmail: DerivativesTradingOperationUnit@set.or.th
    Response Should Contain Property With Value   ccMailStaff  ${null}
    Response Should Contain Property With Value   ccMailStaff  ${null}
    Response Should Contain Property With Value   mailPreCheckResult  Nattapong@set.or.th

Update email template
    [Documentation]  Verify that user can update email template API
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Read Dummy Json From File  ${email_template_path}
    Update MM Conf Deri General   ${JSON_DUMMY_DATA}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   mailFrom  DerivativesTradingOperationUnit@set.or.th
    Response Should Contain Property With Value   subject  Market Maker Performance Report (TFEX)
    Response Should Contain Property With Value   header  ${empty}
    Response Should Contain Property With Value   firstBodyMail   ${null}
    Response Should Contain Property With Value   secondBodyMail  ${null}
    Response Should Contain Property With Value   footer   \n\n\n\nDerivatives Trading Operation Unit\nDerivatives Trading Operation Department\nThe Stock Exchange of Thailand\nTel 0-2009-9334-5\nEmail: DerivativesTradingOperationUnit@set.or.th
    Response Should Contain Property With Value   ccMailStaff  ${null}
    Response Should Contain Property With Value   ccMailStaff  ${null}
    Response Should Contain Property With Value   mailPreCheckResult  Nattapong@set.or.th
