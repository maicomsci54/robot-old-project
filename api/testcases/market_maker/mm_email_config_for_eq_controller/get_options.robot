*** Settings ***
Documentation    Tests to verify that API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_email_config_for_eq_controller/mm_email_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get member options
    [Documentation]  Verify that getMemberOptions API can sucessfully call 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Member Options
    Response Correct Code  ${SUCCESS_CODE}

Get prod group options
    [Documentation]  Verify that getProdGroupOptions API can sucessfully call 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Prod Group Options
    Response Correct Code  ${SUCCESS_CODE}