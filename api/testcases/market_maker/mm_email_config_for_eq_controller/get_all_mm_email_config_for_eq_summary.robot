*** Settings ***
Documentation    Tests to verify that the "Get MM Config Foreq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_email_config_for_eq_controller/mm_email_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get all mm email config for eq summary
    [Documentation]  Verify that getAllMMEmailConfigForEQSummary API can sucessfully call 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For EQ Summary
    Response Correct Code  ${SUCCESS_CODE}
    
Get mm email config for eq summary with brokerName
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with brokerName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For EQ Summary   brokerName=KGI
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value   .brokerName  KGI

Get mm email config for eq summary with prodGroupName
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with prodGroupName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For EQ Summary   prodGroupName=DW
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property With Value   .prodGroupName   DW

Get all mm email config for eq summary with order by
    [Documentation]   Verify that Getmmconfigforeq API can sucessfully call with order by
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For EQ Summary    sort=DESC&order_by=brokerName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   .brokerName   DESC
    Get All MM Email Config For EQ Summary    sort=ASC&order_by=brokerName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   .brokerName   ASC
    Get All MM Email Config For EQ Summary    sort=DESC&order_by=member
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   .member   DESC
    Get All MM Email Config For EQ Summary    sort=ASC&order_by=member
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   .member   ASC

Get all mm email config for eq summary with correct page and per page
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Email Config For EQ Summary   page=1&per_page=5
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values    .brokerName   5
