*** Settings ***
Documentation    Tests to verify that the "Get MM Config Foreq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_email_config_for_eq_controller/mm_email_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Create mm email config for EQ
    [Documentation]  Verify that user can create MM Email Config For EQ
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Email Config For EQ   { "brokerName": "robot_test", "member": "0001", "emailTo": "robot_test@hotmail.com", "emailCC": "robot_test@hotmail.com", "emailBCC": "robot_test@hotmail.com", "description": "test_robot", "updUser": "robot" }
    Response Correct Code  ${CREATED_CODE}
    Get Email No
    Response Should Contain Property With Value  brokerName   robot_test
    Response Should Contain Property With Value  member    0001
    Response Should Contain Property With Value  emailTo   robot_test@hotmail.com
    Response Should Contain Property With Value  emailCC   robot_test@hotmail.com
    Response Should Contain Property With Value  emailBCC  robot_test@hotmail.com
    Response Should Contain Property With Value  description   test_robot
    Response Should Contain Property With Value  updUser   robot
    Post Delete MM Email Config For EQ  ${EMAIL_NO}
    Response Correct Code  ${SUCCESS_CODE}

Update mm email config for EQ
    [Documentation]  Verify that user can create MM Email Config For EQ
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Email Config For EQ   { "brokerName": "robot_test", "member": "0001", "emailTo": "robot_test@hotmail.com", "emailCC": "robot_test@hotmail.com", "emailBCC": "robot_test@hotmail.com", "description": "robot_test", "updUser": "robot" }
    Response Correct Code  ${CREATED_CODE}
    Get Email No
    Response Should Contain Property With Value   brokerName   robot_test
    Response Should Contain Property With Value   member       0001
    Response Should Contain Property With Value   description   robot_test
    Put Update MM Email Config For EQ  { "emailNo": ${EMAIL_NO}, "brokerName": "robot_test_update", "member": "0002", "emailTo": "robot_test@hotmail.com", "emailCC": "robot_test@hotmail.com", "emailBCC": "robot_test@hotmail.com", "description": "robot_test_update", "updUser": "robot"}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  brokerName   robot_test_update
    Response Should Contain Property With Value  member    0002
    Response Should Contain Property With Value  description   robot_test_update
    Post Delete MM Email Config For EQ  ${EMAIL_NO}
    Response Correct Code  ${SUCCESS_CODE}

Create mm email config for EQ and add product group 
    [Documentation]  Verify that user can create mm email config for EQ and add product group 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Email Config For EQ   { "brokerName": "robot_test", "member": "0001", "emailTo": "robot_test@hotmail.com", "emailCC": "robot_test@hotmail.com", "emailBCC": "robot_test@hotmail.com", "description": "test_robot", "updUser": "robot" }
    Response Correct Code  ${CREATED_CODE}
    Get Email No 
    Post Create MM Config EQ ProdGroup  { "emailNo": ${EMAIL_NO}, "prodGroupNo": "1", "updUser": "robot" } 
    Response Correct Code  ${CREATED_CODE}
    Get MM Email Config For EQ Summary With Email No  ${EMAIL_NO}
    Response Should Contain Property With Value  prodGroupList..detailNo   ${1}
    Response Should Contain Property With Value  prodGroupList..prodGroupNo   ${1}
    Response Should Contain Property With Value  prodGroupList..prodGroupName   DW
    Get MM Email Config For EQ Summary With Email No  ${EMAIL_NO}
    Post Delete MM Config EQ ProdGroup  { "emailNo": ${EMAIL_NO}, "detailNo": [1] }
    Response Correct Code  ${SUCCESS_CODE}
    Post Delete MM Email Config For EQ  ${EMAIL_NO}
    Response Correct Code  ${SUCCESS_CODE}