*** Settings ***
Documentation    Tests to verify that the "MMConfEQGeneral" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_conf_eq_general_controller/mm_conf_eq_general_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${json_path}   ../../resources/testdata/mm_general_config_eq/create_mm_general_config_eq.json

*** Test Case ***
Get mm general confige
    [Documentation]  Verify that user can getMMConfEQGeneral sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config EQ General
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   mailFrom   ANT-Dev@set.or.th
    Response Should Contain Property With Value   listMailUpdateRecordConfigEQChange    pornprasert@set.or.th
    Response Should Contain Property With Value   listMailInternal   pornprasert@set.or.th
    Response Should Contain Property With Value   subject   ผลการวัด performance MM
    Response Should Contain Property With Value   header   เรียน บริษัทผู้ออกใบสำคัญแสดงสิทธิอนุพันธ์
    #Response Should Contain Property With Value   firstBodyMail   ตลาดหลักทรัพย์แห่งประเทศไทย ขอแจ้งผลการปฏิบัติหน้าที่ของผู้ดูแลสภาพคล่องของใบสำคัญแสดงสิทธิอนุพันธ์ที่ออกโดยบริษัทฯ (Indicative MM performance) เฉพาะหลักทรัพย์ที่ไม่ผ่านตามเกณฑ์ ช่วงระหว่าง [weeklyDate] ${SPACE}จำนวน [TINo] หลักทรัพย์ รายละเอียดดังนี้ 
    #Response Should Contain Property With Value   secondBodyMail   1. ข้อมูลดังกล่าวเป็นเพียงข้อมูลเบื้องต้น โดยคำนวณตามการทำหน้าที่ของ market maker ในตลาดหลักทรัพย์ฯเท่านั้น ไม่ได้ครอบคลุมเงื่อนไขการสงวนสิทธิในการปฏิบัติหน้าที่ของผู้ดูแลสภาพคล่องตามที่ระบุไว้ในข้อกำหนดสิทธิฯ\n2. ข้อมูลการทำหน้าที่ในหลักทรัพย์ที่หมดอายุในเดือนนี้ อาจแตกต่างจากที่บริษัทฯ คำนวณได้ เนื่องจากระบบของตลาดหลักทรัพย์ฯคำนวณการทำหน้าที่ของ market maker จนถึงวันทำการซื้อขายสุดท้าย
    Response Should Contain Property With Value   footer    จึงเรียนมาเพื่อโปรดทราบ\nฝ่ายปฏิบัติการซื้อขายหลักทรัพย์\nโทร. 0-2009-9328\t\n
    Response Should Contain Property With Value   minDate  5
    Response Should Contain Property With Value   ccMailStaff   ${null}
    Response Should Contain Property With Value   bccMailStaff  ${null}
    Response Should Contain Property With Value   mailPreCheckResult   pornprasert@set.or.th

Update mm general config eq
    [Documentation]  Verify that user can update mm general config eq
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config EQ General
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   subject   ผลการวัด performance MM
    Read Dummy Json From File  ${json_path}
    Update Dummy Json Value Data  $.subject   ผลการวัด performance MM
    Put Update MM Config EQ General  ${JSON_DUMMY_DATA}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   subject   ผลการวัด performance MM
    #re-back update 
    Read Dummy Json From File  ${json_path}
    Update Dummy Json Value Data  $.subject   ผลการวัด performance MM
    Put Update MM Config EQ General  ${JSON_DUMMY_DATA}
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value   subject   ผลการวัด performance MM