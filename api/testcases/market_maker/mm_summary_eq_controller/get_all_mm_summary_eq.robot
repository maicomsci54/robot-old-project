*** Settings ***
Documentation    Tests to verify that the "getAllMMSummaryEq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_summary_eq_controller/mm_summary_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get getAllMMSummaryEq with page and per page
    [Documentation]  Verify that getAllMMSummaryEq API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Summary EQ   page=1&per_page=3
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values   .no   3

Get getAllMMSummaryEq with prodGroupName
    [Documentation]  Verify that getAllMMSummaryEq API can sucessfully call with prodGroupName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Summary EQ   prodGroupName=DW
    Response Should Contain All Property With Value  .prodGroupName   DW
    Response Correct Code  ${SUCCESS_CODE}

Get All Instrument ID
    [Documentation]  Verify that getAllInstrumentId API can sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Instrument ID  
    Response Correct Code  ${SUCCESS_CODE}

Get All Segment ID
    [Documentation]  Verify that getAllsegmentId API can sucessfully 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Segment ID
    Response Correct Code  ${SUCCESS_CODE}