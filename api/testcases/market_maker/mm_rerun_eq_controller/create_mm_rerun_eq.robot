*** Settings ***
Documentation    Tests to verify that the "getAllMMSummaryEq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_rerun_eq_controller/mm_rerun_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Create MM rerun for equity
    [Documentation]  Verify that can create MM rerun for equity
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${current_date}=  Get Date With Format And Increment   %Y%m%d 
    Post Create MM Rerun EQ    { "date": "${current_date}", "exeType": "Rerun", "reason": "test_robot", "updUser": "robot" }
    Response Correct Code   ${SUCCESS_CODE}
    Get No
    Response Should Contain Property With Value    date      ${current_date}
    Response Should Contain Property With Value    exeType   Rerun
    Response Should Contain Property With Value    reason    test_robot
    Response Should Contain Property With Value    updUser   robot
    Response Should Contain Property With Value    result    Pending
    Response Should Contain Property With Value    resultDesc   ${null}
    Response Should Contain Property With Value   exeDttm      ${null}
    Delete MM Rerun EQ  ${NO}
    Response Correct Code   ${SUCCESS_CODE}

Duplicate MM rerun for equity
    [Documentation]  Verify that can duplicate MM rerun for equity
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${current_date}=  Get Date With Format And Increment   %Y%m%d 
    Post Create MM Rerun EQ    { "date": "${current_date}", "exeType": "Rerun", "reason": "test_robot", "updUser": "robot" }
    Response Correct Code   ${SUCCESS_CODE}
    Get No
    Response Should Contain Property With Value    date      ${current_date}
    Response Should Contain Property With Value    exeType   Rerun
    Response Should Contain Property With Value    reason    test_robot
    Response Should Contain Property With Value    updUser   robot
    Response Should Contain Property With Value    result    Pending
    Response Should Contain Property With Value    resultDesc   ${null}
    Response Should Contain Property With Value   exeDttm      ${null}
    Post Duplicate MM Rerun EQ  ${NO}
    Response Correct Code   ${SUCCESS_CODE}
    Get Duplicate No
    Response Should Contain Property With Value    date      ${current_date}
    Response Should Contain Property With Value    exeType   Rerun
    Response Should Contain Property With Value    reason    test_robot
    Response Should Contain Property With Value    updUser   robot
    Response Should Contain Property With Value    result    Pending
    Response Should Contain Property With Value    resultDesc   ${null}
    Response Should Contain Property With Value   exeDttm      ${null}
    Delete MM Rerun EQ In List No  ${NO}  ${DUP_NO}

Update MM rerun for equity
    [Documentation]  Verify that can update MM rerun for equity
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${current_date}=  Get Date With Format And Increment   %Y%m%d 
    Post Create MM Rerun EQ    { "date": "${current_date}", "exeType": "Rerun", "reason": "test_robot", "updUser": "robot" }
    Response Correct Code   ${SUCCESS_CODE}
    Get No
    Response Should Contain Property With Value    reason    test_robot
    Put Update MM Rerun EQ  { "no": ${NO}, "date": "${current_date}", "exeType": "Rerun", "reason": "test_robot_update", "updUser": "kittiwut" }
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property With Value    reason    test_robot_update
    Delete MM Rerun EQ  ${NO}
    Response Correct Code   ${SUCCESS_CODE}

Get All MM Rerun EQ Summary
    [Documentation]  Verify that can getAllMMRerunEQSummary
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Rerun EQ Summary
    Response Correct Code   ${SUCCESS_CODE}