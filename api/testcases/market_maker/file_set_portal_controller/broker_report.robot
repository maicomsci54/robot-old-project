*** Settings ***
Documentation    Tests to verify that the "file-set-portal-controller" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/file_set_portal_controller/file_set_portal_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get Derivatives Name Files Last Two Date
    [Documentation]  Verify that user can getDerivativesNameFilesLastTwoDate sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Derivatives Name Files Last Two Date
    Response Correct Code  ${SUCCESS_CODE}
    
Get EQ Name Files Last Two Date    
    [Documentation]  Verify that user can getEQNameFilesLastTwoDate sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get EQ Name Files Last Two Date
    Response Correct Code  ${SUCCESS_CODE}

Post Download File
    [Documentation]  Verify that user can downloadFile sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Download File
    Response Correct Code  ${SUCCESS_CODE}


    

    