*** Settings ***
Documentation    Tests to verify that the "MMConfEQExceptionSummary" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_conf_eq_exception_controller/mm_conf_eq_exception_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get all MM config EQ exception summary
    [Documentation]  Verify that user can getAllMMConfEQExceptionSummary sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config EQ Exception Summary
    Response Correct Code   ${SUCCESS_CODE}

Get all MM config EQ exception summary with exceptionName 
    [Documentation]  Verify that user can getAllMMConfEQExceptionSummary with exceptionName sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All MM Config EQ Exception Summary   search=Exp_FW_592019
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain All Property With Value  content..exceptionName   Exp_FW_592019 

Get instrument options
    [Documentation]  Verify that user can getInstrumentOptions sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Options
    Response Correct Code   ${SUCCESS_CODE}

Get Member Options
    [Documentation]  Verify that user can getMemberOptions sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot    
    Get Member Options
    Response Correct Code   ${SUCCESS_CODE}

Get product options
    [Documentation]  Verify that user can getProductOptions sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Product Options
    Response Correct Code   ${SUCCESS_CODE}

Get tradable instrument options
    [Documentation]  Verify that user can getTradableInstrumentOptions sucessfully
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Tradable Instrument Options
    Response Correct Code   ${SUCCESS_CODE}