*** Settings ***
Documentation    Tests to verify that the api create, update and delete "MMConfEQException" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_conf_eq_exception_controller/mm_conf_eq_exception_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Create MM config EQ exception
    [Documentation]  Verify that user can create MM config EQ exception
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config EQ Exception  { "exceptionName": "Exp_FW_robot", "prodGroupNo": "1", "exceptionTI": [], "exceptionMember": [], "exceptionInstrument": [], "customGroup": "test_robot", "exceptionFrom": "2020/04/07", "exceptionTo": "2020/04/07", "updUser": "robot" }
    Response Correct Code  ${SUCCESS_CODE}
    Get Exception No  
    Get MM Config EQ Exception   ${EXCEPTION_NO}
    Response Should Contain Property With Value   prodGroupNo    ${1}    
    Response Should Contain Property With Value   exceptionName  Exp_FW_robot
    Response Should Contain Property With Value   prodGroupDTO.prodGroupName   DW
    Response Should Contain Property With Value   exceptCustomGroup     test_robot
    Response Should Contain Property With Value   updUser   robot
    Post Delete MM Config EQ Exception   ${EXCEPTION_NO}
    Response Correct Code  ${SUCCESS_CODE}

Create duplicate exception name MM config EQ exception
    [Documentation]  Verify that user can create duplicate exception name MM config EQ exception
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config EQ Exception  { "exceptionName": "Exp_FW_robot_dup", "prodGroupNo": "1", "exceptionTI": [], "exceptionMember": [], "exceptionInstrument": [], "customGroup": "test_robot", "exceptionFrom": "2020/04/07", "exceptionTo": "2020/04/07", "updUser": "robot" }
    Response Correct Code  ${SUCCESS_CODE}
    Get Exception No
    Post Create MM Config EQ Exception  { "exceptionName": "Exp_FW_robot_dup", "prodGroupNo": "1", "exceptionTI": [], "exceptionMember": [], "exceptionInstrument": [], "customGroup": "test_robot", "exceptionFrom": "2020/04/07", "exceptionTo": "2020/04/07", "updUser": "robot" }
    Response Correct Code  ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value  statusCode      ${400}
    Response Should Contain Property With Value   errorMessage    Already this exception name or product name.
    Post Delete MM Config EQ Exception   ${EXCEPTION_NO}
    Response Correct Code  ${SUCCESS_CODE}

Update MM config EQ exception
    [Documentation]  Verify that user can create MM config EQ exception
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create MM Config EQ Exception  { "exceptionName": "Exp_FW_robot", "prodGroupNo": "1", "exceptionTI": [], "exceptionMember": [], "exceptionInstrument": [], "customGroup": "test_robot", "exceptionFrom": "2020/04/07", "exceptionTo": "2020/04/07", "updUser": "robot" }
    Response Correct Code  ${SUCCESS_CODE}
    Get Exception No
    Response Should Contain Property With Value   exceptionName  Exp_FW_robot
    Response Should Contain Property With Value   prodGroupDTO.prodGroupName   DW
    Response Should Contain Property With Value   exceptCustomGroup     test_robot
    Response Should Contain Property With Value   updUser   robot
    Post Update MM Config EQ Exception   ${EXCEPTION_NO}    { "exceptionName": "Exp_FW_robot_update", "prodGroupNo": "2", "exceptionTI": [], "exceptionMember": [], "exceptionInstrument": [], "customGroup": "test_robot_update", "exceptionFrom": "2020/04/07", "exceptionTo": "2020/04/07", "updUser": "robot" }
    Response Correct Code  ${SUCCESS_CODE}  
    Response Should Contain Property With Value   exceptionName  Exp_FW_robot_update
    Response Should Contain Property With Value   prodGroupDTO.prodGroupName   MM_ETF
    Response Should Contain Property With Value   exceptCustomGroup     test_robot_update
    Response Should Contain Property With Value   updUser   robot
    Get Exception No
    Post Delete MM Config EQ Exception   ${EXCEPTION_NO}
    Response Correct Code  ${SUCCESS_CODE}