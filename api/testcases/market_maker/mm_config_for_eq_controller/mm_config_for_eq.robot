*** Settings ***
Documentation    Tests to verify that uthor action can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_config_for_eq_controller/mm_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get Market List ID
    [Documentation]  Verify that get market list ID API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Market List ID   market_id=SET
    Response Correct Code  ${SUCCESS_CODE}
    #Response Should Contain Property With All Values  .marketListId   STOCK   ETF   INDEX

Get All Instrument ID
    [Documentation]  Verify that get all instrument ID API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Instrument ID
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  .instrumentId   (ALL)

Get All Market ID  
    [Documentation]  Verify that get all market ID API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Market ID
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value  .marketId   SET
    
Get All Segment ID
    [Documentation]  Verify that get all segment ID API can sucessfully call with correct
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get All Segment ID
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With All Values  .segmentId   DERIVATIVE_WARRANT   DERIVATIVE_WARRANT_FOREIGN_UNDERLYING   ETF
    