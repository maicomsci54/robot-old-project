*** Settings ***
Documentation    Tests to verify that the "Get MM Config Foreq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_config_for_eq_controller/mm_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***

Get mmconfigforeq with page and per page
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with correct page and per page
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config Foreq   page=1&per_page=2
    Response Correct Code  ${SUCCESS_CODE}
    Response Equal With Count Property Values   content..prodGroupNo   2
    
Get mmconfigforeq with sort by prodGroupName
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with ascending and descending sort by prodGroupName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config Foreq   sort=ASC
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupName   ASC
    Get MM Config Foreq   sort=DESC
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupName   DESC

Get mmconfigforeq with order by
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with all property order by
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config Foreq   order_by=prodGroupNo
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupNo   ASC
    Get MM Config Foreq   order_by=prodGroupName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..prodGroupName   ASC
    Get MM Config Foreq   order_by=marketId
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..marketId   ASC
    Get MM Config Foreq   order_by=segmentId
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..segmentId   ASC
    Get MM Config Foreq   order_by=instrumentId
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..instrumentId   ASC
    Get MM Config Foreq   order_by=tiSelected
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..tiSelected   ASC
    Get MM Config Foreq   order_by=obligation
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..obligation   ASC
    Get MM Config Foreq   order_by=scilaFileName
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..scilaFileName   ASC
    Get MM Config Foreq   order_by=updUser
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..updUser   ASC
    Get MM Config Foreq   order_by=updDttm
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain All Property Values Are Sorted   content..updDttm   ASC

Get mmconfigforeq with prodGroupName
    [Documentation]  Verify that Getmmconfigforeq API can sucessfully call with prodGroupName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get MM Config Foreq   prodGroupName=DW
    Response Should Contain All Property With Value   content..prodGroupName   DW
    Response Correct Code  ${SUCCESS_CODE}
