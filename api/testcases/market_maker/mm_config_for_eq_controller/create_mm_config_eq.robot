*** Settings ***
Documentation    Tests to verify that the "Get MM Config Foreq" API can work as expected.
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/market_maker/mm_config_for_eq_controller/mm_config_for_eq_keywords.robot
Test Setup   Run Keywords   Create Old ANT Gateway Session  AND  Generate MM Gateway Header 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Create configuration for EQ
    [Documentation]  Verify that can create configuration for EQ
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Configuration For EQ    { "prodGroupName": "robot_test", "marketId": "SET", "marketListId": "STOCK", "segmentId": "DERIVATIVE_WARRANT", "instrumentId": "(ALL)", "tiSelected": "ALL", "condition": "P", "obligation": "100", "scilaFileName": "robot", "minInstrument": null, "minTi": null, "emailCondition": "ALL", "updUser": "pornprasert" }
    Response Correct Code  ${CREATED_CODE}
    Response Should Contain Property With Value    prodGroupName    robot_test
    Response Should Contain Property With Value    marketId        SET
    Response Should Contain Property With Value    marketListId    STOCK
    Response Should Contain Property With Value    segmentId    DERIVATIVE_WARRANT
    Response Should Contain Property With Value    instrumentId  (ALL)
    Response Should Contain Property With Value    tiSelected   ALL
    Response Should Contain Property With Value    condition  P
    Response Should Contain Property With Value    obligation   ${100}
    Response Should Contain Property With Value    scilaFileName   robot
    Response Should Contain Property With Value    updUser   pornprasert
    Get Prod Group No
    Delete Configuration For EQ   ${PROD_GROUP_NO}
    Response Correct Code   ${SUCCESS_CODE}

Update configuration for EQ
    [Documentation]  Verify that can update configuration for EQ
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Configuration For EQ    { "prodGroupName": "robot_test", "marketId": "SET", "marketListId": "STOCK", "segmentId": "DERIVATIVE_WARRANT", "instrumentId": "(ALL)", "tiSelected": "ALL", "condition": "P", "obligation": "100", "scilaFileName": "robot", "minInstrument": null, "minTi": null, "emailCondition": "ALL", "updUser": "pornprasert" }
    Response Correct Code  ${CREATED_CODE}
    Get Prod Group No
    Put Configuration For EQ    { "prodGroupNo": ${PROD_GROUP_NO}, "prodGroupName": "robot_test_update", "marketId": "SET", "marketListId": "STOCK", "segmentId": "DERIVATIVE_WARRANT", "instrumentId": "(ALL)", "tiSelected": "ALL", "condition": "P", "obligation": 100, "scilaFileName": "robot", "minInstrument": null, "minTi": null, "emailCondition": "ALL", "updUser": "pornprasert" }
    Response Correct Code  ${SUCCESS_CODE}
    Response Should Contain Property With Value    prodGroupName    robot_test_update
    Delete Configuration For EQ    ${PROD_GROUP_NO}
    Response Correct Code   ${SUCCESS_CODE}