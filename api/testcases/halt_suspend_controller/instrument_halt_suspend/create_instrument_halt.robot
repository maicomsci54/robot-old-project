*** Settings ***
Documentation    Verify that create instrument Halt/SP work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
...          AND   Delete Instrument With Options   user=kittiwut  effectiveDateFrom=${effective_date}  effectiveDateTo=${effective_date}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${instrument_id}   CGD
${internal_id}   CGD_SYMB
${effective_date}   2023-04-20

*** Test Case ***
Create new instrument Halt/SP is complete
    [Documentation]     [API] Verify that Create new instrument Halt/SP is complete
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID
    Get Details Instrument Halt/SP By ID  ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Should Contain Property With Value   id   ${${TEST_VARIABLE_INSTRUMENT_ID}}
    Response Should Contain Property With Value   instrumentShortName   ${instrument_id}
    Response Should Contain Property With Value   effectiveDate   ${effective_date}
    Delete Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Get Details Instrument Halt/SP By ID  ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Should Be Empty Body

Create duplicate instrument name in the same effective date
    [Documentation]     [API] Verify that user create a duplicate Instrument name in the same effective date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   Duplicated instrument and Effective date[${instrument_id},${effective_date}]
    Delete Instrument Halt/SP   ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Get Details Instrument Halt/SP By ID  ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Should Be Empty Body

Create instrument Halt/SP with effective date less than current date
    [Documentation]     [API] Verify that user create instrument Halt/SP with effective date is current date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "2020-03-20" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The effective date should be T+1 !!!

Create instrument Halt/SP with effective date is current date
    [Documentation]     [API] Verify that user create instrument Halt/SP with effective date is current date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${current_date}=   Get Current Date    result_format=%Y-%m-%d
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${current_date}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The effective date should be T+1 !!!

Create instrument Halt/SP with effective date is holiday
    [Documentation]     [API] Verify that user create instrument Halt/SP with effective date is holiday
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${holiday}=  Get Holiday
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${holiday}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The new effective date should not be holiday !!!