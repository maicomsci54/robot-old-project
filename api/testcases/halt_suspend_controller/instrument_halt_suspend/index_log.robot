*** Settings ***
Documentation    Verify that Getholidays work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${mm_dd_yyyy_regex}   ([0-9]{2})/([0-9]{2})/(?:19|20)[0-9]{2}

*** Test Case ***
Get index logs and get index logs file 
    [Documentation]     [API] Verify that can getIndexLogs work correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Index Logs
    Response Correct Code   ${SUCCESS_CODE} 
    Response Should Contain Property Matches Regex  [0].businessDate  ${mm_dd_yyyy_regex}
    Response Should Contain Property Matches Regex  [0].fileName  .
    Download Index Logs File Until Got Success Response