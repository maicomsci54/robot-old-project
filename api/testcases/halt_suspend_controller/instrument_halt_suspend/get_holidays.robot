*** Settings ***
Documentation    Verify that Getholidays work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get holidays
    [Documentation]     [API] Verify that Getholidays work correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Holidays
    Response Correct Code   ${SUCCESS_CODE}