*** Settings ***
Documentation    Verify that search work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
Test Teardown   Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${yyyy_dd_mm_regex}   (?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:
...  (?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))
${yyyy_mm_dd_hh:mm:ss}  (?:19|20)[0-9]{2}-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})

*** Test Case ***
Search instrument Halt/SP without params 
    [Documentation]    Verify that [API] Search instrument Halt/SP without params 
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  effectiveDateFrom=2020-03-11
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property Matches Regex  [0].instrumentShortName   [A-Za-z0-9]
    Response Should Contain Property Matches Regex  [0].effectiveDate   ${yyyy_dd_mm_regex}
    Response Should Contain Property Matches Regex  [0].uploadDate      ${yyyy_mm_dd_hh:mm:ss}
    Response Should Contain Property Matches Regex  [0].lastUpdateBy   [A-Za-z]
    Response Should Contain Property Matches Regex  [0].lastUpdateTime   ${yyyy_mm_dd_hh:mm:ss}

Search instrument Halt/SP with effectiveDateFrom
    [Documentation]    Verify that [API] Search instrument Halt/SP with effectiveDateFrom
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  effectiveDateFrom=2020-03-17
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .effectiveDate
    Compare Date Should Be True All Property Values     ${property_values_list}   >=   2020-03-17
    
Search instrument Halt/SP with effectiveDateTo
    [Documentation]    Verify that [API] Search instrument Halt/SP with effectiveDateTo
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  effectiveDateTo=2020-03-16
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .effectiveDate
    Compare Date Should Be True All Property Values   ${property_values_list}  <=  2020-03-16

Search instrument Halt/SP with effectiveDateFrom and effectiveDateTo
    [Documentation]    Verify that [API] Search instrument Halt/SP with effectiveDateFrom and effectiveDateTo
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  effectiveDateFrom=2020-03-17&effectiveDateTo=2020-03-19
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .effectiveDate
    Compare Date Should Be True All Property Values With 2 Operators   ${property_values_list}  >=  2020-03-17   and  ${property_values_list}  <=  2020-03-19
    
Search instrument Halt/SP with effectiveDateFrom, uploadDateFrom and user
    [Documentation]    Verify that [API] Search instrument Halt/SP with effectiveDateFrom, uploadDateFrom and user
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  effectiveDateFrom=2020-03-23&uploadDateFrom=2020-03-12&user=kittiwut
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .effectiveDate
    Set Test Variable    ${effectivedate_list}   ${property_values_list}
    Get Property Value List From Json   .uploadDate 
    Set Test Variable    ${uploaddate_list}  ${property_values_list}
    Compare Date Should Be True All Property Values   ${effectivedate_list}  >=  2020-03-23
    Compare Date Should Be True All Property Values   ${uploaddate_list}  >=  2020-03-12
    Response Should Contain All Property With Value  .lastUpdateBy    kittiwut

Search instrument Halt/SP with effectiveDateFrom, uploadDateTo
    [Documentation]    Verify that [API] Search instrument Halt/SP with effectiveDateFrom, uploadDateTo
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP   effectiveDateFrom=2020-03-16&uploadDateTo=2020-03-12
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .effectiveDate
    Set Test Variable    ${effectivedate_list}   ${property_values_list}
    Get Property Value List From Json   .uploadDate 
    Set Test Variable    ${uploaddate_list}  ${property_values_list}
    Compare Date Should Be True All Property Values   ${effective_date_list}  >=  2020-03-16
    Compare Date Should Be True All Property Values   ${upload_date_list}  <=  2020-03-12
    
Search instrument Halt/SP with uploadDateFrom
    [Documentation]    Verify that [API] Search instrument Halt/SP with uploadDateFrom
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  uploadDateFrom=2020-03-12
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .uploadDate
    Compare Date Should Be True All Property Values     ${property_values_list}  >=  2020-03-12
    
Search instrument Halt/SP with uploadDateTo
    [Documentation]    Verify that [API] Search instrument Halt/SP with uploadDateTo
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  uploadDateTo=2020-03-10
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .uploadDate
    Compare Date Should Be True All Property Values     ${property_values_list}  <=  2020-03-10

Search instrument Halt/SP with uploadDateFrom to uploadDateTo
    [Documentation]    Verify that [API] Search instrument Halt/SP with uploadDateFrom to uploadDateTo
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  uploadDateFrom=2020-03-10&uploadDateTo=2020-03-12
    Response Correct Code   ${SUCCESS_CODE}
    Get Property Value List From Json   .uploadDate
    Compare Date Should Be True All Property Values With 2 Operators   ${property_values_list}  >=  2020-03-10   and  ${property_values_list}  <=  2020-03-12

Search instrument Halt/SP with user
    [Documentation]    Verify that [API] Search instrument Halt/SP with user
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  user=kittiwut
    Response Correct Code   ${SUCCESS_CODE}   
    Response Should Contain All Property With Value  .lastUpdateBy    kittiwut

Search instrument Halt/SP with instrumentShortName
    [Documentation]    Verify that [API] Search instrument Halt/SP with instrumentShortName
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Instrument Halt/SP  instrumentId=CGD
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain All Property With Value  .instrumentShortName    CGD