*** Settings ***
Documentation    Verify that getById work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header  ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
...          AND          Delete Instrument With Options   user=kittiwut  effectiveDateFrom=${effective_date}  effectiveDateTo=${effective_date}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variable ***
${yyyy_dd_mm_regex}   (?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:
...  (?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))
${instrument_id}   CGD
${internal_id}   CGD_SYMB
${effective_date}   2023-04-20

*** Test Case ***
Get details instrument Halt/SP
    [Documentation]     [API] Verify that getById work correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID
    Get Details Instrument Halt/SP By ID   ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property Matches Regex  instrumentShortName   [A-Za-z0-9]
    Response Should Contain Property Matches Regex  effectiveDate     ${yyyy_dd_mm_regex}
    Response Should Contain Property Matches Regex  uploadDate        .
    Response Should Contain Property Matches Regex  lastUpdateBy      [A-Za-z0-9]
    Response Should Contain Property Matches Regex  lastUpdateTime    .