*** Settings ***
Documentation    Verify that create instrument Halt/SP work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}   
...          AND   Delete Instrument With Options   user=kittiwut  effectiveDateFrom=${effective_date}  effectiveDateTo=${update_effective_date}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Variables ***
${instrument_id}   CGD
${internal_id}   CGD_SYMB
${effective_date}   2023-04-20
${update_effective_date}   2023-04-24

*** Test Case ***

Update instrument Halt/SP
    [Documentation]     [API] Verify that user can update instrument Halt/SP
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effectiveDate}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID
    Get Details Instrument Halt/SP By ID  ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Should Contain Property With Value   effectiveDate   ${effectiveDate}
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "${update_effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Details Instrument Halt/SP By ID  ${TEST_VARIABLE_INSTRUMENT_ID}
    Response Should Contain Property With Value   effectiveDate   ${update_effective_date}
    Delete Instrument ID In List      ${TEST_VARIABLE_INSTRUMENT_ID}

Update duplicate instrument name in the same effective date
    [Documentation]     [API] Verify that user can update instrument Halt/SP
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    #prepare create the date for update case duplicate same effective date
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "2023-04-24" }
    Response Correct Code   ${SUCCESS_CODE}
    ${instrument_id_data_1}   Get Instrument Halt/SP ID
    #prepare create the date for update 
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    ${instrument_id_data_2}   Get Instrument Halt/SP ID
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "2023-04-24" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   Duplicated instrument and Effective date[${instrument_id},${effectiveDate}]
    Delete Instrument ID In List   ${instrument_id_data_1}  ${instrument_id_data_2}

Update instrument Halt/SP with effective date less than current date
    [Documentation]     [API] Verify that user Update instrument Halt/SP with effective date is current date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID    
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "2020-03-20" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The effective date should be T+1 !!!
    Delete Instrument ID In List  ${TEST_VARIABLE_INSTRUMENT_ID} 

Update instrument Halt/SP with effective date is current date
    [Documentation]     [API] Verify that user update instrument Halt/SP with effective date is current date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${current_date}=   Get Current Date    result_format=%Y-%m-%d
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID    
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "${current_date}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The effective date should be T+1 !!!
    Delete Instrument ID In List  ${TEST_VARIABLE_INSTRUMENT_ID} 

Update instrument Halt/SP with effective date is holiday
    [Documentation]     [API] Verify that user update instrument Halt/SP with effective date is holiday
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${holiday}=  Get Holiday
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID    
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "${holiday}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The new effective date should not be holiday !!!
    Delete Instrument ID In List  ${TEST_VARIABLE_INSTRUMENT_ID} 

Update instrument Halt/SP with same effective date
    [Documentation]     [API] Verify that user update instrument Halt/SP with same effective date
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    ${holiday}=  Get Holiday
    Post Create Instrument Halt/SP    { "instrumentId": "${instrument_id}", "internalId": "${internal_id} ", "effectiveDate": "${effective_date}" }
    Response Correct Code   ${SUCCESS_CODE}
    Get Instrument Halt/SP ID    
    Patch Update Instrument Halt/SP    ${TEST_VARIABLE_INSTRUMENT_ID}   { "effectiveDate": "${effective_date}" }
    Response Correct Code   ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value   status   ${${BAD_REQUEST_CODE}}
    Response Should Contain Property With Value   errorMessage   The new effective date same as old effective date !!!
    Delete Instrument ID In List  ${TEST_VARIABLE_INSTRUMENT_ID} 