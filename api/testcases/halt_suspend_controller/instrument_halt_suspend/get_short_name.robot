*** Settings ***
Documentation    Verify that getInstruments work correctly
Resource    ../../../keywords/common/api_common.robot
Resource    ../../../keywords/common/gateway_common.robot
Resource    ../../../keywords/halt_suspend_controller/instrument_halt_suspend/instrument_halt_suspend_keywords.robot
Test Setup   Run Keywords   Create MKTOPS Gateway Session  AND  Generate MKTOPS Gateway Header   ${USERNAME_ADMIN}    ${PASSWORD_ADMIN}
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Case ***
Get instruments short name
    [Documentation]     [API] Verify that getInstrument name work correctly
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Short Name
    Response Correct Code   ${SUCCESS_CODE}
    Response Should Contain Property Matches Regex  [0].internalId     .
    Response Should Contain Property Matches Regex  [0].instrumentId   .
    Response Should Contain Property Matches Regex  [0].shortName      . 