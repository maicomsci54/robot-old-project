*** Settings ***
Documentation    Verify that can get Report Controller work correctly 
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/rpt/report_controller_keywords.robot
Test Setup   Create RPT Gateway Session 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Get generate astsecsh CA report
    [Documentation]   [API] generateAstsecshCAReport
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Astsecsh CA Report
    Response Correct Code   ${SUCCESS_CODE}

Get Generate Eodrcase CA Report
    [Documentation]   [API] generateEodrcaseCaReport
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Astsecsh CA Report
    Response Correct Code   ${SUCCESS_CODE}
