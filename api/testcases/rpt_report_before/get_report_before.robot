*** Settings ***
Documentation    Verify that can get Report Controller work correctly 
Resource    ../../keywords/common/api_common.robot
Resource    ../../keywords/common/gateway_common.robot
Resource    ../../keywords/rpt_before/report_controller_keywords.robot
Test Setup   Create RPT Before Gateway Session 
Test Teardown    Run Keywords    Log Bug For Test Case   AND  Delete All Sessions

*** Test Cases ***
Get generate astidx report
    [Documentation]   [API] generateAstidxReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Astidx Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate astsec report
    [Documentation]   [API] generateAstsecReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Astsec Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate astsecsh report
    [Documentation]   [API] generateAstsecshReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Astsecsh Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate eodrcase report
    [Documentation]   [API] generateEodrcaseReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Eodrcase Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate maiidx report
    [Documentation]   [API]  generateMaiidxReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Maiidx Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate mairidx report
    [Documentation]   [API] generateMairidxReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Mairidx Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate maisec report
    [Documentation]   [API] generateMaisecReport  
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Maisec Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate mktstat report
    [Documentation]   [API] generateMktstatReport
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Mktstat Report
    Response Correct Code   ${SUCCESS_CODE}

Get generate rindex report
    [Documentation]   [API] generateRindexReport
    [Tags]    Regression    Smoke    Sanity   IncludeRobot
    Get Generate Rindex Report
    Response Correct Code   ${SUCCESS_CODE}