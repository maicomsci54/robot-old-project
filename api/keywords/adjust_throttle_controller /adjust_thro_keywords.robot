*** Settings ***
Resource    ../../resources/init.robot
Resource    ../../../import.robot
Resource    ../common/date_time_common.robot

*** Keywords ***
Post Create Adjust Throttles
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}    /adjust-throttles   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Patch Cancel Adjust Throttles
    [Arguments]  ${user_id}
    ${RESP}=     Patch Request     ${MKTOPS_GATEWAY_SESSION}   /adjust-throttles/cancel?ids=${user_id}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Adjust Throttles
    [Arguments]   ${params_uri}=${EMPTY}
    ${RESP}=     Get Request     ${MKTOPS_GATEWAY_SESSION}   /adjust-throttles   params=${params_uri}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get User ID
    ${USER_ID}=    Get Property Value From Json By Index    .id     0
    Set Test Variable    ${USER_ID}

Put Update Adjust Throttles
    [Arguments]  ${user_id}  ${data}
    ${RESP}=     Put Request     ${MKTOPS_GATEWAY_SESSION}    /adjust-throttles/${user_id}   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

