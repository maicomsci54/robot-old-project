*** Settings ***
Resource    ../../resources/init.robot

*** Keywords ***

Get Foreign Holiday
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Foreign Holiday
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Foreign Holiday By ID
    [Arguments]  ${foreign_holiday_id}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/${foreign_holiday_id}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Foreign Holiday
    [Arguments]  ${foreign_holiday_id}  ${data}
    ${RESP}=     Put Request     ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/${foreign_holiday_id}   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Countries
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /foreign-holiday/countries   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Holiday Status
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/status   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Patch Delete Foreign Holiday
    [Arguments]  ${data}
    ${RESP}=     Patch Request     ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/delete   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Upload Foreign Holiday
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/upload  data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Pre Verify Upload Foreign Holiday
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}   /foreign-holiday/verify-upload  data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Foreign Holiday ID
    Set Test Variable   ${FOREIGN_HOLIDAY_ID}   ${RESP.text}

