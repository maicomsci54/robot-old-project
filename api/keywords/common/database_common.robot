*** Settings ***
Resource      ../../resources/init.robot
Resource      list_common.robot

*** Keywords ***
Connect Database 
    [Arguments]    ${database_name}
    Connect To Database   ${SQL_MODULE_NAME}  ${database_name}  ${SQL_USERNAME}   ${SQL_PASSWORD}  ${SQL_HOST}  ${SQL_PORT}

Check If Exists In DB
    [Arguments]   ${sql_command}
    Check If Exists In Database   ${sql_command}  