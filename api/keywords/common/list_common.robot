*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***
All Items In List Match Regex
    [Arguments]    ${list}    ${regex}
    :FOR    ${property_value}    IN    @{list}
    \    ${string_value}=    Convert To String    ${property_value}
    \    Should Match Regexp    ${string_value}    ${regex}

Get Last Number In Item List
    [Arguments]    ${item_list} 
    ${item_list}=  Sort List With Options   ${item_list}   DESC
    [return]   @{item_list}[0]  

Sort List Items
    [Arguments]    ${list}    ${sort_option}
    Sort List With Options   ${list}   ${sort_option}
    Log  ${list}

All Items In List Match
    [Arguments]   ${list}    ${value}
    :FOR    ${property_value}    IN    @{list}
    \    Should Contain    ${property_value}    ${value} 

All Items In List Empty
    [Arguments]   ${list}   
    :FOR    ${property_value}    IN    @{list}
    \    Should Be Empty    ${property_value}
