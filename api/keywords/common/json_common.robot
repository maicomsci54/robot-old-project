*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***
Get Property Value From Json By Index
    [Arguments]    ${property}    ${index}    ${input_json}=${RESP.json()}
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    [Return]    @{property_values}[${index}]

Read Dummy Json From File
    [Arguments]    ${data_file}=${dummy_data_file}
    ${dummy_data_file_path}=     Catenate    SEPARATOR=/    ${CURDIR}    ${data_file}
    ${JSON_DUMMY_DATA}=    Load JSON From File    ${dummy_data_file_path}
    Set Test Variable    ${JSON_DUMMY_DATA}

Update Dummy Json Value Data
    [Arguments]    ${json_path}    ${value}
    ${JSON_DUMMY_DATA}=    Update Value To Json    ${json_dummy_data}    ${json_path}    ${value}
    Set Test Variable    ${JSON_DUMMY_DATA}

Get Property Value From Json
    [Arguments]    ${property} 
    @{property_values}=    Get Value From Json    ${RESP.json()}   $.${property}
    [Return]    @{property_values}