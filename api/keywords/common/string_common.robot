*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***
Random Unique String By Epoch Datetime
    ${RETURN_STRING}=    Get Current Date    time_zone=local    increment=0    result_format=%Y%m%d%H%M%S%f    exclude_millis=False
    Set Test Variable   ${RETURN_STRING}
    ${return_tring}=    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%Y%m%d%H%M%S%f    exclude_millis=False
    [Return]    ${return_tring}

Get Random Strings
    [Arguments]    ${length}    ${format}
    ${random_string} =    Generate Random String    ${length}    ${format}
    [Return]    ${random_string}