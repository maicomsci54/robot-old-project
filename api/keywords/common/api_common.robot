*** Settings ***
Resource      ../../resources/init.robot
Resource      list_common.robot

*** Keywords ***
Response Correct Code
    [Arguments]                     ${resp_code}
    Should Be Equal As Strings      ${RESP.status_code}             ${resp_code}

Response Should Be Empty
    Should Be Equal    ${RESP.text}    []

Response Should Be Empty Body
    Should Be Empty    ${RESP.text}

Response Should Be String
    Should Be String    ${RESP.text}

Response Should Contain With Body
    [Arguments]  ${body}
    Should Contain    ${RESP.text}    ${body}

Response Should Contain Property With Value
    [Arguments]    ${property}    ${value}
    ${property_value}=    Get Value From Json    ${RESP.json()}    $.${property}
    List Should Contain Value    ${property_value}    ${value}

Response Should Contain All Property With Value
    [Arguments]    ${property}    ${value}
    @{property_value_list}=    Get Value From Json    ${RESP.json()}    $.${property}
    All Items In List Match   ${property_value_list}   ${value}
    
Response Should Contain Property With All Values
    [Arguments]    ${property}    @{values_list}
    @{property_values}=    Get Value From Json    ${RESP.json()}    $.${property}
    Should Not Be Empty    ${property_values}
    :FOR    ${value}    IN    @{values_list}
    \    Should Contain    ${property_values}    ${value}

Response Equal With Count Property Values
    [Arguments]    ${property}  ${expect_length}
    @{property_values}=    Get Value From Json    ${RESP.json()}    $.${property}  
    ${length}=   Get length  ${property_values}
    Should Be True    ${length} == ${expect_length}   msg= count value = ${length} but as expected = ${expect_length}

Response Should Contain Property With Integer Value
    [Arguments]    ${property}
    @{property_value}=    Get Value From Json    ${RESP.json()}    $.${property}
    ${integer_value}=     Convert To Integer    @{property_value}[0]
    Should Be Equal    @{property_value}[0]    ${integer_value}

Response Should Contain Property Matches Regex
    [Arguments]    ${property}    ${regex}
    @{property_value}=    Get Value From Json    ${RESP.json()}    $.${property}
    ${string_value}=    Convert To String    @{property_value}[0]
    Should Match Regexp    ${string_value}    ${regex}

Response Should Contain Property Matches Regex All list
    [Arguments]    ${property}    ${regex}
    @{property_value}=    Get Value From Json    ${RESP.json()}    $.${property}
    All Items In List Match Regex   ${property_value}   ${regex}

Response Should Contain All Property Values Are Integer
    [Arguments]    ${property}
    @{property_values}=    Get Value From Json    ${RESP.json()}    $.${property}
    Should Not Be Empty    ${property_values}
    :FOR    ${property_value}    IN    @{property_values}
    \    Should Be Equal As Integers   ${property_value}    ${${property_value}}

Response Should Contain All Property Values Are Null
    [Arguments]    ${property}
    @{property_values}=    Get Value From Json    ${RESP.json()}    $.${property}
    Should Not Be Empty    ${property_values}
    :FOR    ${property_value}    IN    @{property_values}
    \    Should Be Null    ${property_value}

Compare Date Should Be True All Property Values 
    [Arguments]   ${list}   ${operators}  ${date} 
    @{actual_list}=   Create List
    :FOR  ${property_values}  IN  @{list}
    \  ${property_values} =    Get Regexp Matches	  ${property_values}    (?:19|20[0-9]{2}-[0-9]{2}-[0-9]{2})
    \  Set Test Variable  ${property_values}   ${property_values}[0]
    \  Append To List  ${actual_list}  ${property_values}
    :FOR    ${property_values}   IN    @{actual_list}
    \  Should Be True    '${property_values}'${operators}'${date}'

Compare Date Should Be True All Property Values With 2 Operators 
    [Arguments]   ${list}  ${operators_1}  ${date_1}  ${condition}  ${list}  ${operators_2}  ${date_2}
    @{actual_list}=   Create List
    :FOR  ${property_values}  IN  @{list}
    \  ${property_values} =    Get Regexp Matches	  ${property_values}    (?:19|20[0-9]{2}-[0-9]{2}-[0-9]{2})
    \  Set Test Variable  ${property_values}   ${property_values}[0]
    \  Append To List  ${actual_list}  ${property_values}
    :FOR    ${property_values}   IN    @{actual_list}
    \  Should Be True    '${property_values}'${operators_1}'${date_1}' ${condition} '${property_values}'${operators_2}'${date_2}'

Get Property Value List From Json
    [Arguments]    ${property}  
    @{property_values_list}=    Get Value From Json    ${RESP.json()}    $.${property}
    ${property_values_list}=  Sort List With Options   ${property_values_list}   DESC
    Set Test Variable   ${property_values_list}

Log Bug For Test Case
    Run Keyword If Test Failed    Fail    *HTML*<font size="2" color="red"><b>${RESP.text}</b></font>         

Response Should Contain All Property Values Are Sorted
    [Arguments]    ${property}    ${sort_option}
    @{property_values}=    Get Value From Json    ${RESP.json()}    $.${property}
    Should Not Be Empty    ${property_values}
    ${sort_list}=    Copy List    ${property_values}
    Sort List Items    ${sort_list}    ${sort_option}
    Lists Should Be Equal    ${property_values}    ${sort_list}

Response Should Contain Property Value Are Empty
    [Arguments]    ${property}
    @{property_value}=  Get Value From Json    ${RESP.json()}    $.${property}
    ${string_value}=    Convert To String    @{property_value}[0]
    Should Be Equal    ${string_value}    []