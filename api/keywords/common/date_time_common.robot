*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***
Get Date With Format And Increment
    [Arguments]    ${result_format}    ${increment}=0
    ${desired_date} =    Get Current Date    result_format=${result_format}    increment=${increment}
    Set Test Variable    ${DESIRED_DATE}    ${desired_date}
    [Return]    ${desired_date}

Get Unix Time Stamp From Current Date
    [Arguments]   ${result_format}
    ${desired_date} =    Get Current Date    result_format=${result_format}
    ${UNIX_TIME_STAMP}=   Convert Date   ${desired_date}     epoch
    Set Test Variable    ${UNIX_TIME_STAMP}

Get Future Date
    [Arguments]    ${add_days}  ${result_format}
    ${current_date} =   Get Current Date    result_format=${result_format}    increment=0
    ${future_date}=   Add Time To Date  ${current_date}   ${add_days}   ${result_format}
    [Return]    ${future_date}

Get Previous Day
    [Arguments]    ${date_remove}   
    ${current_date} =   Get Current Date    result_format=datetime    increment=0
    ${previous_date}=   Subtract Time From Date  ${current_date}   ${date_remove}   result_format=datetime
    [Return]    ${previous_date.day}

Get Previous Date
    [Arguments]    ${date_remove}
    ${holiday}=  Get Holiday   
    ${current_date} =   Get Current Date    result_format=datetime    increment=0
    ${previous_date}=   Subtract Time From Date  ${current_date}   ${date_remove}   result_format=datetime
    ${str_previous_date}=   Convert To String  ${previous_date}
    ${previous_date}=    Get Regexp Matches   ${str_previous_date}   (?:19|20)[0-9]{2}-([0-9]{2})-([0-9]{2})
    [Return]    ${previous_date}[0]

Get Holiday
    :FOR     ${index}    IN RANGE     7
    \   ${date_yymmdd}=  Get Future Date   ${index} Day    %Y-%m-%d
    \   ${date}=  Get Future Date   ${index} Day   %A-%Y-%m-%d
    \   ${date_name} =    Get Regexp Matches	${date}    Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday
    \   Set Test Variable    ${date_name}   ${date_name}[0]
    \   Run Keyword If  '${date_name}' == 'Saturday' or '${date_name}' == 'Sunday'
        ...    Exit For Loop
    [Return]  ${date_yymmdd}