*** Settings ***
Resource      ../../resources/init.robot
Resource      date_time_common.robot

*** Keywords ***
Create Gateway Session
    Create Session    ${ANT_GATEWAY_SESSION}   ${ANT_API_HOST}   disable_warnings=1

Generate Access Token
    [Arguments]   ${username}  ${password}
    Create Session    ${AUTHORIZATION_GATEWAY_SESSION}   ${ANT_API_HOST_TOKEN}   disable_warnings=1
    &{headers}=    Create Dictionary    Content-Type=application/x-www-form-urlencoded    Authorization=Basic Y2xpZW50OnNlY3JldA==
    &{data}=    Create Dictionary    username=${username}    password=${password}   grant_type=password
    ${resp}=    Post Request    ${AUTHORIZATION_GATEWAY_SESSION}    /oauth/token    data=${data}    headers=${headers}
    &{resp_body}=    To Json    ${resp.text}
    Set Suite Variable    ${ACCESS_TOKEN}    ${resp_body.access_token}

Generate ANT Gateway Header
    [Arguments]   ${username}  ${password}
    Generate Access Token   ${username}  ${password}
    Create Gateway Session
    &{ANT_GATEWAY_HEADER}=    Create Dictionary   Content-Type=application/json    authorization=Bearer ${ACCESS_TOKEN} 
    Set Test Variable    &{ANT_GATEWAY_HEADER}

Generate ANT Gateway Header Without Permissions
    &{ANT_GATEWAY_HEADER}=    Create Dictionary   Content-Type=application/json
    Set Test Variable    &{ANT_GATEWAY_HEADER}

Create MKTOPS Gateway Session
    Create Session    ${MKTOPS_GATEWAY_SESSION}   ${MKTOPS_API_HOST}    disable_warnings=1

Generate MKTOPS Gateway Header
    [Arguments]   ${username}  ${password}
    Generate Access Token   ${username}  ${password}
    &{MKTOPS_GATEWAY_HEADER}=    Create Dictionary   Content-Type=application/json    authorization=Bearer ${ACCESS_TOKEN} 
    Set Test Variable    &{MKTOPS_GATEWAY_HEADER}
    
Create Index Action Gateway Session
    Create Session    ${INDEX_ACTION_GATEWAY_SESSION}   ${ANT_INDEX_ACTION_HOST}    disable_warnings=1

Create Old ANT Gateway Session
    Create Session   ${OLD_ANT_GATEWAY_SESSION}    ${OLD_ANT_HOST}   disable_warnings=1

Generate MM Gateway Header 
    &{MM_GATEWAY_HEADER}=    Create Dictionary   Content-Type=application/json
    Set Test Variable    &{MM_GATEWAY_HEADER}

Create RPT Before Gateway Session
    Create Session    ${RPT_BEFORE_GATEWAY_SESSION}   ${ANT_RPT_BEFORE_HOST}    disable_warnings=1

Create RPT Gateway Session
    Create Session    ${RPT_GATEWAY_SESSION}   ${ANT_RPT_HOST}    disable_warnings=1