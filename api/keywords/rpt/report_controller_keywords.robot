*** Settings ***
Resource    ../../resources/init.robot

*** Keywords ***

Get Generate Astsecsh CA Report
    ${RESP}=    Get Request    ${RPT_GATEWAY_SESSION}    /report/astsecsh-ca
    Set Test Variable    ${RESP}

Get Generate Eodrcase CA Report
    ${RESP}=    Get Request    ${RPT_GATEWAY_SESSION}    /report/eodrcase-ca
    Set Test Variable    ${RESP}
