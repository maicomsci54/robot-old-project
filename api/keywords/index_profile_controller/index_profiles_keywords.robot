*** Settings ***
Resource    ../../resources/init.robot
Resource    ../common/string_common.robot
Resource    ../common/database_common.robot

*** Keywords ***

Post Create Index Profile Standard Index
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${INDEX_ACTION_GATEWAY_SESSION}     /index-action/index-profiles/standard-index   data=${data}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Profile Standard Index
    [Arguments]  ${index_profile_id}
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/standard-index/${index_profile_id}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Index Profile Customize Index
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${INDEX_ACTION_GATEWAY_SESSION}     /index-action/index-profiles/customize-index   data=${data}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Profile Customize Index 
    [Arguments]  ${index_profile_id}
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/customize-index/${index_profile_id}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Index Profile Standard Index
    [Arguments]  ${index_profile_id}   ${data}
    ${RESP}=     Put Request     ${INDEX_ACTION_GATEWAY_SESSION}     /index-action/index-profiles/standard-index/${index_profile_id}   data=${data}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Index Profile Customize Index
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${INDEX_ACTION_GATEWAY_SESSION}     /index-action/index-profiles/customize-index/${id}   data=${data}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Profile ID
    Set Test Variable   ${INDEX_PROFILE_ID}   ${RESP.text}  

Generate Index Short Name
    ${random_index_short_name}=    Get Random Strings    5  [LETTERS][NUMBERS][UPPER]
    Set Test Variable    ${RANDOM_INDEX_SHORT_NAME}    index_Robot_${random_index_short_name}

Get Index Types
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/types    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Profile QS Names
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/qs/names    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Profile Names
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/names    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Rebalance Frequency
    ${RESP}=     Get Request     ${INDEX_ACTION_GATEWAY_SESSION}    /index-action/index-profiles/rebalance-frequencies    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Select Index ID By Index Short Name
    [Arguments]  ${index_short_name}
    @{index_id}	 Query	SELECT ixd.id FROM INDX.index ixd join INDX.index_profiles ixp on ixp.index_id=ixd.id where index_short_name= '${index_short_name}'
    Set Test Variable  ${INDEX_ID}   ${index_id[0][0]}

Delete Index Profile By Index ID
    [Arguments]   ${index_id}
    Execute Sql String   DELETE FROM INDX.index where id = '${index_id}'

Check Index ID If Exists Delete Index Profile
    [Arguments]  ${index_short_name}
    ${status}=  Run Keyword And Return Status   Check If Exists In DB    SELECT ixd.id FROM INDX.index ixd join INDX.index_profiles ixp on ixp.index_id=ixd.id where index_short_name= '${index_short_name}'
    Run Keyword If  '${status}' == 'True'   Run Keywords  Select Index ID By Index Short Name   ${index_short_name}
    ...   AND  Delete Index Profile By Index ID  ${INDEX_ID} 