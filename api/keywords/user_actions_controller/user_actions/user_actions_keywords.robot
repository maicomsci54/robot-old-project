*** Settings ***
Resource    ../../../resources/init.robot
Resource    ../../common/date_time_common.robot

*** Keywords ***
Post Create User Action
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}    /user-actions   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Job Status
    [Arguments]  ${job_status}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /user-actions?jobStatus=${job_status}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Patch Cancel User Action
    [Arguments]    ${user_id}
    ${RESP}=    Patch Request   ${MKTOPS_GATEWAY_SESSION}     /user-actions/cancel?ids=${user_id}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get User Action By ID
    [Arguments]  ${user_id}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /user-actions/${user_id}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get User Actions
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /user-actions   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update User Action
    [Arguments]  ${user_id}  ${data}  
    ${RESP}=     Put Request     ${MKTOPS_GATEWAY_SESSION}    /user-actions/${user_id}   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MKT Actions
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}     /user-actions/mkt-actions   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Actions
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}     /user-actions/actions   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Min Effective Date
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}     /user-actions/min-effective-date   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    Get Effective Date
    
Get Effective Date
    ${Effective_Date}=    Get Property Value From Json By Index    .effectiveDate     0
    Set Test Variable    ${EFFECTIVE_DATE}

Get Last User ID
    [Arguments]    ${property} 
    @{property_value}=  Get Value From Json    ${RESP.json()}    $.${property}
    ${last_user_id}=    Get Last Number In Item List  ${property_value}
    Set Test Variable    ${LAST_USER_ID}