*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get All MM Email Config For EQ Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforeq/mmemailconfigforeqsummary   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Email Config For EQ
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforeq/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Email Config For EQ
    [Arguments]  ${email_no}
    ${RESP}=    Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforeq/delete/${email_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Email No
    ${EMAIL_NO}=    Get Property Value From Json By Index    emailNo   0
    Set Test Variable    ${EMAIL_NO}

Put Update MM Email Config For EQ
    [Arguments]  ${data}
    ${RESP}=    Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforeq/update   data=${data}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Member Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforeq/member/options  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Prod Group Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}     /analytics/mmemailconfigforeq/prodgroup/options  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Email Config For EQ Summary With Email No
    [Arguments]   ${email_no} 
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforeq/${email_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Config EQ ProdGroup
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforeq/prodgroup/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Config EQ ProdGroup
     [Arguments]  ${data}
    ${RESP}=    Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforeq/prodgroup/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
