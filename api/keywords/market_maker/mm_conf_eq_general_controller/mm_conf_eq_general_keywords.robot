*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get MM Config EQ General
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeqgeneral/config/email   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Config EQ General
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfeqgeneral/update/config/email   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
