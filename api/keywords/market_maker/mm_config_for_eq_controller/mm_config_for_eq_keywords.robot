*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get MM Config Foreq
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforeq/mmconfigforeqsummary   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Market List ID
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/marketListId   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Configuration For EQ
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Prod Group No
    ${PROD_GROUP_NO}=    Get Property Value From Json By Index    .prodGroupNo   0
    Set Test Variable    ${PROD_GROUP_NO}

Delete Configuration For EQ
    [Arguments]  ${prod_group_no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/delete/${prod_group_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Configuration For EQ
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All MM Config For Eq Summary
    [Arguments]  ${prod_group_name}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/${prod_group_name}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All AccoutId With Product Group No
    [Arguments]  ${prod_group_name}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/accountid/${prod_group_name}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Instrument ID
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforeq/instrumentId/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Market ID
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}  /analytics/mmconfigforeq/marketid/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Segment ID
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}  /analytics/mmconfigforeq/segmentId/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
