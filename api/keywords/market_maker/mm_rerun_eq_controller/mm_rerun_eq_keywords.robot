*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get All MM Rerun EQ Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmreruneq/mmreruneqsummary   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Rerun EQ
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmreruneq/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Rerun EQ
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmreruneq/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Delete MM Rerun EQ
    [Arguments]  ${no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmreruneq/delete/${no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get No
    ${NO}=    Get Property Value From Json By Index    no   0
    Set Test Variable    ${NO}

Post Duplicate MM Rerun EQ
    [Arguments]  ${no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmreruneq/duplicate/${no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Duplicate No
    ${DUP_NO}=    Get Property Value From Json By Index    no   0
    Set Test Variable    ${DUP_NO}

Delete MM Rerun EQ In List No
    [Arguments]   @{no_list}
    :FOR  ${list}  IN    @{no_list}
    \   Delete MM Rerun EQ   ${list}
    \   Response Correct Code   ${SUCCESS_CODE}
