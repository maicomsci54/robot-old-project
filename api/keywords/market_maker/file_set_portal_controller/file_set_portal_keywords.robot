*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get Derivatives Name Files Last Two Date
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/derivatives/file-name/last-two-date/list   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get EQ Name Files Last Two Date
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/eq/file-name/last-two-date/list   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Download File
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/download/set-portal   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}