*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***

Get All MM Config EQ Exception Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/mmconfeq-exception-summary   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Config EQ Exception
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfeq-exception/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Exception No
    ${exception_no}=    Get Property Value From Json By Index    exceptionNo   0
    Set Test Variable    ${EXCEPTION_NO}

Post Delete MM Config EQ Exception
    [Arguments]  ${exception_no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfeq-exception/delete/${exception_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Update MM Config EQ Exception
    [Arguments]  ${exception_no}  ${data} 
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfeq-exception/update/${exception_no}    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Config EQ Exception
    [Arguments]  ${exception_no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/exception-config/${exception_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Instrument Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/instrument-options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Member Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/member-options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Product Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/product-options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Tradable Instrument Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfeq-exception/tradable-instrument-options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

