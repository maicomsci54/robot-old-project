*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get Index Name Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/setindexvalue/idxname/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    
Get All SET Index Value Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/setindexvalue/setindexvaluesummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create SET Index Value
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/setindexvalue/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete SET Index Value
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/setindexvalue/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update SET Index Value
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/setindexvalue/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get SET Index Value Detail Summary
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/setindexvalue/setindexvaluedetailsummary   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

