*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Post Download File Report
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/downloadFile   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
