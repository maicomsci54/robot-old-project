*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get MM Conf Deri General
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfderigeneral/config/email    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Update MM Conf Deri General
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfderigeneral/update/config/email   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}