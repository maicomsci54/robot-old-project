*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get MM By Session Conf Summary
    [Arguments]  ${rec_no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmbysessionconf/${rec_no}     headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All MM By Session Conf Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmbysessionconf/mmbysessionconfsummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM By Session Conf
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmbysessionconf/create  data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM By Session Conf
    [Arguments]  ${rec_no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmbysessionconf/delete/${rec_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM By Session Conf
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmbysessionconf/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Rec No
    ${rec_no}=  Get Property Value From Json By Index    recNo   0
    Set Test Variable  ${REC_NO}