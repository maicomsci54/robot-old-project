*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get MM Performance
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmperformance/getmmperformance   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
