*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get MM Tier Discount Controller
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=     Get Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmtierdiscount/mmtierdiscountsummary   params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
