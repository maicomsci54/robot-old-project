*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM Daily Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmdaily/mmdailysummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All MM Daily Prod Group Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmdaily/mmdailysummary/prodgroups  params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
