*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Post Create MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Rerun Deri    
    [Arguments]  ${no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/delete/${no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Member MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/member/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Member MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/member/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Member Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmrerunderi/member/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Member Unit MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/memberunit/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Member Unit MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/memberunit/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Member Unit Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmrerunderi/memberunit/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All MM Rerun Deri Summary    
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmrerunderi/mmrerunderisummary   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Rerun Deri Summary
    [Arguments]  ${no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmrerunderi/mmrerunderisummary/${no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Prod Group MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/prodgroup/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Prod Group MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/prodgroup/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Product Group Options    
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmrerunderi/productgroup/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/rerun   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Rerun Deri
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmrerunderi/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get No
    ${NO}=  Get Property Value From Json By Index    no   0
    Set Test Variable  ${NO}