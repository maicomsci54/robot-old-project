*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM Config For Deri Instrument Group Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/instrumentgroupsummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Con Deri Instrument Group
    [Arguments]  ${instrument_group_no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/instrumentgroupsummary/${instrument_group_no}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Instrument Option
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/instrument/options     headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Instrument ID
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/instrumentid/options     headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Instrument Group No
    ${instrument_group_no}=  Get Property Value From Json By Index    instrumentGroupNo   0
    Set Test Variable  ${INSTRUMENT_GROUP_NO}

Post Create Instrument Group
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroup/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Instrument Group
    [Arguments]  ${instrument_group_no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroup/delete/${instrument_group_no}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Instrument Group
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroup/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Instrument Group Detail
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroupdetail/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Instrument Group Detail
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroupdetail/delete  data=${data}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Instrument Group Detail
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroupdetail/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Remove Data before Execute
    [Arguments]  ${instrument_group_name}
    Get All MM Config For Deri Instrument Group Summary  instrumentGroupName=${instrument_group_name}
    Response Correct Code  ${SUCCESS_CODE}
    ${status}  ${instrument_group_no}=  Run Keyword And Ignore Error  Get Property Value From Json By Index    content[0].instrumentGroupNo   0
    Run Keyword If  '${status}' != 'FAIL'   Post Delete Instrument Group   ${instrument_group_no}
