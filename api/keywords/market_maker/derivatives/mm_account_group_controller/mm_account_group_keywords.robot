*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM Account Group
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmaccountGroup/all   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Add Account Group
    [Arguments]  ${params_uri}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmaccountGroup/add   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Account Group
    [Arguments]  ${params_uri}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmaccountGroup/delete  params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Search Account Group
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=     Post Request   ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmaccountGroup/search  params=${params_uri}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Account Group
    [Arguments]   ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmaccountGroup/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Account Group ID
    ${account_group_id}=  Get Property Value From Json By Index    .AccountGroupID   0
    Set Test Variable  ${ACCOUNT_GROUP_ID}