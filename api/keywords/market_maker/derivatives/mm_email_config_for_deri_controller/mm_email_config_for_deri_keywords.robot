*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM Email Config For Deri Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforderi/mmemailconfigforderisummary  params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Product Group Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforderi/prductgroup/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Member Unit Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforderi/tradingmember/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Email Config For Deri Summary
    [Arguments]  ${email_no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmemailconfigforderi/${email_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Email Config For Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforderi/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Email Config For Deri
    [Arguments]  ${email_no}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforderi/delete/${email_no}     headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Email Config For Deri Prod Group
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforderi/mmemailconfigforderiprodgroup/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Email Config For Deri Prod Group
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforderi/mmemailconfigforderiprodgroup/delete    params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Email Config For Deri
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmemailconfigforderi/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Email No
    ${email_no}=  Get Property Value From Json By Index    emailNo   0
    Set Test Variable  ${EMAIL_NO}
