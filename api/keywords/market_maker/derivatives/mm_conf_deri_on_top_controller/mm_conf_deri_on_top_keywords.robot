*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get MM Conf Deri OnTop
    [Arguments]   ${on_top_id} 
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/${on_top_id}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Conf Deri On-Top
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/create    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Conf Deri On-Top
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/delete    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Conf Deri On-Top
    [Arguments]   ${data}
    ${RESP}=    Put Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/update    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All MM Conf Deri On-Top Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfderiontop/mmconfderiontopsummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Prduct Group Options
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/prductgroup/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Conf Deri On-Top ProdGroup
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/product-group/create    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Conf Deri On-Top ProdGroup
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/product-group/delete    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Conf Deri On-Top ProdGroup
    [Arguments]   ${data}
    ${RESP}=    Put Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/product-group/update    data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Conf Deri On-Top Tier Level
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/tier-level/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Conf Deri On-Top Tier Level
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/tier-level/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MM Conf Deri On-Top Tier Level
    [Arguments]   ${data}
    ${RESP}=    Put Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfderiontop/tier-level/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get On-Top ID
    ${ON_TOP_ID}=  Get Property Value From Json By Index    onTopID   0
    Set Test Variable  ${ON_TOP_ID}

Get Rec No
    ${rec_no}=  Get Property Value From Json By Index    recNo   0
    Set Test Variable  ${REC_NO}