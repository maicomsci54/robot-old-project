*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM Config For Deri Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/mmconfigforderisummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get MM Config For Deri Summary
    [Arguments]  ${prod_group_no}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/mmconfigforderisummary/${prod_group_no}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Relation Type Option
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/relationtype/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Trading Member Option
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/tradingmember/options   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Trading Member Unit Option
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/tradingmemberunit/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Instrument    
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/instrumentgroup/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All Clearing Member Option
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/clearingmember/options    headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MM Config For Deri
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Prod Group No
    ${PROD_GROUP_NO}=  Get Property Value From Json By Index    prodGroupNo   0
    Set Test Variable  ${PROD_GROUP_NO}

Get RecNo
    ${REC_NO}=  Get Property Value From Json By Index    recNo   0
    Set Test Variable  ${REC_NO}

Put Update MM Config For Deri
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmconfigforderi/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MM Config For Deri    
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}  /analytics/mmconfigforderi/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Obligation
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/obligation/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Obligation
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/obligation/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Obligation
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/obligation/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create MarketMaker
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/marketmaker/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update MarketMaker
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/marketmaker/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete MarketMaker
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/marketmaker/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Instrument Discount
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}  /analytics/mmconfigforderi/instrument-discount/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Instrument Discount
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}  /analytics/mmconfigforderi/instrument-discount/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Priority MinInt GroupTier
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityminintgrouptier/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Priority MinInt GroupTier
    [Arguments]  ${data}
    ${RESP}=     Put Request   ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityminintgrouptier/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Priority MinInt GroupTier
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityminintgrouptier/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Priority Block Min Pass Tier
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityblockminpasstier/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Priority Block Min Pass Tier
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityblockminpasstier/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Priority Block Min Pass Tier
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/priorityblockminpasstier/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Block Min PassInt
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/blockminpassint/create   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Delete Block Min PassInt
    [Arguments]  ${data}
    ${RESP}=     Post Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/blockminpassint/delete   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update Block Min PassInt
    [Arguments]  ${data}
    ${RESP}=     Put Request  ${OLD_ANT_GATEWAY_SESSION}   /analytics/mmconfigforderi/blockminpassint/update   data=${data}   headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
