*** Settings ***
Resource    ../../../../resources/init.robot

*** Keywords ***

Get All MM By Session Summary
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${OLD_ANT_GATEWAY_SESSION}    /analytics/mmbysession/mmbysessionsummary   params=${params_uri}  headers=&{MM_GATEWAY_HEADER}
    Set Test Variable    ${RESP}