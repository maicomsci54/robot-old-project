*** Settings ***
Resource      ../../resources/init.robot
Resource      ../common/json_common.robot

*** Keywords ***
Get Menus List
    ${RESP}=    Get Request    ${ANT_GATEWAY_SESSION}    /api/user-info/menus   headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}