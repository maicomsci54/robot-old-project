*** Settings ***
Resource      ../../resources/init.robot

*** Keywords ***

Post Create Users
    [Arguments]   ${data}
    ${RESP}=    Post Request    ${ANT_GATEWAY_SESSION}   /api/user-management/users   data=${data}    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    
Get User ID
    ${TEST_VARIABLE_USER_ID}=    Get Property Value From Json By Index    .userId   0
    Set Test Variable    ${TEST_VARIABLE_USER_ID}

Get Users Management
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${ANT_GATEWAY_SESSION}    /api/user-management   params=${params_uri}   headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Delete Users By ID
    [Arguments]    ${user_id}
    ${RESP}=    Delete Request    ${ANT_GATEWAY_SESSION}     /api/user-management/users/${user_id}    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Patch Users By ID
    [Arguments]    ${user_id}   ${data}
    ${RESP}=    Patch Request   ${ANT_GATEWAY_SESSION}     api/user-management/users/${user_id}    data=${data}    headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Users Management Detail
    [Arguments]  ${user_id}
    ${RESP}=    Get Request    ${ANT_GATEWAY_SESSION}    /api/user-management/users/${user_id}  headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get ID
    ${TEST_VARIABLE_ID}=    Get Property Value From Json By Index    .id   0
    Set Test Variable    ${TEST_VARIABLE_ID}

Clear User Data
    [Arguments]   @{username}
    :FOR  ${user_list}   IN   @{username}
    \   Get Users Management   username=${user_list}
    \   Run Keyword If  '${RESP.text}' != '[]'  Run keywords   Get ID   AND   Delete Users By ID    ${TEST_VARIABLE_ID}

