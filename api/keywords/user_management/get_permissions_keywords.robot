*** Settings ***
Resource      ../../resources/init.robot
Resource      ../common/json_common.robot

*** Keywords ***
Get Permissions List
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${ANT_GATEWAY_SESSION}    /api/user-management/permissions   params=${params_uri}   headers=&{ANT_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
