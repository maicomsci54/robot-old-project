*** Settings ***
Resource    ../../../resources/init.robot

*** Keywords ***
Get Holidays
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend/holidays   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Instrument Halt/SP
    [Arguments]  ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend  params=${params_uri}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Details Instrument Halt/SP By ID
    [Arguments]  ${id}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}   /instrument/halt-suspend/${id}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    
Get Uses
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend/users   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Short Name
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend/short-name   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create Instrument Halt/SP
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Patch Update Instrument Halt/SP
    [Arguments]  ${instrument_id}   ${data}
    ${RESP}=     Patch Request     ${MKTOPS_GATEWAY_SESSION}    /instrument/halt-suspend/${instrument_id}   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Instrument Halt/SP ID
    Set Test Variable    ${TEST_VARIABLE_INSTRUMENT_ID}   ${RESP.text}
    [Return]  ${RESP.text}

Get HaltSuspend
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /crontab/halt-suspend   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Logs
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /index-logs   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Index Logs File
    [Arguments]  ${file_name}
    ${RESP}=    Get Request    ${MKTOPS_GATEWAY_SESSION}    /index-logs/download/${file_name}   headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get File Name
    ${file_name}=    Get Property Value From Json By Index    [0].fileName     0
    Set Test Variable    ${TEST_VARIABLE_FILE_NAME}   ${file_name}

Post Move History Halt/SP
    [Arguments]  ${data}
    ${RESP}=     Post Request     ${MKTOPS_GATEWAY_SESSION}   /crontab/halt-suspend/move-hist   data=${data}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Delete Instrument Halt/SP
    [Arguments]  ${id}
    ${RESP}=    Delete Request    ${MKTOPS_GATEWAY_SESSION}   /instrument/halt-suspend/${id}  headers=&{MKTOPS_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Delete Instrument ID In List
    [Arguments]  @{instrument_id_list}
    :FOR  ${instrument_id}  IN  @{instrument_id_list}
    \   Delete Instrument Halt/SP    ${instrument_id}
    \   Response Correct Code   ${SUCCESS_CODE}
    \   Get Details Instrument Halt/SP By ID   ${instrument_id}
    \   Response Correct Code   ${INTERNAL_SERVER_CODE}
    \   Response Should Be Empty Body

Delete Instrument With Options
    [Arguments]  ${user}=user  ${effectiveDateFrom}=date_from  ${effectiveDateTo}=date_to
    Get Instrument Halt/SP   user=${user}&effectiveDateFrom=${effectiveDateFrom}&effectiveDateTo=${effectiveDateTo}
    @{property_list}=  Get Property Value From Json   .id
    Delete Instrument ID In List   @{property_list}

Clear Instrument Halt/SP In List
    [Documentation]  Check data first before run test every time
    [Arguments]   ${user}  ${effective_date_from}  ${effective_date_to}
    Get Instrument Halt/SP  user=${user}&effectiveDateFrom=${effective_date_from}&effectiveDateTo=${effective_date_to}
    Run Keyword If   '${RESP.text}' != '[]'  Run Keywords   Get Instrument ID All Property List
    ...  AND  Delete Instrument ID In List   @{INSTRUMENT_ID_LIST}

Get Instrument ID All Property List
    @{instrument_id_list}=    Get Property Value From Json    .id
    Set Test Variable    @{INSTRUMENT_ID_LIST}   @{instrument_id_list}

Download All Index Logs File
    @{property_value}=    Get Value From Json    ${RESP.json()}    $..fileName
    :FOR  ${file_name}  IN   @{property_value}
    \  Get Index Logs File    ${file_name}
    \  run keyword and continue on failure   Response Correct Code  ${SUCCESS_CODE}

Download Index Logs File Until Got Success Response
    @{property_value}=    Get Value From Json    ${RESP.json()}    $..fileName
    :FOR  ${file_name}  IN   @{property_value}
    \  Get Index Logs File    ${file_name}
    \  Run Keyword If    ${RESP.status_code}==${SUCCESS_CODE}
       ...  Exit For Loop