*** Settings ***
Resource    ../../resources/init.robot

*** Keywords ***

Get Generate Astidx Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/astidx
    Set Test Variable    ${RESP}

Get Generate Astsec Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/astsec
    Set Test Variable    ${RESP}

Get Generate Astsecsh Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/astsecsh
    Set Test Variable    ${RESP}

Get Generate Eodrcase Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/eodrcase
    Set Test Variable    ${RESP}

Get Generate Maiidx Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/maiidx
    Set Test Variable    ${RESP}

Get Generate Mairidx Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/mairidx
    Set Test Variable    ${RESP}

Get Generate Maisec Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/maisec
    Set Test Variable    ${RESP}

Get Generate Mktstat Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}   /report/mktstat
    Set Test Variable    ${RESP}

Get Generate Rindex Report
    ${RESP}=    Get Request    ${RPT_BEFORE_GATEWAY_SESSION}    /report/rindex
    Set Test Variable    ${RESP}


