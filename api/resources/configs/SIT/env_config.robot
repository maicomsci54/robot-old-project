*** Variables ***
#New ANT
${AUTHORIZATION_GATEWAY_SESSION}  AUTHORIZATION_GATEWAY_SESSION
${ANT_API_HOST_TOKEN}     https://antlogin-dev.trading.set/sso-robot

${ANT_GATEWAY_SESSION}    ANT_GATEWAY_SESSION
${ANT_API_HOST}     https://antlogin-dev.trading.set/sso

${AUTHORIZATION_KEY}    Basic dHJ1ZXlvdXNlbGxlcmFwcDpDLFpmJmR9OUckJUQzYChE

${MKTOPS_GATEWAY_SESSION}    MKTOPS_GATEWAY_SESSION
${MKTOPS_API_HOST}   https://mktops-dev.trading.set/mkt-ops

${INDEX_ACTION_GATEWAY_SESSION}    INDEX_ACTION_GATEWAY_SESSION
${ANT_INDEX_ACTION_HOST}   https://antindexaction-dev.trading.set

${RPT_BEFORE_GATEWAY_SESSION}    RPT_BEFORE_GATEWAY_SESSION
${ANT_RPT_BEFORE_HOST}  https://antreport-dev.trading.set/rpt-before

${RPT_GATEWAY_SESSION}    RPT_GATEWAY_SESSION
${ANT_RPT_HOST}   https://antreport-dev.trading.set/rpt

#Old ANT
${OLD_ANT_GATEWAY_SESSION}   OLD_ANT_GATEWAY_SESSION
${OLD_ANT_HOST}   https://antanalyticsdev.trading.set

# List of the response code
${SUCCESS_CODE}               200
${CREATED_CODE}               201
${BAD_REQUEST_CODE}           400
${INTERNAL_SERVER_CODE}       500
${NOT_FOUND_CODE}             404
${UNAUTHORIZED}               401
${FORBIDDEN_CODE}             403
${METHOD_NOT_ALLOWED}         405
${ACCEPTED_CODE}              202